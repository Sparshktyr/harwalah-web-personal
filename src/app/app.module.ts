import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { ForgotPasswordComponent } from './pages/auth/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './pages/auth/verify-email/verify-email.component';
import { CreatePasswordComponent } from './pages/auth/create-password/create-password.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptorService } from './service/auth-interceptor.service';
import { DataSharedService } from './service/data-shared.service';
import { UppercasePipe } from './pipes/uppercase.pipe';
import { LowercasePipe } from './pipes/lowercase.pipe';
import { CurrencyInfoPipe } from './pipes/currency-info.pipe';
import { SecondsToMinutesPipe } from './pipes/seconds-to-minutes.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,LoginComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    CreatePasswordComponent,
    UppercasePipe,
    LowercasePipe,
    CurrencyInfoPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    SharedModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  exports:[
    SharedModule
  ],
  providers: [
    {
      multi: true,
      useClass: AuthInterceptorService,
      provide: HTTP_INTERCEPTORS,
    },
    DataSharedService
  ],  
  bootstrap: [AppComponent]
})
export class AppModule { }
