import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyService } from '../service/currency-service.service'; // You need to implement this service

@Pipe({
  name: 'currencyInfo'
})
export class CurrencyInfoPipe implements PipeTransform {
  constructor(private currencyService: CurrencyService) {}

  transform(countryCode: string, infoType: 'symbol' | 'name'): string {
    if (!countryCode) return '';

    const currencyInfo = this.currencyService.getCurrencyInfo(countryCode);
    if (!currencyInfo) return '';

    return infoType === 'symbol' ? currencyInfo.symbol : currencyInfo.name;
  }


  /* ------ How to use this pipe -------*/
/*
  p>{{ 'US' | currencyInfo: 'symbol' }}</p>   // what you want from this pipe either symbol only or name 
  <p>{{ 'US' | currencyInfo: 'name' }}</p>->
*/
}
