import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondsToMinutes'
})
export class SecondsToMinutesPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    
    let isNegative = false;
    if(Number(value) < 0){
      value = Number(value) * -1;
      isNegative = true;
    }
    let min = `${Math.floor(Number(value) / 60)}`;
    let second = `${Number(value) % 60}`;
    if(min.length == 1){
      min = `0${min}`
    }
    if(second.length == 1){
      second = `0${second}`
    }
    if(isNegative){
      return `- ${min}:${second}`;
    }
    return `${min}:${second}`;
  }

}
