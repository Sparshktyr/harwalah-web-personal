import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'diffBetMin'
})
export class DiffBetMinPipe implements PipeTransform {

  transform(valueForDiff: unknown, ...args: unknown[]): unknown {
    // console.log(args);
    
    if(!args || !args[0]){
      return null;
    }
    
    let value:any[] = [`${valueForDiff}`];
    value[1] = `${args[0]}`.split("/")[0];
    let valueArr = `${value[0]}`.split(":");
    let totalMinut:number = Number(valueArr[0]) * 60;
    if(valueArr[1]){
      totalMinut = totalMinut + Number(valueArr[1]);
    }
    // console.log(totalMinut);
    let differenceReps;
    if(value[1]){
      let findDataHourMinArr = `${value[1]}`.split(":");
      let minOfFindData = Number(findDataHourMinArr[0]) * 60;
      if(findDataHourMinArr[1]){
        minOfFindData = minOfFindData + Number(findDataHourMinArr[1]);
      }
      differenceReps = `${minOfFindData - totalMinut}`;
    }
    if(differenceReps){      
      let isNegative = false;
      if(Number(differenceReps) < 0){
        differenceReps = Number(differenceReps) * -1;
        isNegative = true;
      }
      let min = `${Math.floor(Number(differenceReps) / 60)}`;
      let second = `${Number(differenceReps) % 60}`;
      if(min.length == 1){
        min = `0${min}`
      }
      if(second.length == 1){
        second = `0${second}`
      }
      if(second == '00' && min == "00"){
        return ` ${min}:${second}`;
      }
      if(isNegative){
        return `+ ${min}:${second}`;
      }
      return `- ${min}:${second}`;      
    }
    return null;
  }

}
