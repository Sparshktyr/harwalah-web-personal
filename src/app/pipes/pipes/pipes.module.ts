import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecondsToMinutesPipe } from '../seconds-to-minutes.pipe';
import { DiffBetMinPipe } from '../diff-bet-min.pipe';



@NgModule({
  declarations: [
    SecondsToMinutesPipe,
    DiffBetMinPipe
  ],
  imports: [
    CommonModule
  ],
  entryComponents: [SecondsToMinutesPipe],
  exports: [SecondsToMinutesPipe,DiffBetMinPipe]
})
export class PipesModule { }
