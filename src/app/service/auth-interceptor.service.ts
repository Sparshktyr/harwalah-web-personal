import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, catchError, throwError } from "rxjs";
import { SessionStorageService } from "./session-storage.service";
// import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: "root",
})
export class AuthInterceptorService {
  constructor(
    private router: Router,
    private sessionStorage: SessionStorageService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //debugger
    let userData = this.sessionStorage.getLocalData("ClubPe-Admin");
    var access_token;
    if (userData && userData != "undefined") {
      console.log(userData);

      access_token = userData.access_token;
      if (!access_token) {
        access_token = "";
      }
    } else {
      access_token = "";
    }
    let url = "";

    url = req.url;
    // console.log(url)
    const copiedReq = req.clone({
      headers: req.headers.append("access_token", access_token),
      url: url,
    });

    return next.handle(copiedReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (
          error.error &&
          (error.error.message == "Invalid access_token." ||
            error.error.message == "Access token missing" ||
            error.error.message == "You are blocked by admin" ||
            error.error.message == "Invalid/expired session" ||
            error.error.message == "Blocked by Admin")
        ) {
          localStorage.removeItem("ClubPe-Admin");
          this.router.navigate(["/log-in"]);

          if (error.error.message === "Blocked by Admin") {
            //  this.toaster.error('You are blocked by admin')
          }

          if (error.error.message === "Invalid/expired session") {
            // this.toaster.error('Session Expired')
          }

          // window.location.reload();
        }
        return throwError(error);
      })
    );
  }
}
