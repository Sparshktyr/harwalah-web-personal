import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  constructor() { }


  baseUrl = environment.baseUrl;

  public setLocalData(key: string, value: any) {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  public setWebLocal(value: any,) {
    sessionStorage.setItem("harwallah-web", JSON.stringify(value));
  }

  public removeWebLocal() {
    sessionStorage.removeItem("harwallah-web");
  }

  public getWebLocal() {
    let data = sessionStorage.getItem("harwallah-web");
    if (data == null || data == 'undefined') {
      return null;
    }
    return JSON.parse(data);
  }

  ///  for Coach
  public setWebLocalForCoach(value: any) {
    sessionStorage.setItem("harwallah-web-coach", JSON.stringify(value));
  }

  public removeWebLocalForCoach() {
    sessionStorage.removeItem("harwallah-web-coach");
  }

  public getWebLocalForCoach() {
    let data = sessionStorage.getItem("harwallah-web-coach");
    if (data == null || data == 'undefined') {
      return null;
    }
    return JSON.parse(data);
  }
  //


  public getLocalData(key: string) {
    let data = sessionStorage.getItem(key);
    if (data == null || data == 'undefined') {
      return null;
    }
    return JSON.parse(data);
  }

  public setSignupLocal(value:Object) {
    localStorage.setItem("harwallah-web-signup", JSON.stringify(value));
  }

  public removeSignupLocal() {
    localStorage.removeItem("harwallah-web-signup");
  }

  public getSignupLocal() {
    let data = localStorage.getItem("harwallah-web-signup");
    if (data == null || data == 'undefined') {
      return null;
    }
    return JSON.parse(data);
  }

}
