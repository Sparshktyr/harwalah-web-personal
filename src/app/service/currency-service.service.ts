// currency.service.ts

import { Injectable } from '@angular/core';
import { CURRENCY_DATA } from '../utils/currency-data'; 

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  getCurrencyInfo(countryCode: string): { symbol: string, name: string } {
    return CURRENCY_DATA[countryCode.toUpperCase()];
  }
}
