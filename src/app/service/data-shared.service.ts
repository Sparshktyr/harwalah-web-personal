import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSharedService {

  constructor() { }

  private message = new BehaviorSubject("");
  getMessage = this.message.asObservable();

  setMessage(message:any){
    this.message.next(message);
  }

  private loginData = new BehaviorSubject("");
  getLoginData = this.loginData.asObservable();

  setLoginData(message:string){
    this.loginData.next(message);
  }


}
