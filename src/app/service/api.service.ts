import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl

  // user_post(url: any, data: any) {
  //   return this.http.post(`${this.baseUrl}/api/v1/app/user/${url}`, data);
  // }

  // user_get(url: any) {
  //   return this.http.get(`${this.baseUrl}/api/v1/app/user/${url}`);
  // }

  user_post(url: any, data: any) {
    return this.http.post(`${this.baseUrl}/api/v1/web/user/${url}`, data);
  }

  user_get(url: any) {
    return this.http.get(`${this.baseUrl}/api/v1/web/user/${url}`);
  }
  
  coach_post(url: any, data: any) {
    return this.http.post(`${this.baseUrl}/api/v1/web/coach/${url}`, data);
  }

  coach_get(url: any) {
    return this.http.get(`${this.baseUrl}/api/v1/web/coach/${url}`);
  }

  coach_post_app(url: any, data: any) {
    return this.http.post(`${this.baseUrl}/api/v1/app/coach/${url}`, data);
  }

  coach_get_app(url: any) {
    return this.http.get(`${this.baseUrl}/api/v1/app/coach/${url}`);
  }

  common_api_get(url:any){
    // http://staging.harwalah.com/api/v1/web/countries
    return this.http.get(`${this.baseUrl}/api/v1/web/${url}`);
  }
  common_api_post(url:any,data:any){
    // http://staging.harwalah.com/api/v1/web/countries
    return this.http.post(`${this.baseUrl}/api/v1/web/${url}`,data);
  }
}
