import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  constructor(
    private sesionStorage:SessionStorageService,
    private api : ApiService
  ){}

  userData:any;
  ngOnInit(): void {
    this.userData = this.sesionStorage.getWebLocal();
  }
}
