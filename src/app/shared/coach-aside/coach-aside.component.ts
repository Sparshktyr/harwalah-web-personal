import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { RejectedDialogComponent } from '../rejected-dialog/rejected-dialog.component';

@Component({
  selector: 'app-coach-aside',
  templateUrl: './coach-aside.component.html',
  styleUrls: ['./coach-aside.component.css']
})
export class CoachAsideComponent implements OnInit {

  constructor(
    private sesionStorage:SessionStorageService,
    private api : ApiService,
    private dialog:MatDialog
  ){}

  coachData:any;
  statusActiveInactive:any = {
    '1':"Active",
    '0':"Inactive",
  }
  ngOnInit(): void {
    this.coachData = this.sesionStorage.getWebLocalForCoach();
  }

  // 0 = inactive, 1 = active/accept , 2 = pending, 3 = rejected
  changeStatus(event:any){
    console.log(event.checked);
    let status:number = 0;
    if(event.checked){
      status = 1; 
    }else{
      // this.changeStatusActiveInactiveCoach(0)
      status = 0;
    }
    this.changeStatusActiveInactiveCoach(status).then(()=>{
        this.coachData.status = status;
        this.sesionStorage.setWebLocalForCoach(this.coachData);
    })
    
  }

  changeStatusActiveInactiveCoach(status:number){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":this.coachData?.login_token,
        "coach_id":this.coachData?.coach_id,
        "status":`${status}`
    }
    this.api.coach_post("change/status",data).subscribe({
      next: (res: any) => {
        if(res && !res.error){
            resolve({
              success:true,
              data:res.data
            })
        }else{
          reject({
            success:false,
            message:res.message
          })
        }
      },
      error: (err: any) => {
        console.log(err);
        // this.error = err.error.message;
        reject({
          success:false,
          message:err.error.message
        })
      }
    });

  })
  }

  goToRouterLink(link:String){
    
  }
  BlockAction(){
    let text="";
    if(this.coachData.status == 2){
      text = "You Are Rejected By Admin.";
    }else if(this.coachData.status == 3){
      text = "You Are Blocked By Admin.";
    }else{
      text = "You Are Blocked By Admin.";
    }
    this.dialog.open(RejectedDialogComponent,{
      disableClose:true,
      //position: {right:'0px', top: '0px'},
      data:{
        text:text
      } 
    })
  }


}
