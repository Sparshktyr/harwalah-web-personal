import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-rejected-dialog',
  templateUrl: './rejected-dialog.component.html',
  styleUrls: ['./rejected-dialog.component.css']
})
export class RejectedDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<RejectedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api:ApiService
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    
  }

}
