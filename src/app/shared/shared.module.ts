import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { DataSharedService } from '../service/data-shared.service';
import { CoachAsideComponent } from './coach-aside/coach-aside.component';
import { AsideComponent } from './aside/aside.component';
import { RejectedDialogComponent } from './rejected-dialog/rejected-dialog.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CoachAsideComponent,
    AsideComponent,
    RejectedDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],

  exports:[
    HeaderComponent,
    FooterComponent,
    CoachAsideComponent,
    AsideComponent
  ],
  providers:[
    DataSharedService
  ]
})
export class SharedModule { }
