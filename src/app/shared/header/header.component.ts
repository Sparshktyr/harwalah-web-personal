import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSharedService } from 'src/app/service/data-shared.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit , AfterContentChecked{

  selected = 'option2';
  
  hide = true;
  signUpData: any;

  constructor(
    private localstorage:SessionStorageService,
    private router:Router
  ) { }


  webFor = {
    "USER":false,
    "COACH":false
  }
  loginData:any;
  ngOnInit(): void {
    // this.getCountryList();
    this.getLoginData();
    // this.getSignUpData();
  }

  ngAfterContentChecked(): void {
    if(!this.loginData){
      this.getLoginData();
    }

  }
  getLoginData(){
    this.loginData = this.localstorage.getWebLocal();
    if(!this.loginData){
      this.loginData = this.localstorage.getWebLocalForCoach();
      if(this.loginData){
        this.webFor.COACH = true;
      }
    }else{
      this.webFor.USER = true;
    }
    // console.log(this.loginData);
  }

  getSignUpData(){
    this.loginData = this.localstorage.getSignupLocal();
    if(!this.loginData){
      this.loginData = true
    }else{
      this.loginData = false
    }
  }

  Logout(){
    this.localstorage.removeSignupLocal()
    this.localstorage.removeWebLocalForCoach();
    this.localstorage.removeWebLocal()
    this.router.navigateByUrl("/login").then(()=>{
      // window.location.reload()
      this.loginData = false;
      this.webFor = {
        COACH:false,
        USER:false
      }
    });
  }


}
