import { Component } from '@angular/core';
import { SessionStorageService } from '../service/session-storage.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent {
loginData: Boolean  = false;
constructor(
  private localstorage:SessionStorageService,
) { }
webFor = {
  "USER":false,
  "COACH":false
}
ngOnInit(){
this.getSignUpData()
this.getLoginData()
}


getLoginData(){
  this.loginData = this.localstorage.getWebLocal();
  if(!this.loginData){
    this.loginData = this.localstorage.getWebLocalForCoach();
    if(this.loginData){
      this.webFor.COACH = true;
    }
  }else{
    this.webFor.USER = true;
  }
  // console.log(this.loginData);
}
getSignUpData(){
  this.loginData = this.localstorage.getSignupLocal();
  if(!this.loginData){
    this.loginData = true
  }else{
    this.loginData = false
  }
}
}
