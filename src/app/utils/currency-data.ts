// currency-data.ts
export const CURRENCY_DATA: { [code: string]: { symbol: string, name: string } } = {
    'AF': { symbol: '؋', name: 'Afghan Afghani' },
    'AL': { symbol: 'L', name: 'Albanian Lek' },
    'DZ': { symbol: 'د.ج', name: 'Algerian Dinar' },
    'AO': { symbol: 'Kz', name: 'Angolan Kwanza' },
    'AR': { symbol: '$', name: 'Argentine Peso' },
    'AM': { symbol: '֏', name: 'Armenian Dram' },
    'AW': { symbol: 'ƒ', name: 'Aruban Florin' },
    'AU': { symbol: '$', name: 'Australian Dollar' },
    'AT': { symbol: '€', name: 'Euro' },
    'AZ': { symbol: '₼', name: 'Azerbaijani Manat' },
    'BS': { symbol: '$', name: 'Bahamian Dollar' },
    'BH': { symbol: '.د.ب', name: 'Bahraini Dinar' },
    'BD': { symbol: '৳', name: 'Bangladeshi Taka' },
    'BB': { symbol: '$', name: 'Barbadian Dollar' },
    'BY': { symbol: 'Br', name: 'Belarusian Ruble' },
    'BE': { symbol: '€', name: 'Euro' },
    'BZ': { symbol: 'BZ$', name: 'Belize Dollar' },
    'BJ': { symbol: 'CFA', name: 'West African CFA franc' },
    'BM': { symbol: '$', name: 'Bermudian Dollar' },
    'BT': { symbol: 'Nu.', name: 'Bhutanese Ngultrum' },
    'BO': { symbol: 'Bs', name: 'Bolivian Boliviano' },
    'BA': { symbol: 'KM', name: 'Bosnia-Herzegovina Convertible Mark' },
    'BW': { symbol: 'P', name: 'Botswana Pula' },
    'BR': { symbol: 'R$', name: 'Brazilian Real' },
    'VG': { symbol: '$', name: 'British Virgin Islands Dollar' },
    'BN': { symbol: '$', name: 'Brunei Dollar' },
    'BG': { symbol: 'лв', name: 'Bulgarian Lev' },
    'BF': { symbol: 'CFA', name: 'West African CFA franc' },
    'BI': { symbol: 'FBu', name: 'Burundian Franc' },
    'KH': { symbol: '៛', name: 'Cambodian Riel' },
    'CM': { symbol: 'CFA', name: 'Central African CFA franc' },
    'CA': { symbol: '$', name: 'Canadian Dollar' },
    'CV': { symbol: 'Esc', name: 'Cape Verdean Escudo' },
    'KY': { symbol: '$', name: 'Cayman Islands Dollar' },
    'CF': { symbol: 'CFA', name: 'Central African CFA franc' },
    'TD': { symbol: 'CFA', name: 'Central African CFA franc' },
    'CL': { symbol: '$', name: 'Chilean Peso' },
    'CN': { symbol: '¥', name: 'Chinese Yuan' },
    'CO': { symbol: '$', name: 'Colombian Peso' },
    'KM': { symbol: 'CF', name: 'Comorian Franc' },
    'CG': { symbol: 'CFA', name: 'Central African CFA franc' },
    'CD': { symbol: 'FC', name: 'Congolese Franc' },
    'CR': { symbol: '₡', name: 'Costa Rican Colón' },
    'HR': { symbol: 'kn', name: 'Croatian Kuna' },
    'CU': { symbol: '₱', name: 'Cuban Peso' },
    'CW': { symbol: 'ƒ', name: 'Netherlands Antillean Guilder' },
    'CY': { symbol: '€', name: 'Euro' },
    'CZ': { symbol: 'Kč', name: 'Czech Koruna' },
    'DK': { symbol: 'kr', name: 'Danish Krone' },
    'DJ': { symbol: 'Fdj', name: 'Djiboutian Franc' },
    'DM': { symbol: '$', name: 'Dominican Peso' },
    'EC': { symbol: '$', name: 'US Dollar' },
    'EG': { symbol: 'E£', name: 'Egyptian Pound' },
    'SV': { symbol: '$', name: 'Salvadoran Colón' },
    'GQ': { symbol: 'CFA', name: 'Central African CFA franc' },
    'ER': { symbol: 'Nfk', name: 'Eritrean Nakfa' },
    'EE': { symbol: '€', name: 'Euro' },
    'ET': { symbol: 'Br', name: 'Ethiopian Birr' },
    'FK': { symbol: '£', name: 'Falkland Islands Pound' },
    'FO': { symbol: 'kr', name: 'Faroese Króna' },
    'FJ': { symbol: '$', name: 'Fijian Dollar' },
    'FI': { symbol: '€', name: 'Euro' },
    'FR': { symbol: '€', name: 'Euro' },
    'PF': { symbol: 'F', name: 'CFP Franc' },
    'GA': { symbol: 'CFA', name: 'Central African CFA franc' },
    'GM': { symbol: 'D', name: 'Gambian Dalasi' },
    'GE': { symbol: '₾', name: 'Georgian Lari' },
    'DE': { symbol: '€', name: 'Euro' },
    'GH': { symbol: '₵', name: 'Ghanaian Cedi' },
    'GI': { symbol: '£', name: 'Gibraltar Pound' },
    'GR': { symbol: '€', name: 'Euro' },
    'GL': { symbol: 'kr', name: 'Danish Krone' },
    'GD': { symbol: '$', name: 'Grenadian Dollar' },
    'GT': { symbol: 'Q', name: 'Guatemalan Quetzal' },
    'GN': { symbol: 'FG', name: 'Guinean Franc' },
    'GW': { symbol: 'CFA', name: 'West African CFA franc' },
    'GY': { symbol: '$', name: 'Guyanaese Dollar' },
    'HT': { symbol: 'G', name: 'Haitian Gourde' },
    'HN': { symbol: 'L', name: 'Honduran Lempira' },
    'HK': { symbol: '$', name: 'Hong Kong Dollar' },
    'HU': { symbol: 'Ft', name: 'Hungarian Forint' },
    'IS': { symbol: 'kr', name: 'Icelandic Króna' },
    'IN': { symbol: '₹', name: 'Indian Rupee' },
    'ID': { symbol: 'Rp', name: 'Indonesian Rupiah' },
    'IR': { symbol: '﷼', name: 'Iranian Rial' },
    'IQ': { symbol: 'ع.د', name: 'Iraqi Dinar' },
    'IE': { symbol: '€', name: 'Euro' },
    'IM': { symbol: '£', name: 'Manx Pound' },
    'IL': { symbol: '₪', name: 'Israeli New Shekel' },
    'IT': { symbol: '€', name: 'Euro' },
    'JM': { symbol: '$', name: 'Jamaican Dollar' },
    'JP': { symbol: '¥', name: 'Japanese Yen' },
    'JO': { symbol: 'JD', name: 'Jordanian Dinar' },
    'KZ': { symbol: '〒', name: 'Kazakhstani Tenge' },
    'KE': { symbol: 'KSh', name: 'Kenyan Shilling' },
    'KI': { symbol: '$', name: 'Kiribati Dollar' },
    'KP': { symbol: '₩', name: 'North Korean Won' },
    'KR': { symbol: '₩', name: 'South Korean Won' },
    'KW': { symbol: 'د.ك', name: 'Kuwaiti Dinar' },
    'KG': { symbol: 'som', name: 'Kyrgyzstani Som' },
    'LA': { symbol: '₭', name: 'Lao Kip' },
    'LV': { symbol: '€', name: 'Euro' },
    'LB': { symbol: 'ل.ل', name: 'Lebanese Pound' },
    'LS': { symbol: 'L', name: 'Lesotho Loti' },
    'LR': { symbol: '$', name: 'Liberian Dollar' },
    'LY': { symbol: 'LD', name: 'Libyan Dinar' },
    'LI': { symbol: 'CHF', name: 'Swiss Franc' },
    'LT': { symbol: '€', name: 'Euro' },
    'LU': { symbol: '€', name: 'Euro' },
    'MO': { symbol: 'P', name: 'Macanese Pataca' },
    'MK': { symbol: 'den', name: 'Macedonian Denar' },
    'MG': { symbol: 'Ar', name: 'Malagasy Ariary' },
    'MW': { symbol: 'MK', name: 'Malawian Kwacha' },
    'MY': { symbol: 'RM', name: 'Malaysian Ringgit' },
    'MV': { symbol: 'Rf', name: 'Maldivian Rufiyaa' },
    'ML': { symbol: 'CFA', name: 'West African CFA franc' },
    'MT': { symbol: '€', name: 'Euro' },
    'MH': { symbol: '$', name: 'US Dollar' },
    'MR': { symbol: 'UM', name: 'Mauritanian Ouguiya' },
    'MU': { symbol: 'Rs', name: 'Mauritian Rupee' },
    'MX': { symbol: '$', name: 'Mexican Peso' },
    'MD': { symbol: 'MDL', name: 'Moldovan Leu' },
    'MC': { symbol: '€', name: 'Euro' },
    'MN': { symbol: '₮', name: 'Mongolian Tugrik' },
    'ME': { symbol: '€', name: 'Euro' },
    'MS': { symbol: '$', name: 'East Caribbean Dollar' },
    'MA': { symbol: 'DH', name: 'Moroccan Dirham' },
    'MZ': { symbol: 'MTn', name: 'Mozambican Metical' },
    'MM': { symbol: 'K', name: 'Myanmar Kyat' },
    'NA': { symbol: 'N$', name: 'Namibian Dollar' },
    'NP': { symbol: 'Rs', name: 'Nepalese Rupee' },
    'NL': { symbol: '€', name: 'Euro' },
    'NZ': { symbol: '$', name: 'New Zealand Dollar' },
    'NI': { symbol: 'C$', name: 'Nicaraguan Córdoba' },
    'NE': { symbol: 'CFA', name: 'West African CFA franc' },
    'NG': { symbol: '₦', name: 'Nigerian Naira' },
    'NO': { symbol: 'kr', name: 'Norwegian Krone' },
    'OM': { symbol: 'ر.ع.', name: 'Omani Rial' },
    'PK': { symbol: '₨', name: 'Pakistani Rupee' },
    'PW': { symbol: '$', name: 'US Dollar' },
    'PS': { symbol: '₪', name: 'Israeli New Shekel' },
    'PA': { symbol: 'B/.', name: 'Panamanian Balboa' },
    'PG': { symbol: 'K', name: 'Papua New Guinean Kina' },
    'PY': { symbol: '₲', name: 'Paraguayan Guarani' },
    'PE': { symbol: 'S/', name: 'Peruvian Sol' },
    'PH': { symbol: '₱', name: 'Philippine Peso' },
    'PL': { symbol: 'zł', name: 'Polish Zloty' },
    'PT': { symbol: '€', name: 'Euro' },
    'QA': { symbol: 'ر.ق', name: 'Qatari Riyal' },
    'RO': { symbol: 'lei', name: 'Romanian Leu' },
    'RU': { symbol: '₽', name: 'Russian Ruble' },
    'RW': { symbol: 'RF', name: 'Rwandan Franc' },
    'LC': { symbol: '$', name: 'East Caribbean Dollar' },
    'WS': { symbol: 'WS$', name: 'Samoan Tala' },
    'SM': { symbol: '€', name: 'Euro' },
    'ST': { symbol: 'Db', name: 'São Tomé and Príncipe Dobra' },
    'SA': { symbol: 'ر.س', name: 'Saudi Riyal' },
    'SN': { symbol: 'CFA', name: 'West African CFA franc' },
    'RS': { symbol: 'din', name: 'Serbian Dinar' },
    'SC': { symbol: 'SR', name: 'Seychellois Rupee' },
    'SL': { symbol: 'Le', name: 'Sierra Leonean Leone' },
    'SG': { symbol: '$', name: 'Singapore Dollar' },
    'SX': { symbol: 'ƒ', name: 'Netherlands Antillean Guilder' },
    'SK': { symbol: '€', name: 'Euro' },
    'SI': { symbol: '€', name: 'Euro' },
    'SB': { symbol: 'SI$', name: 'Solomon Islands Dollar' },
    'SO': { symbol: 'Sh', name: 'Somali Shilling' },
    'ZA': { symbol: 'R', name: 'South African Rand' },
    'SS': { symbol: 'SSP', name: 'South Sudanese Pound' },
    'ES': { symbol: '€', name: 'Euro' },
    'LK': { symbol: 'Rs', name: 'Sri Lankan Rupee' },
    'BL': { symbol: '€', name: 'Euro' },
    'SH': { symbol: '£', name: 'Saint Helena Pound' },
    'KN': { symbol: '$', name: 'East Caribbean Dollar' },
    'MF': { symbol: '€', name: 'Euro' },
    'PM': { symbol: '€', name: 'Euro' },
    'VC': { symbol: '$', name: 'East Caribbean Dollar' },
    'SD': { symbol: 'SDG', name: 'Sudanese Pound' },
    'SR': { symbol: '$', name: 'Surinamese Dollar' },
    'SZ': { symbol: 'L', name: 'Swazi Lilangeni' },
    'SE': { symbol: 'kr', name: 'Swedish Krona' },
    'CH': { symbol: 'CHF', name: 'Swiss Franc' },
    'SY': { symbol: 'LS', name: 'Syrian Pound' },
    'TW': { symbol: 'NT$', name: 'New Taiwan Dollar' },
    'TJ': { symbol: 'ЅМ', name: 'Tajikistani Somoni' },
    'TZ': { symbol: 'TSh', name: 'Tanzanian Shilling' },
    'TH': { symbol: '฿', name: 'Thai Baht' },
    'TL': { symbol: 'MT', name: 'Timorese Dollar' },
    'TG': { symbol: 'CFA', name: 'West African CFA franc' },
    'TO': { symbol: 'T$', name: 'Tongan Paʻanga' },
    'TT': { symbol: 'TT$', name: 'Trinidad and Tobago Dollar' },
    'TN': { symbol: 'DT', name: 'Tunisian Dinar' },
    'TR': { symbol: '₺', name: 'Turkish Lira' },
    'TM': { symbol: 'T', name: 'Turkmenistan Manat' },
    'TC': { symbol: '$', name: 'US Dollar' },
    'TV': { symbol: 'VT', name: 'Tuvaluan Dollar' },
    'UG': { symbol: 'USh', name: 'Ugandan Shilling' },
    'UA': { symbol: '₴', name: 'Ukrainian Hryvnia' },
    'AE': { symbol: 'د.إ', name: 'United Arab Emirates Dirham' },
    'GB': { symbol: '£', name: 'British Pound' },
    'US': { symbol: '$', name: 'US Dollar' },
    'UY': { symbol: '$', name: 'Uruguayan Peso' },
    'UZ': { symbol: 'лв', name: 'Uzbekistani Som' },
    'VU': { symbol: 'VT', name: 'Vanuatu Vatu' },
    'VA': { symbol: '€', name: 'Euro' },
    'VE': { symbol: 'Bs', name: 'Venezuelan Bolívar' },
    'VN': { symbol: '₫', name: 'Vietnamese Đồng' },
    'VI': { symbol: '$', name: 'US Dollar' },
    'WF': { symbol: '₣', name: 'CFP Franc' },
    'EH': { symbol: 'د.م.', name: 'Moroccan Dirham' },
    'YE': { symbol: '﷼', name: 'Yemeni Rial' },
    'ZM': { symbol: 'ZK', name: 'Zambian Kwacha' },
    'ZW': { symbol: 'Z$', name: 'Zimbabwean Dollar' }
  };
  