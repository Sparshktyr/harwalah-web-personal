import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export function calculateDaysDifference(startDate: string, endDate: string): number {

  const startDateObj: Date = new Date(startDate);
  const endDateObj: Date = new Date(endDate);

  const timeDifference: number = endDateObj.getTime() - startDateObj.getTime();


  const daysDifference: number = timeDifference / (1000 * 3600 * 24);

  return daysDifference;
}

export function convertSecondIntoMinuts(second:number): string {

  let result:string = `${second / 60}`;
  let reminder:string = `${second % 60}`;
  if(result.length == 1){
    result = `0${result}`;
  }
  if(reminder.length == 1){
    reminder = `0${reminder}`;
  }

  return `${result}:${reminder}`;
}

export function calculateYearsDifference(fromDate: string): number {
  // FUNCTION TO CALCULATE CURRENT AGE FROM GIVEN DATE
  const currentDate: Date = new Date();
  const fromDateObj: Date = new Date(fromDate);

  const currentYear: number = currentDate.getFullYear();
  const fromYear: number = fromDateObj.getFullYear();

  const yearsDifference: number = currentYear - fromYear;

  return yearsDifference;
}


export function formatDate(date: Date, monthsToAdd: number = 0): string {
  date.setMonth(date.getMonth() + monthsToAdd);

  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return `${day}-${month < 10 ? "0" + month : month}-${year}`;
}

export function capitalizeFirstLetter(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function generateRandomString(length: number): string {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let result = "";
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
}

export function deepClone(obj: any): any {
  return JSON.parse(JSON.stringify(obj));
}

export function isEmpty(value: any): boolean {
  return (
    value === null ||
    value === undefined ||
    (Array.isArray(value) && value.length === 0) ||
    value === ""
  );
}

export function isValidEmail(email: string): boolean {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}

export function toCamelCase(str: string): string {
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, "");
}

export function truncateText(text: string, maxLength: number): string {
  if (text.length <= maxLength) {
    return text;
  }
  return text.slice(0, maxLength) + '...';
}

export function removeDuplicates<T>(array: T[]): T[] {
  return [...new Set(array)];
}

export function sortByProperty<T>(array: T[], property: keyof T, ascending: boolean = true): T[] {
  return array.slice().sort((a, b) => {
    if (a[property] === b[property]) {
      return 0;
    }
    return (a[property] < b[property] ? -1 : 1) * (ascending ? 1 : -1);
  });
}

export function objectToArray<T>(obj: { [key: string]: T }): T[] {
  return Object.keys(obj).map(key => obj[key]);
}

export function sleep(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function isPalindrome(str: string): boolean {
  const normalizedStr = str.toLowerCase().replace(/[^a-zA-Z0-9]/g, '');
  const reversedStr = normalizedStr.split('').reverse().join('');
  return normalizedStr === reversedStr;
}


//Mat Table Common used methods

export function applyFilter<T>(dataSource: MatTableDataSource<T>, filterValue: string): void {
  filterValue = filterValue.trim().toLowerCase();
  dataSource.filter = filterValue;
}

export function filterByProperty<T>(array: T[], property: keyof T, filterValue: string): T[] {
  return array.filter(item => {
    const value = item[property]?.toString().toLowerCase();
    return value && value.includes(filterValue.toLowerCase());
  });
}

export function filterByMultipleProperties<T>(array: T[], properties: (keyof T)[], filterValue: string): T[] {
  return array.filter(item => {
    return properties.some(prop => {
      const value = item[prop]?.toString().toLowerCase();
      return value && value.includes(filterValue.toLowerCase());
    });
  });
}

export function paginate<T>(array: T[], page: number, pageSize: number): T[] {
  const startIndex = (page - 1) * pageSize;
  return array.slice(startIndex, startIndex + pageSize);
}

export function sortDataSource<T>(dataSource: MatTableDataSource<T>, property: keyof T, ascending: boolean = true): void {
  dataSource.data = dataSource.data.sort((a, b) => {
    if (a[property] === b[property]) {
      return 0;
    }
    return (a[property] < b[property] ? -1 : 1) * (ascending ? 1 : -1);
  });
}

export function filterByDateRange<T>(array: T[], startDate: any, endDate: any, dateProperty: keyof T): T[] {
  return array.filter((item : any) => {
    const itemDate = new Date(item[dateProperty]);
    return itemDate >= startDate && itemDate <= endDate;
  });
}

export function filterByCheckbox<T>(array: T[], checkboxProperty: keyof T, isChecked: boolean): T[] {
  return array.filter(item => {
    return !!item[checkboxProperty] === isChecked;
  });
}

export function filterByDropdown<T>(array: T[], dropdownProperty: keyof T, selectedValue: any): T[] {
  return array.filter(item => {
    return item[dropdownProperty] === selectedValue;
  });
}

export function clearTableFilter<T>(dataSource: MatTableDataSource<T>): void {
  dataSource.filter = '';
}

export function resetTableSort<T>(dataSource: MatTableDataSource<T>): void {
  if (dataSource.sort) {
    dataSource.sort.active = '';
    dataSource.sort.direction = 'asc';
  }
}

export function resetTablePaginator(paginator: MatPaginator): void {
  paginator.firstPage();
}