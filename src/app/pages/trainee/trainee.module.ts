import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TraineeRoutingModule } from './trainee-routing.module';
import { TraineeComponent } from './trainee.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { BmiComponent } from './bmi/bmi.component';
import { AuthInterceptorService } from 'src/app/service/auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarouselModule } from 'ngx-owl-carousel-o';



@NgModule({
  declarations: [
  
    TraineeComponent,
    BmiComponent
  ],
  imports: [
    CommonModule,
    TraineeRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule
  ],
  providers: [
    {
      multi: true,
      useClass: AuthInterceptorService,
      provide: HTTP_INTERCEPTORS,
    },
  ],  
})
export class TraineeModule { }
