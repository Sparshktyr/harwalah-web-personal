import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.css']
})
export class BmiComponent {

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }
  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob || 
      !this.localSignUpData.reasons ||
      !this.localSignUpData.goals ||
      !this.localSignUpData.fitness_level
    ){
        window.history.back();
    }
  }

  // step_seven_form:FormGroup = new FormGroup({
  //   weight:new FormControl('',Validators.required),
  //   height:new FormControl('',Validators.required),
  // })

  // getError(value:any){
    // // if(this.submitted &&  this.step_seven_form.controls[value].errors){
    //   return true;
    // }
  //   return false
  // }

  error:String="";
  submitted:Boolean = false;
  submit(){
    // this.submitted = true;
    // // if(this.step_seven_form.invalid){
    // //   return;
    // // }

    // this.localSignUpData.weight = this.step_seven_form.value.weight;
    // this.localSignUpData.height = this.step_seven_form.value.height;

    // this.router.navigateByUrl('/trainee/create-account/step-7-1').then(()=>{
    //   this.localstorage.setSignupLocal(this.localSignUpData);
    // });
    this.router.navigateByUrl('/home').then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }
}
