import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-five',
  templateUrl: './step-five.component.html',
  styleUrls: ['./step-five.component.css']
})
export class StepFiveComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob || 
      !this.localSignUpData.reasons
    ){
        window.history.back();
    }
  }


  isCheckStepFive(value:String){
    return this.goals.includes(value);
  }
  checkedStepFive(value:String){
    let index = this.goals.indexOf(value);
    if(index == -1){
      this.goals.push(value);
    }else{
      this.goals.splice(index,1);
    }
  }
  goals:String[] = [];

  error:String="";
  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(this.goals.length == 0){
      return;
    }

    // console.log(this.step_three_form.value);

    this.localSignUpData.goals = this.goals.toString();
    // console.log(this.localSignUpData);
  
    this.router.navigateByUrl('/trainee/create-account/step-6').then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }


}
