import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-seven',
  templateUrl: './step-seven.component.html',
  styleUrls: ['./step-seven.component.css']
})
export class StepSevenComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob || 
      !this.localSignUpData.reasons ||
      !this.localSignUpData.goals ||
      !this.localSignUpData.fitness_level
    ){
        window.history.back();
    }
  }

  step_seven_form:FormGroup = new FormGroup({
    weight:new FormControl('',Validators.required),
    height:new FormControl('',Validators.required),
  })

  getError(value:any){
    if(this.submitted &&  this.step_seven_form.controls[value].errors){
      return true;
    }
    return false
  }

  error:String="";
  submitted:Boolean = false;

  submit(){
    debugger;
    this.submitted = true;
    if(this.step_seven_form.invalid){
      return;
    }

    this.localSignUpData.weight = this.step_seven_form.value.weight;
    this.localSignUpData.height = this.step_seven_form.value.height;
    this.localSignUpData.device_type = "3";
    this.localSignUpData.device_token = "abcd";
    // this.router.navigateByUrl('/trainee/create-account/step-7-1').then(()=>{
    //   this.localstorage.setSignupLocal(this.localSignUpData);
    // });

    this.api.user_post('register', this.localSignUpData).subscribe({
      next: (res: any) => {
        console.log(res);
        if(res && !res.error){
          this.router.navigateByUrl("/trainee/step-7-2").then(()=>{
            // this.localstorage.setSignupLocal(this.localSignUpData);
            this.localstorage.removeSignupLocal();
            this.localstorage.setWebLocal(res.data);
            // this.dataSharedService.setLoginData(JSON.stringify(res.data));
            // this.router.navigateByUrl('/home');
          });
        }else{
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });

    this.router.navigateByUrl('/trainee/step-7-2').then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }

  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }
  
}
