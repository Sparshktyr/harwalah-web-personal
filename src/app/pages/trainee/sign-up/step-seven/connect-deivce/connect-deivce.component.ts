import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-connect-deivce',
  templateUrl: './connect-deivce.component.html',
  styleUrls: ['./connect-deivce.component.css']
})
export class ConnectDeivceComponent  implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob || 
      !this.localSignUpData.reasons ||
      !this.localSignUpData.goals || 
      !this.localSignUpData.weight ||
      !this.localSignUpData.height
    ){
        window.history.back();
    }
  }

  connect_device:Number=0;

  error:String="";
  submit(){
    debugger
    this.localSignUpData.connect_device = this.connect_device;
    this.localSignUpData.device_type = "3";
    this.localSignUpData.device_token = "abcd";

    // return;
    this.api.user_post('register', this.localSignUpData).subscribe({
      next: (res: any) => {
        console.log(res);
        
        if(res && !res.error){
          this.router.navigateByUrl("/trainee/step-7-2").then(()=>{
            // this.localstorage.setSignupLocal(this.localSignUpData);
            this.localstorage.removeSignupLocal();
            this.localstorage.setWebLocal(res.data);
            // this.dataSharedService.setLoginData(JSON.stringify(res.data));
            this.router.navigateByUrl('/home');



          });
        }else{
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });


  }
}
