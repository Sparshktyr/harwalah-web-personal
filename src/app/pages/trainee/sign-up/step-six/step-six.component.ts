import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-six',
  templateUrl: './step-six.component.html',
  styleUrls: ['./step-six.component.css']
})
export class StepSixComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob || 
      !this.localSignUpData.reasons ||
      !this.localSignUpData.goals
    ){
        window.history.back();
    }
  }

  fitness_level:String="";

  error:String="";
  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(!this.fitness_level){
      return;
    }

    this.localSignUpData.fitness_level = this.fitness_level;
  
    this.router.navigateByUrl('/trainee/create-account/step-7').then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }
}
