import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.css']
})
export class StepTwoComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password
    ){
        window.history.back();
    }
  }

  otp:String = "";
  error:String="";
  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(!this.otp){
      return;
    }
    let data = {
      "email":this.localSignUpData.email,
      "otp":this.otp
    }

    // this.router.navigateByUrl('/trainee/create-account/step-3').then(()=>{
    //   this.localSignUpData.otp = this.otp;
    //   this.localstorage.setSignupLocal(this.localSignUpData);
    // });
    // return;

    this.api.user_post('verify/otp', data).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.router.navigateByUrl('/trainee/create-account/step-3').then(()=>{
            this.localSignUpData.otp = this.otp;
            this.localstorage.setSignupLocal(this.localSignUpData);
          });
        }else{
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });

  }

  successMessage:any;
  resendOtp(){
    let data = {
      "email":this.localSignUpData.email
    }
    this.api.user_post('send/otp', data).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.successMessage = "Otp Send Success."
          setTimeout(() => {
          this.successMessage = "";
          }, 4000);
        }else{
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });
  }

  
  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }
  
}
