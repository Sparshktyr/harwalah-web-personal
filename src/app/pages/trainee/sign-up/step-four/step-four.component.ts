import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.css']
})
export class StepFourComponent implements OnInit {
  localSignUpData: any;
  reason: string | null = null; // Initialize to null
  error: string = "";
  submitted: boolean = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) { }

  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if (
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender ||
      !this.localSignUpData.dob
    ) {
      window.history.back();
    }
  }

  isCheckStepFour(value: string) {
    return this.reason === value;
  }

  checkedStepFour(value: string) {
    if (this.reason === value) {
      this.reason = null;
    } else {
      this.reason = value; 
    }
  }

  submit() {
    this.submitted = true;
    if (!this.reason) {
      return;
    }

    this.localSignUpData.reasons = this.reason;
    this.router.navigateByUrl('/trainee/create-account/step-5').then(() => {
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
  }
}
