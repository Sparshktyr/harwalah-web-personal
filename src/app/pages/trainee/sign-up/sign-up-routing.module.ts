import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignUpComponent } from './sign-up.component';
import { StepOneComponent } from './step-one/step-one.component';
import { StepTwoComponent } from './step-two/step-two.component';
import { StepThreeComponent } from './step-three/step-three.component';
import { StepSevenComponent } from './step-seven/step-seven.component';
import { StepSixComponent } from './step-six/step-six.component';
import { StepFiveComponent } from './step-five/step-five.component';
import { StepFourComponent } from './step-four/step-four.component';
import { ConnectDeivceComponent } from './step-seven/connect-deivce/connect-deivce.component';

const routes: Routes = [
  {path:'',component:SignUpComponent,children:[
    {path:'',redirectTo:'step-1',pathMatch:'full'},
    {path:'step-1',component:StepOneComponent},
    {path:'step-2',component:StepTwoComponent},
    {path:'step-3',component:StepThreeComponent},
    {path:'step-4',component:StepFourComponent},
    {path:'step-5',component:StepFiveComponent},
    {path:'step-6',component:StepSixComponent},
    {path:'step-7',component:StepSevenComponent},
    {path:'step-7-1',component:ConnectDeivceComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignUpRoutingModule { }
