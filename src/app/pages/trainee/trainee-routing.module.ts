import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoachComponent } from '../coach/coach.component';
import { TraineeComponent } from './trainee.component';
import { BmiComponent } from './bmi/bmi.component';


const routes: Routes = [
   {path:'',component:TraineeComponent,children:[
    {path:'create-account',loadChildren:()=>import('./sign-up/sign-up.module').then(s=>s.SignUpModule)},
    {path:'step-7-2',component:BmiComponent}
    ]},
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TraineeRoutingModule { }
