import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TraineeLayoutRoutingModule } from './trainee-layout-routing.module';
import { TraineeLayoutComponent } from './trainee-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionPnlComponent } from './subscription-pnl/subscription-pnl.component';
import { SubscriptionListComponent } from './subscription-list/subscription-list.component';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { SuccessfullyPageComponent } from './successfully-page/successfully-page.component';
import { GraphComponent } from './dashboard/graph/graph.component';
import { SliderBannerComponent } from './dashboard/slider-banner/slider-banner.component';
import { SliderCardsComponent } from './dashboard/slider-cards/slider-cards.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WorkoutsComponent } from './workouts/workouts.component';
import { WorkoutDetailsComponent } from './workouts/workout-details/workout-details.component';
import { MusicDialogComponent } from './workouts/music-dialog/music-dialog.component';
import { RunningPageComponent } from './running-page/running-page.component';
import { ViewRunningComponent } from './running-page/view-running/view-running.component';
import { PipesModule } from 'src/app/pipes/pipes/pipes.module';
import { FoodRecipesComponent } from './food-recipes/food-recipes.component';
import { NutritionDietComponent } from './nutrition-diet/nutrition-diet.component';
import { CoachTrainerComponent } from './coach-trainer/coach-trainer.component';
import { ChallengePageComponent } from './challenge-page/challenge-page.component';
import { ChallengeDetailsComponent } from './challenge-page/challenge-details/challenge-details.component';
import { CoachDetailsComponent } from './coach-trainer/coach-details/coach-details.component';
import { GroupsComponent } from './groups/groups.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { AddProfileComponent } from './profile-info/add-profile/add-profile.component';
import { HealthStatusComponent } from './profile-info/health-status/health-status.component';
import { GroupDetailsComponent } from './groups/group-details/group-details.component';
import { GroupChatComponent } from './groups/group-chat/group-chat.component';
import { ReviewDialogComponent } from './coach-trainer/review-dialog/review-dialog.component';
import { TraineeGroupChatComponent } from './groups/trainee-group-chat/trainee-group-chat.component';
import { AddNutritionComponent } from './nutrition-diet/add-nutrition/add-nutrition.component';
import { CustomerDialogComponent } from './nutrition-diet/customer-dialog/customer-dialog.component';
import { RecipesDetailsComponent } from './food-recipes/recipes-details/recipes-details.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BlogComponent } from './blog/blog.component';
import { BlogDetailsComponent } from './blog/blog-details/blog-details.component';
import { CommunityComponent } from './community/community.component';
import { SeeAllComponent } from './community/see-all/see-all.component';
import { CreateGroupComponent } from './community/create-group/create-group.component';
import { JoinGroupComponent } from './community/join-group/join-group.component';
import { MemberListDialogComponent } from './community/member-list-dialog/member-list-dialog.component';
import { LeaveGroupDialogComponent } from './community/leave-group-dialog/leave-group-dialog.component';
import { WritePostComponent } from './community/write-post/write-post.component';
import { MyGroupPostComponent } from './community/my-group-post/my-group-post.component';
import { CalenderComponent } from './calender/calender.component';
import { FullCalendarModule } from '@fullcalendar/angular';


@NgModule({
  declarations: [
    TraineeLayoutComponent,
    DashboardComponent,
    SubscriptionPnlComponent,
    SubscriptionListComponent,
    PaymentMethodComponent,
    SuccessfullyPageComponent,
    GraphComponent,
    SliderBannerComponent,
    SliderCardsComponent,
    WorkoutsComponent,
    WorkoutDetailsComponent,
    MusicDialogComponent,
    RunningPageComponent,
    ViewRunningComponent,
    FoodRecipesComponent,
    NutritionDietComponent,
    CoachTrainerComponent,
    ChallengePageComponent,
    ChallengeDetailsComponent,
    CoachDetailsComponent,
    GroupsComponent,
    ProfileInfoComponent,
    AddProfileComponent,
    HealthStatusComponent,
    GroupDetailsComponent,
    GroupChatComponent,
    ReviewDialogComponent,
    TraineeGroupChatComponent,
    AddNutritionComponent,
    CustomerDialogComponent,
    RecipesDetailsComponent,
    BlogComponent,
    BlogDetailsComponent,
    CommunityComponent,
    SeeAllComponent,
    CreateGroupComponent,
    JoinGroupComponent,
    MemberListDialogComponent,
    LeaveGroupDialogComponent,
    WritePostComponent,
    MyGroupPostComponent,
    CalenderComponent
  ],
  imports: [
    CommonModule,
    TraineeLayoutRoutingModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    CarouselModule,
    FullCalendarModule

  ]
})
export class TraineeLayoutModule { }
