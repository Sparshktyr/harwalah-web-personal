import { Component } from '@angular/core';
import { formatDate } from '../../../../utils/common-function'
import { DataSharedService } from 'src/app/service/data-shared.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { ApiService } from 'src/app/service/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.css']
})
export class PaymentMethodComponent {
  panelOpenState = false;

  subscriptionData: any;
  localStorageData: any;

  constructor(
    private dataService: DataSharedService,
    private localStorage: SessionStorageService,
    private api: ApiService, private router: Router,
  ) { }

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocal()
    this.dataService.getMessage.subscribe((data: any) => {
      this.subscriptionData = data;
      console.log(this.subscriptionData)
      if (!this.subscriptionData) {
        window.history.back();
      } else {
        this.subscriptionData.show_value = this.subscriptionData.discount_price;
      }
    });

  }

  package_type: any = {
    '1': "1 month",
    '2': "6 month",
    '3': "12 month"
  }
  package_type_month: any = {
    '1': 1,
    '2': 6,
    '3': 12
  }


  validateCoupon(promocode: string, package_amount: string, token: string, user_id: number) {
    return new Promise((resolve, reject) => {
      let data = {
        "token": token,
        "user_id": `${user_id}`,
        "promocode": promocode,
        "package_amount": package_amount
      }
      this.api.user_post('coupon/validate', data).subscribe({
        next: (res: any) => {
          if (res.code == 200) {
            resolve(res['data'])
            // alert('Payment Done.')
            // this.router.navigateByUrl(routerLink)
          } else {
            reject({
              error: true,
              message: "Invalid promocode"
            })
          }
        },
        error: (err: any) => {
          reject(err);
        }
      })
    })
  }

  couponCode: string = "";
  couponApplied: Boolean = false;
  errorCouponMessage: string = '';
  promocodeData: any;
  applyCoupen() {
    debugger
    if (
      this.subscriptionData &&
      this.subscriptionData.price &&
      this.localStorageData &&
      this.localStorageData.login_token &&
      this.localStorageData.user_id
    ) {
      this.validateCoupon(
        this.couponCode,
        this.subscriptionData.price,
        this.localStorageData.login_token,
        this.localStorageData.user_id
      ).then((result: any) => {
        debugger
        this.couponApplied = true;
        this.promocodeData = result;
        if (this.promocodeData.type == "percentage") {
          let discountPrice = Number(this.subscriptionData.show_value) * Number(this.promocodeData.type_value) / 100;
          if (discountPrice > Number(this.promocodeData.maximum_discount)) {
            this.subscriptionData.show_value = this.subscriptionData.show_value - this.promocodeData.maximum_discount;
          } else {
            this.subscriptionData.show_value = this.subscriptionData.show_value - discountPrice;
          }
        } else {
          this.subscriptionData.show_value = this.subscriptionData.show_value - this.promocodeData.maximum_discount;
        }
      }).catch((error: any) => {
        if (error && error.error) {
          this.errorCouponMessage = error.message;
          setTimeout(() => {
            this.errorCouponMessage = "";
          }, 3000);
        }
        this.couponCode = "";
        this.couponApplied = false;
      })
    }
  }

  removeCoupon() {
    this.couponApplied = false;
    this.couponCode = "";
    this.subscriptionData.show_value = this.subscriptionData.discount_price;
  }

  makePaymentRequestError: String = "";
  makePaymentRequest() {
    const routerLink = "/trainee-layout/successfully-page"
    let data = {
      "token": this.localStorageData.login_token,
      "user_id": this.localStorageData.user_id,
      "package_id": this.subscriptionData.package_id,
      "purchase_date": formatDate(new Date(), 0),
      "expiry_date": formatDate(new Date(), this.package_type_month[this.subscriptionData.package_type]),
      "amount": this.subscriptionData.show_value,
      "transaction_id": "ghtyu6765ty567ty"
    }
    this.api.user_post('packages/purchase', data).subscribe({
      next: (res: any) => {
        if (res.code == 200) {
          // alert('Payment Done.')
          this.getUserProfileData(
            this.localStorageData.login_token,
            this.localStorageData.user_id
          ).then((profileDetail:any)=>{
            if(profileDetail && profileDetail[0] && profileDetail[0].plan){
              this.localStorageData.plan = profileDetail[0].plan;
            }
            this.router.navigateByUrl(routerLink).then(() => {
              this.localStorage.setWebLocal(this.localStorageData);
              this.dataService.setLoginData(JSON.stringify(this.localStorageData));
              this.dataService.setMessage(this.subscriptionData);
            })  
          }).catch(()=>{
            this.router.navigateByUrl(routerLink).then(() => {
              // this.localStorage.setWebLocal(this.localStorageData);
              // this.dataService.setLoginData(JSON.stringify(this.localStorageData));
              this.dataService.setMessage(this.subscriptionData);
              // this.dataService.setMessage(this.subscriptionData);
            })
          })
          // this.router.navigateByUrl(routerLink).then(() => {
          //   this.dataService.setMessage(this.subscriptionData);
          // })
        } else {
          this.makePaymentRequestError = res.message;
          setTimeout(() => {
            this.makePaymentRequestError = "";
          }, 3000);
        }
      },
    })
  }

  
  getUserProfileData(
    token:String,
    user_id:String
  ){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "user_id":user_id
      }
      this.api.user_post('profile', data).subscribe({
          next: (res: any) => {
            if(res && !res.error){
              resolve(res.data)
            }else{
              reject(res)
            }
          },
          error: (err: any) => {
              reject(err)
          }
      });  
    })
  }

}
