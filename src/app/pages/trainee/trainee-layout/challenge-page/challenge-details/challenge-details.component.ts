import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OwlOptions } from "ngx-owl-carousel-o";
import { ApiService } from "src/app/service/api.service";
import { DataSharedService } from "src/app/service/data-shared.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-challenge-details",
  templateUrl: "./challenge-details.component.html",
  styleUrls: ["./challenge-details.component.css"],
})
export class ChallengeDetailsComponent {
  type: any;
  localStorageData: any;
  coachDetails: any;
  challengeId: any;
  challengesDetail: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService,
    private route: ActivatedRoute,
    private dataService: DataSharedService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.type = params["type"];
      this.challengeId = params["challengeId"];
    });

    this.localStorageData = this.localStorage.getWebLocal();
    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.getCoachDetails(
        this.localStorageData.login_token,
        this.localStorageData?.user_id,
        this.challengeId
      );
    }
  }

  getCoachDetails(token: string, user_id: string, challengeId: any) {
    let data = {
      token: token,
      user_id: user_id,
      challenge_id: challengeId,
    };
    this.api.user_post("challenges/detail", data).subscribe({
      next: (res: any) => {
        this.challengesDetail = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  joinChallenges(){
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData?.user_id,
      challenge_id: this.challengeId,
    };
    this.api.user_post("challenges/join", data).subscribe({
      next: (res: any) => {
        this.router.navigateByUrl('/trainee-layout/challenges')
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  challengesBanner: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      740: {
        items: 1,
      },
      940: {
        items: 1,
      },
    },
    nav: false,
  };
}
