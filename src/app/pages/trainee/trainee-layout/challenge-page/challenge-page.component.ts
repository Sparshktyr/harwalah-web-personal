import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-challenge-page",
  templateUrl: "./challenge-page.component.html",
  styleUrls: ["./challenge-page.component.css"],
})
export class ChallengePageComponent {
  localStorageData: any;
  invitedChallenges: any;
  moreChallenges: any;
  recentlyAddedChallenges: any;
  pastChallenges: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    console.log(this.localStorageData);

    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.getInvitedChallengesList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
      this.getChallenges(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
      this.myChallenges(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
    }
  }

  getInvitedChallengesList(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("coach/challenges", data).subscribe({
      next: (res: any) => {
        this.invitedChallenges = res.data;
        // this.invitedChallenges = [
        //   {
        //     "challenge_id": 2,
        //     "name": "Sorta challenge",
        //     "name_ar": "egnellahC atroS",
        //     "type": "steps",
        //     "challenge_by": "admin",
        //     "coach_id": null,
        //     "start_date": "2024-12-10",
        //     "end_date": "2024-12-20",
        //     "duration": 10,
        //     "daily_steps_goal": 4000,
        //     "ultimate_goal": 70000,
        //     "registration_start_date": "2024-12-01",
        //     "registration_end_date": "2024-12-07",
        //     "external_registration_link": "",
        //     "location": "",
        //     "city": null,
        //     "description": "Cresta a challenge",
        //     "description_ar": "Cresta a challenge",
        //     "is_paid": 1,
        //     "status": 1,
        //     "registration_price": 200,
        //     "image1": "",
        //     "image2": null,
        //     "image3": null,
        //     "image4": null,
        //     "image5": null,
        //     "created_at": "06-09-2024 22:25 PM",
        //     "updated_at": "12-11-2024 23:41 PM",
        //     "deleted_at": null,
        //     "participants_count": 0
        //   }
        // ];
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  
  getChallenges(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("challenges", data).subscribe({
      next: (res: any) => {
        this.moreChallenges = res.data;
        // this.moreChallenges = [
        //   {
        //     "challenge_id": 2,
        //     "name": "Sorta challenge",
        //     "name_ar": "egnellahC atroS",
        //     "type": "steps",
        //     "challenge_by": "admin",
        //     "coach_id": null,
        //     "start_date": "2024-12-10",
        //     "end_date": "2024-12-20",
        //     "duration": 10,
        //     "daily_steps_goal": 4000,
        //     "ultimate_goal": 70000,
        //     "registration_start_date": "2024-12-01",
        //     "registration_end_date": "2024-12-07",
        //     "external_registration_link": "",
        //     "location": "",
        //     "city": null,
        //     "description": "Cresta a challenge",
        //     "description_ar": "Cresta a challenge",
        //     "is_paid": 1,
        //     "status": 1,
        //     "registration_price": 200,
        //     "image1": "",
        //     "image2": null,
        //     "image3": null,
        //     "image4": null,
        //     "image5": null,
        //     "created_at": "06-09-2024 22:25 PM",
        //     "updated_at": "12-11-2024 23:41 PM",
        //     "deleted_at": null,
        //     "participants_count": 0
        //   }
        // ];
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  myChallenges(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("challenges", data).subscribe({
      next: (res: any) => {
        this.recentlyAddedChallenges = res.data.recently_joined;
        this.pastChallenges = res.data.past;
        // this.recentlyAddedChallenges = [
        //   {
        //     "challenge_id": 2,
        //     "name": "Sorta challenge",
        //     "name_ar": "egnellahC atroS",
        //     "type": "steps",
        //     "challenge_by": "admin",
        //     "coach_id": null,
        //     "start_date": "2024-12-10",
        //     "end_date": "2024-12-20",
        //     "duration": 10,
        //     "daily_steps_goal": 4000,
        //     "ultimate_goal": 70000,
        //     "registration_start_date": "2024-12-01",
        //     "registration_end_date": "2024-12-07",
        //     "external_registration_link": "",
        //     "location": "",
        //     "city": null,
        //     "description": "Cresta a challenge",
        //     "description_ar": "Cresta a challenge",
        //     "is_paid": 1,
        //     "status": 1,
        //     "registration_price": 200,
        //     "image1": "",
        //     "image2": null,
        //     "image3": null,
        //     "image4": null,
        //     "image5": null,
        //     "created_at": "06-09-2024 22:25 PM",
        //     "updated_at": "12-11-2024 23:41 PM",
        //     "deleted_at": null,
        //     "participants_count": 1
        //   }
        // ];
        // this.pastChallenges= [
        //   {
        //     "challenge_id": 1,
        //     "coach_id": 1,
        //     "name": "New free challenge",
        //     "type": "steps",
        //     "start_date": "2024-08-30",
        //     "end_date": "2024-09-10",
        //     "duration": 10,
        //     "daily_steps_goal": 100,
        //     "ultimate_goal": 50,
        //     "registration_start_date": "2024-08-28",
        //     "registration_end_date": "2024-08-29",
        //     "external_registration_link": "http://jhjhvhjv",
        //     "location": "148 local bagh, near metro bhawan, Noida",
        //     "city": "Noida",
        //     "description": "test description",
        //     "is_paid": 1,
        //     "status": 1,
        //     "registration_price": 100,
        //     "image1": "",
        //     "image2": null,
        //     "image3": null,
        //     "image4": null,
        //     "image5": null,
        //     "created_at": "27-08-2024 23:10 PM",
        //     "updated_at": "29-08-2024 22:12 PM",
        //     "deleted_at": null,
        //     "participants_count": 1
        //   }
        // ];
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  goToChallengesDetails(type: any, challengeId: any) {
    // routerLink="/trainee-layout/challenges-details"
    this.router.navigateByUrl(
      `/trainee-layout/challenges-details/${type}/${challengeId}`
    );
  }
}
