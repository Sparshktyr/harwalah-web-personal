import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  localStorageData: any;
  coachList: any;
  myCoachList: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService
  ) {}
  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    console.log(this.localStorageData);

    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.getCoachList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
      this.getMyCoachList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      )
    }
  }
  getCoachList(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("coach/list", data).subscribe({
      next: (res: any) => {
        this.coachList = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  getMyCoachList(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("subscribe/coach/list", data).subscribe({
      next: (res: any) => {
        this.myCoachList = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  goToCoachDetailPage(id : any, type : any){
    // type 1 for subscribed 2 for unsubscribed 
    console.log(id)
    this.router.navigateByUrl(`/trainee-layout/coach-details/${id}/${type}`)
  }
}
