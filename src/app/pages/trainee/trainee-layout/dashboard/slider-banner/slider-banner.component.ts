import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-slider-banner',
  templateUrl: './slider-banner.component.html',
  styleUrls: ['./slider-banner.component.css']
})

export class SliderBannerComponent implements OnInit {
  images: string[] = [
    '../../../../../../assets/images/bestCoach/1.jpg',
    '../../../../../../assets/images/bestCoach/2.jpg',
    '../../../../../../assets/images/bestCoach/3.jpg'
  ];

  carouselOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    items: 1,
    nav: true,
    navText: ['<', '>']
  };

  constructor() { }

  ngOnInit(): void {
  }
}
