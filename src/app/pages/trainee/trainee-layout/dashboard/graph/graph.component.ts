import { Component } from '@angular/core';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent {
  chartType = 'bar';
  chartData = [{ data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }];
  chartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  chartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
}
