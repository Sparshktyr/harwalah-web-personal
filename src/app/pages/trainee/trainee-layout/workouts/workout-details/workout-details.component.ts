import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MusicDialogComponent } from '../music-dialog/music-dialog.component';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-workout-details',
  templateUrl: './workout-details.component.html',
  styleUrls: ['./workout-details.component.css']
})
export class WorkoutDetailsComponent {
  localStorageData: any;
  workoutId: any;
  data: any;
  constructor(
    public dialog: MatDialog,
    private localStorage: SessionStorageService,
    private api : ApiService,
    private activatedRoute: ActivatedRoute
    ) {}
  musicDialog(){
    this.dialog.open(MusicDialogComponent,{
      width:'400px',
    })
  }
  ngOnInit(){
    this.localStorageData = this.localStorage.getWebLocal();
    this.workoutId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAdminWorkOutDetails()
  }
  getAdminWorkOutDetails() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      workout_id : this.workoutId
    };
    this.api.user_post("workouts/admin/detail", data).subscribe({
      next :(res :any)=> {
        this.data = res.data[0]
      },
    });
  }
  formatTime(seconds:any) {
    let minutes = Math.floor(seconds / 60);
    let remainingSeconds = seconds % 60;
    return `${minutes.toString().padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}`;
  }
}
