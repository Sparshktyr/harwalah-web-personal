import { Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { SecondsToMinutesPipe } from "src/app/pipes/seconds-to-minutes.pipe";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-workouts",
  templateUrl: "./workouts.component.html",
  styleUrls: ["./workouts.component.css"],
  providers: [SecondsToMinutesPipe],
})
export class WorkoutsComponent {
  localStorageData: any;
  formattedDuration: any;
  adminWorkoutData: any;
  searchData: any;
  levelData: any;
  equipmentData: any;
  frontData: any;
  backData: any;
  searchForm : FormGroup
  constructor(
    private api: ApiService,
    private localStorage: SessionStorageService,
    private route: Router,
    private fb : FormBuilder
  ) {}
  ngOnInit() {
    this.searchForm = this.fb.group({
      level_id: [''],
      equipment_id: [''],
      front_muscle_id: [''],
      back_muscle_id: ['']
    });
    this.localStorageData = this.localStorage.getWebLocal();
    this.getAdminWorkOutList();
    this.getLevelList();
    this.getEquipmentsList();
    this.getFrontMuscelsList();
    this.getbackMuscelsList();
  }
  getAdminWorkOutList() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
    };
    this.api.user_post("workouts/admin", data).subscribe({
      next: (res: any) => {
        this.adminWorkoutData = res.data;
      },
    });
  }
  search(event: any) {
    this.searchData = event.target.value;
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      name: this.searchData,
    };
    this.api.user_post("workouts/admin", data).subscribe({
      next: (res: any) => {
        this.adminWorkoutData = res.data;
      },
    });
  }

  getLevelList() {
    this.api.common_api_post("levels", "").subscribe({
      next: (res: any) => {
        this.levelData = res.data;
      },
    });
  }
  getEquipmentsList() {
    this.api.common_api_post("equipments", "").subscribe({
      next: (res: any) => {
        this.equipmentData = res.data;
      },
    });
  }

  getFrontMuscelsList() {
    this.api.common_api_post("front", "").subscribe({
      next: (res: any) => {
        this.frontData = res.data;
      },
    });
  }

  getbackMuscelsList() {
    this.api.common_api_post("back", "").subscribe({
      next: (res: any) => {
        this.backData = res.data;
      },
    });
  }

  applyFilter(): void {
    const payload = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      level_id: this.searchForm.value.level_id,
      equipment_id: this.searchForm.value.equipment_id,
      front_muscle_id: this.searchForm.value.front_muscle_id,
      back_muscle_id: this.searchForm.value.back_muscle_id
    };

    this.api.user_post('workouts/admin', payload).subscribe((response:any) => {
      this.adminWorkoutData = response.data;
    });
  }

  clickToNavigate(id: any) {
    this.route.navigateByUrl(`trainee-layout/workout-details/${id}`);
  }

  formatTime(seconds:any) {
    let minutes = Math.floor(seconds / 60);
    let remainingSeconds = seconds % 60;
    return `${minutes.toString().padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}`;
  }
}
