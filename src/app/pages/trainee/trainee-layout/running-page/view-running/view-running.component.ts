import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-view-running',
  templateUrl: './view-running.component.html',
  styleUrls: ['./view-running.component.css']
})
export class ViewRunningComponent {
  
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) {
    // this.workoutData = this.router.getCurrentNavigation()?.extras?.state;
  }
  localStorageData: any;
  runningData: any;
  running_id:string="";
  is_self:boolean = false;
  

  running_type:any = {
    1 : "Easy Running",
    2 : "Quality Session",
    3 : "Race"
  }
  ngOnInit(): void {
    // token
    // user_id
    // running_id
    this.activeRoute.queryParams.subscribe({
      next: (res: any) => {
        this.running_id = res?.running_id;
        if (!this.running_id) {
          // this.router.navigate(['/coach-layout/my-members'])
          return
        } else {
          //  debugger
          this.is_self = res?.is_self;
          
          this.localStorageData = this.localstorage.getWebLocal();
          console.log(this.localStorageData);
          
          if (
            this.localStorageData.login_token,
            this.localStorageData?.user_id
          ) {
            // console.log(this.workoutData);
            this.RunningDetail(
              this.localStorageData.login_token,
              this.localStorageData.user_id,
              this.running_id
            );
          }
        }
      }
    })
  }

  RunningDetail(
    token:string,
    user_id:string,
    running_id:string,
  ) {
    this.api.user_post("running/admin/detail", {
      token, user_id,running_id
    }).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          if(res.data && res.data){
            if(res.data && res.data[0]){
              this.runningData = res.data[0];
              console.log(this.runningData)
              if(this.is_self){
                // this.selectedDate = new Date(this.runningData?. ||  "");
              } 
            }
          }
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  minDate = new Date();
  selectedDate = <Date | null>(null)
  isCalenderShow:boolean = false;
  addToCalender(){
    this.isCalenderShow = !this.isCalenderShow;
  }

  dateChange(){
    if(!this.selectedDate){
      return;
    }
    let date = `${this.selectedDate.getDate()}`;
    let month = `${this.selectedDate.getMonth()+1}`;
    let year = `${this.selectedDate.getFullYear()}`;
    if(date.length == 1){
      date = `0${date}` 
    }
    if(month.length == 1){
      month = `0${month}`
    }
    
    this.api.user_post('running/add',{
        token:this.localStorageData.login_token,
        user_id:this.localStorageData?.user_id,
        admin_running_id:this.running_id,
        running_date:`${date}-${month}-${year}`
    }).subscribe((res:any)=>{
      // console.log(res);
      this.router.navigate(["trainee-layout/running"],{
        queryParams:{
          running_id:this.running_id
        }
      })
    })

    
  }


}
