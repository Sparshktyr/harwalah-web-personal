import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-running-page',
  templateUrl: './running-page.component.html',
  styleUrls: ['./running-page.component.css']
})
export class RunningPageComponent {


  constructor(
    private api : ApiService,
    private router : Router,
    private localStorage : SessionStorageService,
  ){}

  ngOnInit(){
    this.localStorageData = this.localStorage.getWebLocal()
    console.log(this.localStorageData);
    
    if(
      this.localStorageData.login_token,
      this.localStorageData?.user_id
    ){
      this.adminRunningDataList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      )
      this.userRunningDataList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      )
    }
  }
  runningList: any[]=[] ;
  localStorageData: any;

  nameSearch:string = "";
  running_type:string = "";

  setTimeOutRunningData:any;
  searchByName(){
    if(this.setTimeOutRunningData){
      clearTimeout(this.setTimeOutRunningData);
    }
    this.setTimeOutRunningData = setTimeout(() => {
      this.adminRunningDataList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id,
        this.nameSearch,
        this.running_type
      );
    }, 2000);
  }

  applyFilter(){
    
      this.adminRunningDataList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id,
        this.nameSearch,
        this.running_type
      );

    }

  adminRunningDataList(
    token:string,
    user_id:string,
    name:string = "",
    running_type:string = ""
  ){
    let data = {
      "token":token,
      "user_id":user_id,
      "name":name,
      "running_type":running_type
    }
    this.api.user_post('running/admin', data).subscribe({
      next : (res : any)=>{
        this.runningList = res.data;
      }, 
      error: (err : any)=> {
        console.log(err)
      },
    })
  }

  user_runningList:any [] = [];
  userRunningDataList(
    token:string,
    user_id:string,
    name:string = "",
    running_type:string = ""
  ){
    let data = {
      "token":token,
      "user_id":user_id,
      "name":name,
      "running_type":running_type
    }
    this.api.user_post('running', data).subscribe({
      next : (res : any)=>{
        this.user_runningList = res.data;
      }, 
      error: (err : any)=> {
        console.log(err)
      },
    })
  }

  goToDetail(element:any,is_self:boolean = false){
    if(!element?.running_id){
      return;
    }
    let routerLink="/trainee-layout/running-view";
    this.router.navigate([routerLink],{
      queryParams:{
        running_id:element.running_id,
        is_self:is_self
      }
    })
  }

}
