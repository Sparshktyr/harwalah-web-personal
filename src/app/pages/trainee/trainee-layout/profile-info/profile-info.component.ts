import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddProfileComponent } from './add-profile/add-profile.component';
import { HealthStatusComponent } from './health-status/health-status.component';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { DataSharedService } from 'src/app/service/data-shared.service';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent {
  localStorageData: any;
constructor(
  public dialog:MatDialog,  private api: ApiService,
  private router: Router,
  private localStorage: SessionStorageService,
  private route: ActivatedRoute,
  private dataService: DataSharedService,
  // private toaster : Toas){
){
}

ngOnInit(){
  this.localStorageData = this.localStorage.getWebLocal();
  console.log(this.localStorageData)
}

  imagePreview: string | ArrayBuffer | null = null;

  onFileSelected(event: Event) {
    const file = (event.target as HTMLInputElement).files?.[0];

    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  updateprofile(){
    this.dialog.open(AddProfileComponent,{
      width:'500px',
      panelClass:'dialogModal'
    })
  }

  healthdialog(){
    this.dialog.open(HealthStatusComponent,{
      width:'500px',
      panelClass:'dialogModal'
    })
  }

}
