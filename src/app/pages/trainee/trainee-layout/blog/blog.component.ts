import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.css"],
})
export class BlogComponent {
  localStorageData: any;
  blogData: { blog_id: number; title: string; title_ar: string; content: string; content_ar: string; image: string; status: number; created_at: string; updated_at: string; deleted_at: any; }[];

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    console.log(this.localStorageData);

    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.getBlogsList(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
    }
  }
  getBlogsList(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("blogs", data).subscribe({
      next: (res: any) => {
        this.blogData = res.data
        //  [
        //   {
        //     "blog_id": 6,
        //     "title": "testing blog1",
        //     "title_ar": "مدونة الاختبار",
        //     "content": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m, 110m,",
        //     "content_ar": "سباق الحواجز هو نوع من سباقات ألعاب القوى حيث يجب على المشاركين الركض السريع والقفز فوق عقبات موزعة بالتساوي تُعرف بالحواجز. يختبر السباق كلاً من السرعة والرشاقة، حيث يتعين على الرياضيين الحفاظ على وتيرة سريعة وتوقيت قفزاتهم لتجاوز كل حاجز دون فقدان الزخم. تشمل المسافات الشائعة لسباقات الحواجز 100 متر، 110 متر، و400 متر، مع ارتفاعات محددة للحواجز تعتمد على فئة السباق. الهدف هو إنهاء السباق بأسرع وقت ممكن مع تجاوز جميع الحواجز بنجاح.",
        //     "image": "https://staging.harwalah.com/file_storage/blog/image/1731006907672d11bb0fb2a.jpeg",
        //     "status": 1,
        //     "created_at": "08-11-2024 00:45 AM",
        //     "updated_at": "08-11-2024 00:47 AM",
        //     "deleted_at": null
        //   },
        //   {
        //     "blog_id": 4,
        //     "title": "Race of Race",
        //     "title_ar": "وصف سباق الحواجز",
        //     "content": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m",
        //     "content_ar": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m",
        //     "image": "https://staging.harwalah.com/file_storage/blog/image/17285717276707e94ff0a61.jpg",
        //     "status": 1,
        //     "created_at": "10-10-2024 20:18 PM",
        //     "updated_at": "10-10-2024 20:18 PM",
        //     "deleted_at": null
        //   },
        //   {
        //     "blog_id": 2,
        //     "title": "Hurdle Race",
        //     "title_ar": "وصف سباق",
        //     "content": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m, 110m, and 400m, with specific hurdle heights depending on the race category. The goal is to finish the race as quickly as possible while clearing all hurdles successfully.",
        //     "content_ar": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m, 110m, and 400m, with specific hurdle heights depending on the race category. The goal is to finish the race as quickly as possible while clearing all hurdles successfully.",
        //     "image": "https://staging.harwalah.com/file_storage/blog/image/172754778066f8498487723.jpg",
        //     "status": 1,
        //     "created_at": "28-09-2024 23:53 PM",
        //     "updated_at": "29-10-2024 19:37 PM",
        //     "deleted_at": null
        //   },
        //   {
        //     "blog_id": 1,
        //     "title": "Hurdle Race Description",
        //     "title_ar": "وصف سباق الحواجز",
        //     "content": "A hurdle race is a type of track and field event where participants must sprint while jumping over evenly spaced obstacles known as hurdles. The race tests both speed and agility, requiring athletes to maintain a fast pace while timing their jumps to clear each hurdle without losing momentum. Common distances for hurdle races include 100m, 110m, and 400m, with specific hurdle heights depending on the race category. The goal is to finish the race as quickly as possible while clearing all hurdles successfully.",
        //     "content_ar": "سباق الحواجز هو نوع من سباقات ألعاب القوى حيث يجب على المشاركين الركض السريع والقفز فوق عقبات موزعة بالتساوي تُعرف بالحواجز. يختبر السباق كلاً من السرعة والرشاقة، حيث يتعين على الرياضيين الحفاظ على وتيرة سريعة وتوقيت قفزاتهم لتجاوز كل حاجز دون فقدان الزخم. تشمل المسافات الشائعة لسباقات الحواجز 100 متر، 110 متر، و400 متر، مع ارتفاعات محددة للحواجز تعتمد على فئة السباق. الهدف هو إنهاء السباق بأسرع وقت ممكن مع تجاوز جميع الحواجز بنجاح.",
        //     "image": "https://staging.harwalah.com/file_storage/blog/image/172754756966f848b1e7e68.jpg",
        //     "status": 1,
        //     "created_at": "28-09-2024 23:49 PM",
        //     "updated_at": "08-11-2024 08:34 AM",
        //     "deleted_at": null
        //   }
        // ]
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  // viewBlog(element: any) {
  //   this.router.navigateByUrl(`trainee-layout/blog/blog-details/${id}`)
  // }
  viewBlog(blog: any) {
    this.router.navigateByUrl(`trainee-layout/blog/blog-details`, {
      state: { blogData: blog }
    });
  }

  isExpanded: { [key: number]: boolean } = {};

  toggleExpand(blogId: number): void {
    this.isExpanded[blogId] = !this.isExpanded[blogId];
  }

}
