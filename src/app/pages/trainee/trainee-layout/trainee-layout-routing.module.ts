import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeLayoutComponent } from './trainee-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionListComponent } from './subscription-list/subscription-list.component';
import { SubscriptionPnlComponent } from './subscription-pnl/subscription-pnl.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { SuccessfullyPageComponent } from './successfully-page/successfully-page.component';
import { WorkoutsComponent } from './workouts/workouts.component';
import { WorkoutDetailsComponent } from './workouts/workout-details/workout-details.component';
import { RunningPageComponent } from './running-page/running-page.component';
import { ViewRunningComponent } from './running-page/view-running/view-running.component';
import { NutritionDietComponent } from './nutrition-diet/nutrition-diet.component';
import { FoodRecipesComponent } from './food-recipes/food-recipes.component';
import { CoachTrainerComponent } from './coach-trainer/coach-trainer.component';
import { ChallengePageComponent } from './challenge-page/challenge-page.component';
import { ChallengeDetailsComponent } from './challenge-page/challenge-details/challenge-details.component';
import { CoachDetailsComponent } from './coach-trainer/coach-details/coach-details.component';
import { GroupsComponent } from './groups/groups.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { GroupDetailsComponent } from './groups/group-details/group-details.component';
import { GroupChatComponent } from './groups/group-chat/group-chat.component';
import { TraineeGroupChatComponent } from './groups/trainee-group-chat/trainee-group-chat.component';
import { AddNutritionComponent } from './nutrition-diet/add-nutrition/add-nutrition.component';
import { BlogDetailsComponent } from './blog/blog-details/blog-details.component';
import { BlogComponent } from './blog/blog.component';
import { RecipesDetailsComponent } from './food-recipes/recipes-details/recipes-details.component';
import { CommunityComponent } from './community/community.component';
import { SeeAllComponent } from './community/see-all/see-all.component';
import { CreateGroupComponent } from './community/create-group/create-group.component';
import { JoinGroupComponent } from './community/join-group/join-group.component';
import { WritePostComponent } from './community/write-post/write-post.component';
import { MyGroupPostComponent } from './community/my-group-post/my-group-post.component';
import { CalenderComponent } from './calender/calender.component';


const routes: Routes = [
  {path:'',component:TraineeLayoutComponent,children:[
    {path:'',redirectTo:'dashboard',pathMatch:'full'},
    {path:'dashboard', component:DashboardComponent},
    {path:'subscription-pnl', component:SubscriptionPnlComponent},
    {path:'subscription-list', component:SubscriptionListComponent},
    {path:'payment-method', component:PaymentMethodComponent},
    {path:'successfully-page', component:SuccessfullyPageComponent},
    {path:'workouts', component:WorkoutsComponent},
    {path:'workout-details/:id', component:WorkoutDetailsComponent},
    {path:'running', component:RunningPageComponent},
    {path:'running-view', component:ViewRunningComponent},
    {path:'nutrition-diet', component:NutritionDietComponent},
    {path:'food-recipes', component:FoodRecipesComponent},
    {path:'food-recipes/details', component:RecipesDetailsComponent},
    {path:'coach-trainer', component:CoachTrainerComponent},
    {path:'challenges', component:ChallengePageComponent},
    {path:'challenges-details/:type/:challengeId', component:ChallengeDetailsComponent},
    {path:'coach-details/:id/:type', component:CoachDetailsComponent},
    {path:'groups', component:GroupsComponent},
    {path:'profile-info', component:ProfileInfoComponent},
    {path:'group-details/:id', component:GroupDetailsComponent},
    {path:'group-chat/:id', component:GroupChatComponent},
    {path:'trainee-group-chat/:id', component:TraineeGroupChatComponent},
    {path:'nutrition-diet/add-nutrition', component:AddNutritionComponent},
    {path:'blog', component:BlogComponent},
    {path:'blog/blog-details', component:BlogDetailsComponent},
    {path:'community', component:CommunityComponent},
    {path:'community/see-all', component:SeeAllComponent},
    {path:'community/create-group', component:CreateGroupComponent},
    {path:'community/join-group', component:JoinGroupComponent},
    {path:'community/write-post', component:WritePostComponent},
    {path:'community/my-group-post', component:MyGroupPostComponent},
    {path:'calender', component:CalenderComponent},





    
    
    


  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TraineeLayoutRoutingModule { }
