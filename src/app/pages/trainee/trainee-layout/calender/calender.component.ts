import { Component } from "@angular/core";
import { CalendarOptions } from '@fullcalendar/core';

import dayGridPlugin from '@fullcalendar/daygrid';


@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent {
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth', // Use the dayGridMonth view
    plugins: [dayGridPlugin], // Register the dayGridPlugin
    events: [
      {
        title: 'Event 1',
        date: '2025-01-30',
        extendedProps: {
          imageUrl: '/assets/images/bestCoach/2.jpg' // Add your image URL
        }
      },
      {
        title: 'Event 2',
        date: '2025-02-01',
        extendedProps: {
          imageUrl: '/assets/images/bestCoach/3.jpg'
        }
      }
    ],
    eventContent: function(arg) {
      let imageUrl = arg.event.extendedProps["imageUrl"];
      let title = arg.event.title;
      return {
        html: `<div><img src="${imageUrl}" alt="${title}" style="width: 100%; height: 80px;display:block; margin-right: 5px;">
        ${title}
        </div>`
      };
    }
  };
}
