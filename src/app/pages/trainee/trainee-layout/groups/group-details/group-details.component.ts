import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-group-details",
  templateUrl: "./group-details.component.html",
  styleUrls: ["./group-details.component.css"],
})
export class GroupDetailsComponent {
  groupData: any;
  notes: any;
  notesList: any;
  localStorageData: any;

  member_user_id: any;
  traniee_data: any;
  groupDetails: any;
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) {
    // console.log(this.router.getCurrentNavigation()?.extras?.state);
  }
  ngOnInit(): void {
    this.traniee_data = history.state;
    console.log("traniee_data", this.traniee_data);

    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.id;
        this.localStorageData = this.localstorage.getWebLocal();
      },
    });
    this.getGroupDetails();
    this.getNotes();
  }
  addNotes() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      group_id: this.member_user_id,
      note: this.notes,
    };
    this.api.user_post("group/notes/add", data).subscribe({
      next: (res: any) => {
        this.getNotes();
        this.notes = "";
      },
    });
  }
  getNotes() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      group_id: this.member_user_id,
    };
    this.api.user_post("group/notes", data).subscribe({
      next: (res: any) => {
        this.notesList = res.data;
      },
    });
  }
  getGroupDetails() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      group_id: this.member_user_id,
    };
    this.api.user_post("groups/detail", data).subscribe({
      next: (res: any) => {
        this.groupDetails = res.data[0];
      },
    });
  }

  removeNotes(element: any) {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData.user_id,
      group_id: this.member_user_id,
      note_id: element?.note_id,
    };
    this.api.user_post("group/notes/remove", data).subscribe({
      next: (res: any) => {
        this.getNotes();
      },
    });
  }
}
