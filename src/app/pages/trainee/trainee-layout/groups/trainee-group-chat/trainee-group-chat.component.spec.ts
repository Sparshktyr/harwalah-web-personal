import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraineeGroupChatComponent } from './trainee-group-chat.component';

describe('TraineeGroupChatComponent', () => {
  let component: TraineeGroupChatComponent;
  let fixture: ComponentFixture<TraineeGroupChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraineeGroupChatComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TraineeGroupChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
