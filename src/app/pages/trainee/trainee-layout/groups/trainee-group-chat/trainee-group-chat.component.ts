import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { DataSharedService } from 'src/app/service/data-shared.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { environment } from 'src/environments/environment';
import { initializeApp } from 'firebase/app';
import { getDatabase, onValue, push, ref, set } from 'firebase/database';

@Component({
  selector: 'app-trainee-group-chat',
  templateUrl: './trainee-group-chat.component.html',
  styleUrls: ['./trainee-group-chat.component.css']
})
export class TraineeGroupChatComponent {
  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService,
    private route: ActivatedRoute,
    private dataService: DataSharedService,
    // private toaster : Toas

  ) {}

  param_id:any;
  localStorageData:any;
  app:any;
  database:any;
  ngOnInit() {
    debugger
    this.route.params.subscribe((params) => {
      this.param_id = params["id"];
      this.app = initializeApp(environment.firebaseConfig);
      this.database = getDatabase(this.app);
      this.getGroup(this.database,this.param_id)
      this.getDataFromDataBase(this.database,this.param_id); 

    });

    this.localStorageData = this.localStorage.getWebLocal();
    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      // this.getCoachDetails(
      //   this.localStorageData.login_token,
      //   this.localStorageData?.user_id,
      //   this.param_id
      // );
    }

  }

  coachDetails:any;
  getCoachDetails(token: string, user_id: string, coachId: any) {
    let data = {
      token: token,
      user_id: user_id,
      coach_id: coachId,
    };
    this.api.user_post("coach/detail", data).subscribe({
      next: (res: any) => {
        
        this.coachDetails = res.data;
        console.log(this.coachDetails,"coachcoachs");
        console.log(this.localStorageData , "locallocallocal");
        this.app = initializeApp(environment.firebaseConfig);
        this.database = getDatabase(this.app);
        this.getDataFromDataBase(this.database,this.param_id);    
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  message_data:any[] = [];
  message_list_url:any;
  getDataFromDataBase(db:any,group_id:string){
    // 
    this.message_list_url = ref(db, `Messages/GroupMessages/${group_id}`);
    // if()
    onValue(this.message_list_url, (snapshot) => {
      let data:any[] = snapshot.val();
      // console.log(data);
      if(data){
        let message_data:any = []
        // need to logic here  
        for (const key in data) {
          if (Object.prototype.hasOwnProperty.call(data, key)) {
            // const element = object[key];
            message_data.push(data[key]);
          }
        }
        this.message_data = message_data;
        setTimeout(() => {
          this.scrollToBottom("scrollProperty");
         }, 50);
        console.log(this.message_data);
      }
      // console.log(data , "   DataDataDataDataData");
      
      // updateStarCount(postElement, data);
    });
  }

  group_data; 
  getGroup(db:any,group_id:string){
    // 
    let group_data_url = ref(db, `Groups/group_${group_id}/information`);
    // if()
    onValue(group_data_url, (snapshot) => {
      this.group_data = snapshot.val();
      console.log(this.group_data);
    });
  }

  writeData(db,group_id:string,data:any) {
    // const db = getDatabase();
    if(!this.message_list_url){
      this.message_list_url = ref(db, `Messages/GroupMessages/${group_id}`);
    }
    const newPostRef = push(this.message_list_url);
    this.message = "";
    set(newPostRef,data).then(res=>{
      console.log(res);
      this.message = "";
      setTimeout(() => {
        this.scrollToBottom("scrollProperty");
       }, 50);
    }).catch((err)=>{console.log(err);});
  }

  message:string = "";
  submit(){
    if(!this.message){
      return;
    }
    let makeData:any = {
      chat_id:this.param_id,
      mediaType:"text",
      message:this.message,
      readStatus:"",
      receiverId:"",
      receiverImage:"",
      receiverName:"",
      senderId:this.localStorageData?.user_id,
      senderImage:this.localStorageData?.profile_image,
      senderName:`${this.localStorageData?.first_name} ${this.localStorageData?.last_name}`,
      timestamp: new Date().getTime()
    }    
    this.writeData(this.database,this.param_id,makeData);
  }

  scrollToBottom(id){
    debugger
    const element = document.getElementById(id);
    element.scrollTop = element.scrollHeight;
  }

}
