import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent {
  localStorageData: any;
  groupList: any;

  constructor(
    // private dataService : DataSharedService, 
    private localStorage : SessionStorageService,
    private api : ApiService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocal()
    if(this.localStorageData && 
      this.localStorageData.login_token 
    ){
      this.fetchGroupList()
    }
  }

  fetchGroupList(){
    let data = {
      "token": this.localStorageData.login_token,
      "user_id": this.localStorageData.user_id
   }
   this.api.user_post('groups', data).subscribe({
    next : (res : any)=>{
      this.groupList = res.data
    }
   })
  }
  goToDetail(item:any){
    this.router.navigateByUrl(`trainee-layout/group-details/${item?.group_id}`,{state:item});
  }
}
