import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CustomerDialogComponent } from '../customer-dialog/customer-dialog.component';

@Component({
  selector: 'app-add-nutrition',
  templateUrl: './add-nutrition.component.html',
  styleUrls: ['./add-nutrition.component.css']
})
export class AddNutritionComponent {

  constructor(private dialog:MatDialog){}

  customerdiet(){
    this.dialog.open(CustomerDialogComponent,{
     width:'400px',
    })
  }

}
