import { Component } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { DataSharedService } from "src/app/service/data-shared.service";
import { SessionStorageService } from "src/app/service/session-storage.service";
import { ReviewDialogComponent } from "../review-dialog/review-dialog.component";

@Component({
  selector: "app-coach-details",
  templateUrl: "./coach-details.component.html",
  styleUrls: ["./coach-details.component.css"],
})
export class CoachDetailsComponent {
  localStorageData: any;
  coachDetails: any;
  coach: any;
  levelData: any;
  equipmentData: any;
  frontData: any;
  backData: any;
  type: any;
  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService,
    private route: ActivatedRoute,
    private dataService: DataSharedService,
    // private toaster : Toas
    private dialog:MatDialog,

  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.coach = params["id"];
      this.type = params["type"];
    });

    this.localStorageData = this.localStorage.getWebLocal();
    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.getCoachDetails(
        this.localStorageData.login_token,
        this.localStorageData?.user_id,
        this.coach
      );
    }

    this.getLevelList()
    this.getEquipmentsList()
    this.getFrontMuscelsList()
    this.getbackMuscelsList()
  }

  getCoachDetails(token: string, user_id: string, coachId: any) {
    let data = {
      token: token,
      user_id: user_id,
      coach_id: coachId,
    };
    this.api.user_post("coach/detail", data).subscribe({
      next: (res: any) => {
        this.coachDetails = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  subscribeCoach() {
    let data = {
      token: this.localStorageData.login_token,
      user_id: this.localStorageData?.user_id,
      coach_id: this.coach,
      amount: this.coachDetails?.price_per_month,
      transaction_id: "45lpoihytf5654rt",
    };
    this.api.user_post("coach/subscribe", data).subscribe({
      next: (res: any) => {
        // this.coachDetails = res.data;
        console.log(this.coachDetails)
        // this.dataService.setMessage(this.coachDetails);
        // this.router.navigate(['/trainee-layout/successfully-page']);

        this.router.navigate(['/trainee-layout/successfully-page']).then(()=>{
          this.dataService.setMessage(this.coachDetails);
        });
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  getLevelList() {
    this.api.common_api_post("levels", "").subscribe({
      next: (res: any) => {
        this.levelData = res.data;
      },
    });
  }
  getEquipmentsList() {
    this.api.common_api_post("equipments", "").subscribe({
      next: (res: any) => {
        this.equipmentData = res.data;
      },
    });
  }

  getFrontMuscelsList() {
    this.api.common_api_post("front", "").subscribe({
      next: (res: any) => {
        this.frontData = res.data;
      },
    });
  }

  getbackMuscelsList() {
    this.api.common_api_post("back", "").subscribe({
      next: (res: any) => {
        this.backData = res.data;
      },
    });
  }

  gotochat(){
    this.router.navigateByUrl(`/trainee-layout/group-chat/${this.coach}`)
  }

  showReview(){
    this.dialog.open(ReviewDialogComponent,{
      
    })
  }


}
