import { Component } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MemberListDialogComponent } from "../member-list-dialog/member-list-dialog.component";
import { LeaveGroupDialogComponent } from "../leave-group-dialog/leave-group-dialog.component";
import { SessionStorageService } from "src/app/service/session-storage.service";
import { ApiService } from "src/app/service/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-join-group",
  templateUrl: "./join-group.component.html",
  styleUrls: ["./join-group.component.css"],
})
export class JoinGroupComponent {
  stateData: any;
  localStorageData: any;
  youJoinFeedData: any;
  selectionData: any;

  constructor(
    public dialog: MatDialog,
    private api: ApiService,
    private localStorage: SessionStorageService,
    private router :Router
  ) {}

  MemberListDialog(
    enterAnimationDuration: string,
    exitAnimationDuration: string,
    data: any
  ) {
    this.dialog.open(MemberListDialogComponent, {
      width: "600px",
      height: "auto",
      maxHeight: "100vh",
      maxWidth: "90vw",
      enterAnimationDuration,
      exitAnimationDuration,
      panelClass: "layout-dialog",
      data: data,
    });
  }

  leaveDialog(
    enterAnimationDuration: string,
    exitAnimationDuration: string,
    data: any
  ) {
    this.dialog.open(LeaveGroupDialogComponent, {
      width: "400px",
      maxWidth: "90vw",
      panelClass: "dialog-layout",
      disableClose: true,
      enterAnimationDuration,
      exitAnimationDuration,
      data: data,
    });
  }

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();

    this.stateData = history.state["data"];
    this.selectionData = history.state["selection"];
    console.log(this.stateData);
    if(this.stateData){
        this.getFeedPost(
          this.localStorageData.login_token,
          this.localStorageData?.user_id
        );
        // this.getMyCoachList(
        //   this.localStorageData.login_token,
        //   this.localStorageData?.user_id
        // )
      }
  }

  getFeedPost(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("feeds/posts", {...data, feed_id : this.stateData.feed_id}).subscribe({
      next: (res: any) => {
        this.youJoinFeedData = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  goToUrl(data : any){
    this.router.navigateByUrl(`trainee-layout/community/write-post`, {state : {data : data}})
  }
}
