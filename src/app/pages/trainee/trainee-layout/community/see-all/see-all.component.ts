import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-see-all',
  templateUrl: './see-all.component.html',
  styleUrls: ['./see-all.component.css']
})
export class SeeAllComponent {

  localStorageData: any;
  youJoinFeedData: any;
  myCoachList: any;
  myCreatedFeeds: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    console.log(this.localStorageData);

    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.yourJoinedFeed(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
    }
  }

  yourJoinedFeed(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("feeds/joined", data).subscribe({
      next: (res: any) => {
        this.youJoinFeedData = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  goToPage(url : any,data : any){
    this.router.navigateByUrl(url, {state : {data :data }})
  }
}
