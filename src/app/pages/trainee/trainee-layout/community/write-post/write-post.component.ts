import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";
import { Location } from '@angular/common'; 

@Component({
  selector: "app-write-post",
  templateUrl: "./write-post.component.html",
  styleUrls: ["./write-post.component.css"],
})
export class WritePostComponent {
  imageUrl: string = "";
  localStorageData: any;
  stateData: any;
  groupForm!: FormGroup;
  constructor(
    private api: ApiService,
    private localStorage: SessionStorageService,
    private fb: FormBuilder,
    private location : Location,
  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    this.groupForm = this.fb.group({
      file: [null],
      description: ["", [Validators.required, Validators.minLength(10)]],
    });

    this.stateData = history.state["data"];
    console.log(this.stateData);
  }

  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageUrl = e.target.result;
      };
      reader.readAsDataURL(file);
  
      this.groupForm.get("file")?.setValue(file);
    }
  }
  
  removeImage(): void {
    this.imageUrl = "";
    this.groupForm.get("file")?.setValue(null);
  }

  createGroup(): void {
    if (this.groupForm.valid) {
      const formData = new FormData();
  
      formData.append("token", this.localStorageData?.login_token || "");
      formData.append("user_id", this.localStorageData?.user_id || "");
      formData.append("feed_id", this.stateData);
      formData.append(
        "description",
        this.groupForm.get("description")?.value || ""
      );
  
      const fileControl = this.groupForm.get("file")?.value;
      if (fileControl instanceof File) {
        formData.append("file", fileControl);
      } else {
        console.error("No valid file found for upload.");
      }
  
      formData.forEach((value, key) => {
        console.log(`${key}:`, value);
      });
  
      this.api.user_post("feeds/posts/create", formData).subscribe(
        (response) => {
          // this.router.navigateByUrl(`/trainee-layout/community/join-group`, {state : {data : this.stateData}})
          console.log("Group created successfully", response);
          this.location.back();
        },
        (error) => {
          console.error("Error creating group", error);
        }
      );
    } else {
      this.groupForm.markAllAsTouched();
    }
  }
  

  // onFileSelected(event: any): void {
  //   const file = event.target.files[0];
  //   if (file) {
  //     const reader = new FileReader();
  //     reader.onload = (e: any) => {
  //       this.imageUrl = e.target.result;
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // }

  // removeImage(): void {
  //   this.imageUrl = "";
  // }
}
