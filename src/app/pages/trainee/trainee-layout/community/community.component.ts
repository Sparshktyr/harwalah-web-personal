import { state } from '@angular/animations';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent {
  localStorageData: any;
  youJoinFeedData: any;
  myCoachList: any;
  myCreatedFeeds: any;
  feedMayJoinList: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private localStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    console.log(this.localStorageData);

    if ((this.localStorageData.login_token, this.localStorageData?.user_id)) {
      this.yourJoinedFeed(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      );
      this.getMyFeeds(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      )
      this.feedMayJoin(
        this.localStorageData.login_token,
        this.localStorageData?.user_id
      )
    }
  }

  yourJoinedFeed(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("feeds/joined", data).subscribe({
      next: (res: any) => {
        this.youJoinFeedData = res.data.slice(0, 4);;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  
  getMyFeeds(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("feeds", data).subscribe({
      next: (res: any) => {
        this.myCreatedFeeds = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }
  
  feedMayJoin(token: string, user_id: string) {
    let data = {
      token: token,
      user_id: user_id,
    };
    this.api.user_post("feeds/to/join", data).subscribe({
      next: (res: any) => {
        this.feedMayJoinList = res.data;
      },
      error: (err: any) => {
        console.log(err);
      },
    });
  }

  goToPage(url : any,data : any){
    this.router.navigateByUrl(url, {state : {data :data }})
  }
}
