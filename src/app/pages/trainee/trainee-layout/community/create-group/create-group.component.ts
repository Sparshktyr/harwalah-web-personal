import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";
import { Location } from '@angular/common'; 

@Component({
  selector: "app-create-group",
  templateUrl: "./create-group.component.html",
  styleUrls: ["./create-group.component.css"],
})
export class CreateGroupComponent implements OnInit {
  imageUrl: string = "";
  groupForm!: FormGroup;
  localStorageData: any;
  stateData: any;

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private localStorage: SessionStorageService,
    private location : Location,

  ) {}

  ngOnInit() {
    this.localStorageData = this.localStorage.getWebLocal();
    this.groupForm = this.fb.group({
      cover: [null, Validators.required],
      name: ["", [Validators.required, Validators.minLength(3)]],
      description: ["", [Validators.required, Validators.minLength(10)]],
    });

    this.stateData = window.history.state['data']
    console.log("this.stateData--->", this.stateData)
    if(this.stateData){
      this.groupForm.patchValue({
        name: this.stateData.name,
        description: this.stateData.description,
      })
      this.imageUrl = this.stateData.cover
    }
  }

  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageUrl = e.target.result;
      };
      reader.readAsDataURL(file);
      this.groupForm.get("cover")?.setValue(file);
    }
  }

  removeImage(): void {
    this.imageUrl = "";
    this.groupForm.get("cover")?.setValue(null);
  }

  createGroup(): void {
    if (this.groupForm.valid) {
      const formData = new FormData();
      formData.append("token", this.localStorageData?.login_token || "");
      formData.append("user_id", this.localStorageData?.user_id || "");
      formData.append("name", this.groupForm.get("name")?.value || "");
      formData.append(
        "description",
        this.groupForm.get("description")?.value || ""
      );
      const cover = this.groupForm.get("cover")?.value;
      if (cover) {
        formData.append("cover", cover);
      }

      this.api.user_post("feeds/create", formData).subscribe(
        (response) => {
          console.log("Group created successfully:", response);
          this.location.back();
        },
        (error) => {
          console.error("Error creating group:", error);
        }
      );
    } else {
      this.groupForm.markAllAsTouched();
    }
  }
}
