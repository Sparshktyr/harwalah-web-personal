import { Component, OnInit } from '@angular/core';
import { DataSharedService } from 'src/app/service/data-shared.service';

@Component({
  selector: 'app-successfully-page',
  templateUrl: './successfully-page.component.html',
  styleUrls: ['./successfully-page.component.css']
})
export class SuccessfullyPageComponent implements OnInit{

  constructor(
    private dataService : DataSharedService, 
  ) { }


  subSrriptionData:any;
  ngOnInit(): void {
    this.dataService.getMessage.subscribe((data : any)  => {
      console.log(this.subSrriptionData)
      this.subSrriptionData = data;
      if(!this.subSrriptionData){
        window.history.back();
      }else{
        
      }
    });
  }

}
