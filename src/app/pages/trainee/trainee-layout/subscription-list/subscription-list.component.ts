import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { DataSharedService } from 'src/app/service/data-shared.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-subscription-list',
  templateUrl: './subscription-list.component.html',
  styleUrls: ['./subscription-list.component.css']
})
export class SubscriptionListComponent {
  subscriptionList: any ;
  localStorageData: any;

  constructor(
    private api : ApiService,
    private router : Router,
    private localStorage : SessionStorageService,
    private dataService: DataSharedService
  ){}

  ngOnInit(){
    this.localStorageData = this.localStorage.getWebLocal()
    this.subscriptionData()
  }
  subscriptionData(){
    let data = {
      "token":this.localStorageData.login_token,
      "user_id": this.localStorageData.user_id
    }
    this.api.user_post('packages', data).subscribe({
      next : (res : any)=>{
        this.subscriptionList = res.data
        this.subscriptionList = this.subscriptionList.sort((a:any,b:any)=>{
          return a.package_type - b.package_type;
        })

      }, 
      error: (err : any)=> {
        console.log(err)
      },
    })
  }

  package_type:any = {
    '1':"1 month",
    '2':"6 month",
    '3':"12 month"
  }

  goToPayment(subscriptionData: any) {
    this.dataService.setMessage(subscriptionData);
    this.router.navigate(['/trainee-layout/payment-method']);
  }
}
