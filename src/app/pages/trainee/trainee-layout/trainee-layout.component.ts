import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainee-layout',
  templateUrl: './trainee-layout.component.html',
  styleUrls: ['./trainee-layout.component.css']
})
export class TraineeLayoutComponent {
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  selected = 'option2'; 
  
constructor( public dialog : MatDialog, private breakpointObserver: BreakpointObserver, private router : Router) {}
 

}
