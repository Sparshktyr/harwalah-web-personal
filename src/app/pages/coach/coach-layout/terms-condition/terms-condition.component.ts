import { Component } from '@angular/core';

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css']
})
export class TermsConditionComponent {

  innerHtml = `
  <p><strong>Last Updated: 11/Jan/2025</strong></p>
  
  <p>Welcome to [Your Fitness App Name]! These Terms and Conditions ("Terms") govern your use of our mobile application and services (collectively, the "Service"). By accessing or using the Service, you agree to be bound by these Terms. If you do not agree to these Terms, please do not use the Service.</p>
  
  <h2>1. Acceptance of Terms</h2>
  <p>By creating an account or using the Service, you confirm that you are at least 18 years old or have the consent of a parent or guardian. If you are using the Service on behalf of an organization, you represent that you have the authority to bind that organization to these Terms.</p>
  
  <h2>2. Changes to Terms</h2>
  <p>We may modify these Terms at any time. We will notify you of any changes by posting the new Terms on our app and updating the "Last Updated" date. Your continued use of the Service after any changes constitutes your acceptance of the new Terms.</p>
  
  <h2>3. User Accounts</h2>
  <p>To access certain features of the Service, you may need to create an account. You agree to provide accurate, current, and complete information during the registration process and to update such information to keep it accurate, current, and complete. You are responsible for maintaining the confidentiality of your account and password and for all activities that occur under your account.</p>
  
  <h2>4. Use of the Service</h2>
  <p>You agree to use the Service only for lawful purposes and in accordance with these Terms. You agree not to:</p>
  <ul>
      <li>Use the Service in any way that violates any applicable federal, state, local, or international law or regulation.</li>
      <li>Engage in any conduct that restricts or inhibits anyone's use or enjoyment of the Service.</li>
      <li>Impersonate or attempt to impersonate [Your Fitness App Name], a [Your Fitness App Name] employee, another user, or any other person or entity.</li>
  </ul>
  
  <h2>5. Health Disclaimer</h2>
  <p>The Service is designed for informational and educational purposes only and is not a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition. You should never disregard professional medical advice or delay in seeking it because of something you have read on the Service.</p>
  
  <h2>6. Subscription and Fees</h2>
  <p>Some features of the Service may require payment of fees. You agree to pay all applicable fees and taxes associated with your use of the Service. We reserve the right to change our fees at any time, but we will notify you in advance of any changes.</p>
  
  <h2>7. Cancellation and Refund Policy</h2>
  <p>You may cancel your subscription at any time through your account settings. Refunds will be handled according to our refund policy, which will be provided at the time of purchase.</p>
  
  <h2>8. Intellectual Property</h2>
  <p>All content, features, and functionality on the Service, including but not limited to text, graphics, logos, and software, are the exclusive property of [Your Fitness App Name] or its licensors and are protected by copyright, trademark, and other intellectual property laws.</p>
  
  <h2>9. Limitation of Liability</h2>
  <p>To the fullest extent permitted by law, [Your Fitness App Name] shall not be liable for any indirect, incidental, special, consequential, or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your use of or inability to use the Service; (ii) any unauthorized access to or use of our servers and/or any personal information stored therein; (iii) any interruption or cessation of transmission to or from the Service; or (iv) any bugs, viruses, or the like that may be`
}
