import { Component } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent {
innerHtml = `<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@400&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Almarai', sans-serif;
            line-height: 1.6;
            margin: 20px;
            padding: 20px;
            background-color: #f9f9f9;
        }
        h1, h2, h3 {
            color: #333;
        }
        p {
            color: #555;
        }
    </style>
</head>
<body>

<p><strong>Last Updated:11/Jan/2025 </strong></p>

<p>At Harwallah, we are committed to protecting your privacy. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you use our mobile application and services (collectively, the "Service"). Please read this Privacy Policy carefully. If you do not agree with the terms of this Privacy Policy, please do not access the Service.</p>

<h2>1. Information We Collect</h2>
<p>We may collect information about you in a variety of ways, including:</p>

<h3>1.1 Personal Information</h3>
<p>When you register for an account, we may ask you to provide certain personal information, including but not limited to:</p>
<ul>
    <li>Name</li>
    <li>Email address</li>
    <li>Phone number</li>
    <li>Date of birth</li>
    <li>Payment information (if applicable)</li>
</ul>

<h3>1.2 Usage Data</h3>
<p>We may collect information about how you access and use the Service, including:</p>
<ul>
    <li>Device information (e.g., IP address, browser type, operating system)</li>
    <li>Log data (e.g., access times, pages viewed, and the time spent on those pages)</li>
    <li>Location data (if you enable location services)</li>
</ul>

<h3>1.3 Cookies and Tracking Technologies</h3>
<p>We may use cookies, web beacons, and similar tracking technologies to collect information about your interactions with the Service. You can control the use of cookies at the individual browser level.</p>

<h2>2. How We Use Your Information</h2>
<p>We may use the information we collect for various purposes, including:</p>
<ul>
    <li>To provide, maintain, and improve our Service</li>
    <li>To process your transactions and manage your account</li>
    <li>To communicate with you, including sending you updates, newsletters, and promotional materials</li>
    <li>To analyze usage and trends to improve user experience</li>
    <li>To detect, prevent, and address technical issues</li>
    <li>To comply with legal obligations</li>
</ul>

<h2>3. Disclosure of Your Information</h2>
<p>We may share your information in the following situations:</p>
<ul>
    <li><strong>With Service Providers:</strong> We may share your information with third-party vendors, service providers, contractors, or agents who perform services for us or on our behalf.</li>
    <li><strong>For Business Transfers:</strong> We may share or transfer your information in connection with a merger, sale of assets, financing, or acquisition of all or a portion of our business to another company.</li>
    <li><strong>With Your Consent:</strong> We may disclose your personal information for any other purpose with your consent.</li>
</ul>

<h2>4. Data Security</h2>
<p>We take reasonable measures to help protect personal information from loss, theft, misuse, and unauthorized access, disclosure, alteration, and destruction. However, no method of transmission over the Internet or method of electronic storage is 100% secure, and we cannot guarantee its absolute security.</p>

<h2>5. Your Rights</h2>
<p>You have the right to:</p>
<ul>
    <li>Access, correct, or delete your personal information.</li>
    <li>Object to or restrict the processing of your personal information.</li>
    <li>Withdraw your consent at any time where we rely on your consent to process your personal information.</li>
</ul>

<h2>6. Changes to This Privacy Policy</h2>
<p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

<h2>7. Contact Us</h2>
<p>If you have any questions about this Privacy Policy, please contact us:</p>
<ul>
    <li>Email: [Your Email Address]</li>
    <li>Phone: [Your Phone Number]</li>
    <li>Address: [Your Company Address]</li>
</ul>

</body>
</html>`
}
