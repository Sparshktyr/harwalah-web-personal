import { Component } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent {
  faqs = [
    {
      question: 'What is Lorem Ipsum?',
      answer: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
      isOpen: false
    },
    {
      question: 'Where does it come from?',
      answer: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      isOpen: false
    },
    {
      question: 'Where can I get some?',
      answer: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.',
      isOpen: false
    }
  ];

  toggleAnswer(faq: any) {
    faq.isOpen = !faq.isOpen;
  }
}
