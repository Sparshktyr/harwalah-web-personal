import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { CoachLayoutRoutingModule } from './coach-layout-routing.module';
import { CoachLayoutComponent } from './coach-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { SubscriptionPnlComponent } from './subscription-pnl/subscription-pnl.component';
import { SubscriptionListComponent } from './subscription-list/subscription-list.component';
import { PaymentPageComponent } from './payment-page/payment-page.component';
import { SuccessfullyComponent } from './successfully/successfully.component';
import { MyMembersComponent } from './my-members/my-members.component';
import { MemberGroupsComponent } from './member-groups/member-groups.component';
import { AddMembersComponent } from './my-members/add-members/add-members.component';
import { VerifyEmailComponent } from './my-members/verify-email/verify-email.component';
import { PageSuccessfullyComponent } from './my-members/page-successfully/page-successfully.component';
import { CreateGroupComponent } from './member-groups/create-group/create-group.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RejectedDialogComponent } from './rejected-dialog/rejected-dialog.component';
import { MemberdataDialogComponent } from './member-groups/memberdata-dialog/memberdata-dialog.component';
import { GroupSuccessfullyComponent } from './member-groups/group-successfully/group-successfully.component';
import { MemberDetailsComponent } from './my-members/member-details/member-details.component';
import { AddWorkoutComponent } from './my-members/add-workout/add-workout.component';
import { SelectWorkoutComponent } from './my-members/select-workout/select-workout.component';
import { SecondsToMinutesPipe } from 'src/app/pipes/seconds-to-minutes.pipe';
import { PipesModule } from 'src/app/pipes/pipes/pipes.module';
import { WorkoutCoachComponent } from './my-members/workout-coach/workout-coach.component';
import { AddRunningComponent } from './my-members/add-running/add-running.component';
import { RunningDetailsComponent } from './my-members/running-details/running-details.component';
import { ChallengeCoachComponent } from './challenge-coach/challenge-coach.component';
import { DetailsChallengeComponent } from './challenge-coach/details-challenge/details-challenge.component';
import { CreateChallengeComponent } from './challenge-coach/create-challenge/create-challenge.component';
import { CoachWorkoutsComponent } from './coach-workouts/coach-workouts.component';
import { CoachRunningsComponent } from './coach-runnings/coach-runnings.component';
import { CoachNutritionsComponent } from './coach-nutritions/coach-nutritions.component';
import { ChatPanelComponent } from './my-members/chat-panel/chat-panel.component';
import { MembersGroupDetailsComponent } from './member-groups/members-group-details/members-group-details.component';
import { MemberGroupChatComponent } from './member-groups/member-group-chat/member-group-chat.component';
import { ChallengesDetailsComponent } from './my-members/challenges-details/challenges-details.component';
import { MemberDialogComponent } from './member-groups/member-dialog/member-dialog.component';
import { NutritionDetailsComponent } from './coach-nutritions/nutrition-details/nutrition-details.component';
import { RecipesMgtComponent } from './recipes-mgt/recipes-mgt.component';
import { RecipesDetailsComponent } from './recipes-mgt/recipes-details/recipes-details.component';
import { RecipesAddComponent } from './recipes-mgt/recipes-add/recipes-add.component';
import { BmiCalculatorComponent } from './bmi-calculator/bmi-calculator.component';
import { CaloriesCalculatorComponent } from './calories-calculator/calories-calculator.component';
import { ResultComponent } from './calories-calculator/result/result.component';
import { Vo2CalculatorComponent } from './vo2-calculator/vo2-calculator.component';
import { FaqComponent } from './faq/faq.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { Vo2ResultComponent } from './vo2-calculator/vo2-result/vo2-result.component';
import { CalculateComponent } from './vo2-calculator/calculate/calculate.component';



@NgModule({
  declarations: [
    CoachLayoutComponent,
    DashboardComponent,
    SubscriptionPnlComponent,
    SubscriptionListComponent,
    PaymentPageComponent,
    SuccessfullyComponent,
    MyMembersComponent,
    MemberGroupsComponent,
    AddMembersComponent,
    VerifyEmailComponent,
    PageSuccessfullyComponent,
    CreateGroupComponent,
    RejectedDialogComponent,
    MemberdataDialogComponent,
    GroupSuccessfullyComponent,
    MemberDetailsComponent,
    AddWorkoutComponent,
    SelectWorkoutComponent,
    WorkoutCoachComponent,
    AddRunningComponent,
    RunningDetailsComponent,
    ChallengeCoachComponent,
    DetailsChallengeComponent,
    CreateChallengeComponent,
    CoachWorkoutsComponent,
    CoachRunningsComponent,
    CoachNutritionsComponent,
    ChatPanelComponent,
    MembersGroupDetailsComponent,
    MemberGroupChatComponent,
    ChallengesDetailsComponent,
    MemberDialogComponent,
    NutritionDetailsComponent,
    RecipesMgtComponent,
    RecipesDetailsComponent,
    RecipesAddComponent,
    BmiCalculatorComponent,
    CaloriesCalculatorComponent,
    ResultComponent,
    Vo2CalculatorComponent,
    FaqComponent,
    TermsConditionComponent,
    PrivacyComponent,
    Vo2ResultComponent,
    CalculateComponent,

  ],
  imports: [
    CommonModule,
    CoachLayoutRoutingModule,
    SharedModule,
    MaterialModule,
    DatePipe,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ]
})
export class CoachLayoutModule { }
