import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-coach-runnings",
  templateUrl: "./coach-runnings.component.html",
  styleUrls: ["./coach-runnings.component.css"],
})
export class CoachRunningsComponent {
  localStorageData: any;
  runningDataSave: any;
  constructor(
    private localStorage: SessionStorageService,
    private api: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach();
    if (
      this.localStorageData &&
      this.localStorageData.login_token &&
      this.localStorageData.coach_id
    ) {
      this.runningData(
        this.localStorageData.login_token,
        this.localStorageData.coach_id
      );
    }
  }

  runningData(token: any, coachId: any) {
    this.api
      .coach_post("running", { token: token, coach_id: coachId })
      .subscribe({
        next: (res: any) => {
          this.runningDataSave = res.data;
        },
      });
  }

  goToRunningDetail(element: any = undefined) {
    if (!element) {
      return;
    }
    this.router.navigateByUrl(
      "/coach-layout/my-members/running-details/" + element?.user_id +"/" +element.running_id
    );
  }

  onSearch(event : any){
    this.api
    .coach_post("running", { token: this.localStorageData.login_token, coach_id: this.localStorageData.coach_id, name : event.target.value })
    .subscribe({
      next: (res: any) => {
        this.runningDataSave = res.data;
      },
    });
  }
}
