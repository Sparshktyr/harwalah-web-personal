import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { DataSharedService } from "src/app/service/data-shared.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent {
  localStorageData: any;
  dashboardDatasave: any;
  constructor(
    private dataService: DataSharedService,
    private localStorage: SessionStorageService,
    private api: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach();
    if (
      this.localStorageData &&
      this.localStorageData.login_token &&
      this.localStorageData.coach_id
    ) {
      this.dashboardData(
        this.localStorageData.login_token,
        this.localStorageData.coach_id
      );
    }
  }

  dashboardData(token: any, coachId: any) {
    this.api.coach_post("home", {token : token, coach_id : coachId}).subscribe({
      next: (res: any) => {
        this.dashboardDatasave = res.data;
      },
    });
  }
  requestAmount(){
    this.api.coach_post("amount/request", {token : this.localStorageData.login_token, coach_id : this.localStorageData.coach_id, request_amount : this.dashboardDatasave?.pending_amount }).subscribe({
      next: (res: any) => {
        // this.dashboardDatasave = res.data;
      },
    });
  }
}
