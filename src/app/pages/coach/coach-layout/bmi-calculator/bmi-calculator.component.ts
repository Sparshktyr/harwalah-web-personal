import { Component } from '@angular/core';

@Component({
  selector: 'app-bmi-calculator',
  templateUrl: './bmi-calculator.component.html',
  styleUrls: ['./bmi-calculator.component.css']
})
export class BmiCalculatorComponent {
  // weight: number = 0;
  // height: number = 0;
  // age: number = 0;
  // gender: string = '';
  // bmi: number = 0;
  // bmiClassification: string = '';
  // healthyWeightRange: { min: number; max: number } = { min: 0, max: 0 };
  // bmiPrime: number = 0;
  // ponderalIndex: number = 0;
  // weightToLose: number = 0;

  // // Function to calculate BMI
  // calculateBMI(weight: number, height: number): number {
  //   const heightInMeters = height / 100; // Convert height from cm to meters
  //   return weight / (heightInMeters * heightInMeters);
  // }

  // // Function to classify BMI
  // classifyBMI(bmi: number, age: number): string {
  //   if (age < 18) {
  //     return 'BMI classifications may not apply accurately to children and teenagers.';
  //   }

  //   if (bmi < 18.5) {
  //     return 'Underweight';
  //   } else if (bmi >= 18.5 && bmi < 24.9) {
  //     return 'Normal';
  //   } else if (bmi >= 25 && bmi < 29.9) {
  //     return 'Overweight';
  //   } else {
  //     return 'Obese';
  //   }
  // }

  // // Function to calculate healthy weight range
  // calculateHealthyWeightRange(height: number): { min: number; max: number } {
  //   const heightInMeters = height / 100;
  //   const minWeight = 18.5 * (heightInMeters * heightInMeters);
  //   const maxWeight = 25 * (heightInMeters * heightInMeters);
  //   return { min: minWeight, max: maxWeight };
  // }

  // // Function to calculate BMI Prime
  // calculateBMIPrime(bmi: number): number {
  //   return bmi / 25.0; // BMI Prime is BMI divided by 25
  // }

  // // Function to calculate Ponderal Index
  // calculatePonderalIndex(weight: number, height: number): number {
  //   const heightInMeters = height / 100;
  //   return weight / (heightInMeters * heightInMeters * heightInMeters);
  // }

  // // Function to calculate all metrics
  // calculateHealthMetrics(): void {
  //   if (this.weight > 0 && this.height > 0) {
  //     // Calculate BMI
  //     this.bmi = this.calculateBMI(this.weight, this.height);

  //     // Classify BMI
  //     this.bmiClassification = this.classifyBMI(this.bmi, this.age);

  //     // Calculate Healthy Weight Range
  //     this.healthyWeightRange = this.calculateHealthyWeightRange(this.height);

  //     // Calculate BMI Prime
  //     this.bmiPrime = this.calculateBMIPrime(this.bmi);

  //     // Calculate Ponderal Index
  //     this.ponderalIndex = this.calculatePonderalIndex(this.weight, this.height);

  //     // Calculate Weight to Lose to Reach BMI of 25
  //     const targetBMI = 25;
  //     const heightInMeters = this.height / 100;
  //     const targetWeight = targetBMI * (heightInMeters * heightInMeters);
  //     this.weightToLose = this.weight - targetWeight;
  //   }
  // }

  dateOfBirth: Date | null = null;
  gender: string = '';
  height: number | null = null; // Height in cm
  weight: number | null = null; // Weight in kg

  // Calculated values
  bmi: number | null = null;
  bmiClassification: string = '';
  healthyWeightRange = { min: 0, max: 0 };
  weightToLose: number | null = null;
  fitnessLevel: string;
  age: number | null = null;
  bmiPrime: number | null = null;
  ponderalIndex: number | null = null;
  weightToGain: number;
  
  // Method to set the fitness level
  selectFitnessLevel(level: string): void {
    this.fitnessLevel = level;
  }

  calculateAge(): void {
    if (this.dateOfBirth) {
      const today = new Date();
      const birthDate = new Date(this.dateOfBirth);
      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDifference = today.getMonth() - birthDate.getMonth();
      if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      this.age = age;
    } else {
      alert('Please select a valid date of birth.');
    }
  }

  // BMI calculation logic
  // BMI calculation logic with age, gender, and fitness level considerations
calculateBMI(): void {
  if (this.height && this.weight) {
    const heightInMeters = this.height / 100;
    this.bmi = this.weight / (heightInMeters * heightInMeters);

    // Classify BMI based on age and gender
    if (this.age < 18) {
      this.bmiClassification = 'BMI classifications may not apply accurately to children and teenagers.';
    } else {
      if (this.bmi < 18.5) {
        this.bmiClassification = 'Underweight';
      } else if (this.bmi >= 18.5 && this.bmi < 24.9) {
        this.bmiClassification = 'Normal weight';
      } else if (this.bmi >= 25 && this.bmi < 29.9) {
        this.bmiClassification = 'Overweight';
      } else {
        this.bmiClassification = 'Obesity';
      }
    }

    // Calculate healthy weight range
    this.healthyWeightRange.min = 18.5 * heightInMeters * heightInMeters;
    this.healthyWeightRange.max = 24.9 * heightInMeters * heightInMeters;

    // Calculate weight to lose or gain
    if (this.bmi > 25) {
      this.weightToLose = this.weight - this.healthyWeightRange.max;
    } else if (this.bmi < 18.5) {
      this.weightToGain = this.healthyWeightRange.min - this.weight;
    } else {
      this.weightToLose = 0;
      this.weightToGain = 0;
    }

    // Adjust classification based on fitness level
    if (this.fitnessLevel) {
      switch (this.fitnessLevel.toLowerCase()) {
        case 'beginner':
          this.bmiClassification += ' (Beginner level: start with light exercises)';
          break;
        case 'inexperienced':
          this.bmiClassification += ' (Inexperienced: gradual fitness improvement recommended)';
          break;
        case 'average level':
          this.bmiClassification += ' (Average fitness: keep maintaining your fitness)';
          break;
        case 'advanced':
          this.bmiClassification += ' (Advanced level: push for higher goals)';
          break;
        case 'expert':
          this.bmiClassification += ' (Expert level: you are in great shape!)';
          break;
      }
    }

    // Log or display the classification and recommendations
    console.log(`BMI: ${this.bmi.toFixed(2)}`);
    console.log(`Classification: ${this.bmiClassification}`);
    console.log(`Healthy weight range: ${this.healthyWeightRange.min.toFixed(2)} - ${this.healthyWeightRange.max.toFixed(2)} kg`);
    console.log(`Weight to lose: ${this.weightToLose.toFixed(2)} kg`);
    console.log(`Weight to gain: ${this.weightToGain?.toFixed(2) || 0} kg`);
  } else {
    alert('Please enter valid height and weight values.');
  }
}

}
