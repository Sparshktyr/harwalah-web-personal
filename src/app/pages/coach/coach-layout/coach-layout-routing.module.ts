import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoachLayoutComponent } from './coach-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionPnlComponent } from './subscription-pnl/subscription-pnl.component';
import { SubscriptionListComponent } from './subscription-list/subscription-list.component';
import { PaymentPageComponent } from './payment-page/payment-page.component';
import { SuccessfullyComponent } from './successfully/successfully.component';
import { MyMembersComponent } from './my-members/my-members.component';
import { MemberGroupsComponent } from './member-groups/member-groups.component';
import { AddMembersComponent } from './my-members/add-members/add-members.component';
import { PageSuccessfullyComponent } from './my-members/page-successfully/page-successfully.component';
import { CreateGroupComponent } from './member-groups/create-group/create-group.component';
import { MemberDetailsComponent } from './my-members/member-details/member-details.component';
import { AddWorkoutComponent } from './my-members/add-workout/add-workout.component';
import { SelectWorkoutComponent } from './my-members/select-workout/select-workout.component';
import { WorkoutCoachComponent } from './my-members/workout-coach/workout-coach.component';
import { AddRunningComponent } from './my-members/add-running/add-running.component';
import { RunningDetailsComponent } from './my-members/running-details/running-details.component';
import { ChallengeCoachComponent } from './challenge-coach/challenge-coach.component';
import { DetailsChallengeComponent } from './challenge-coach/details-challenge/details-challenge.component';
import { CreateChallengeComponent } from './challenge-coach/create-challenge/create-challenge.component';
import { CoachWorkoutsComponent } from './coach-workouts/coach-workouts.component';
import { CoachRunningsComponent } from './coach-runnings/coach-runnings.component';
import { CoachNutritionsComponent } from './coach-nutritions/coach-nutritions.component';
import { ChatPanelComponent } from './my-members/chat-panel/chat-panel.component';
import { MemberdataDialogComponent } from './member-groups/memberdata-dialog/memberdata-dialog.component';
import { MembersGroupDetailsComponent } from './member-groups/members-group-details/members-group-details.component';
import { MemberGroupChatComponent } from './member-groups/member-group-chat/member-group-chat.component';
import { NutritionDetailsComponent } from './coach-nutritions/nutrition-details/nutrition-details.component';
import { RecipesMgtComponent } from './recipes-mgt/recipes-mgt.component';
import { RecipesDetailsComponent } from './recipes-mgt/recipes-details/recipes-details.component';
import { RecipesAddComponent } from './recipes-mgt/recipes-add/recipes-add.component';
import { BmiCalculatorComponent } from './bmi-calculator/bmi-calculator.component';
import { CaloriesCalculatorComponent } from './calories-calculator/calories-calculator.component';
import { ResultComponent } from './calories-calculator/result/result.component';
import { Vo2CalculatorComponent } from './vo2-calculator/vo2-calculator.component';
import { FaqComponent } from './faq/faq.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { Vo2ResultComponent } from './vo2-calculator/vo2-result/vo2-result.component';
import { CalculateComponent } from './vo2-calculator/calculate/calculate.component';


const routes: Routes = [
  {path:'',component:CoachLayoutComponent,children:[
    {path:'',redirectTo:'dashboard',pathMatch:'full'},
    {path:'dashboard', component:DashboardComponent},
    {path:'subscription-pnl', component:SubscriptionPnlComponent},
    {path:'subscription-list', component:SubscriptionListComponent},
    {path:'payment', component:PaymentPageComponent},
    {path:'successfully', component:SuccessfullyComponent},
    {path:'my-members', component:MyMembersComponent},
    {path:'member-groups', component:MemberGroupsComponent},
    {path:'add-members', component:AddMembersComponent},
    {path:'member-successfully', component:PageSuccessfullyComponent},
    {path:'create-group', component:CreateGroupComponent},
    {path:'member-details', component:MemberDetailsComponent},
    {path:'member-details/:user_id', component:MemberDetailsComponent},
    {path:'add-workout', component:AddWorkoutComponent},
    {path:'add-workout/:user_id', component:AddWorkoutComponent},
    {path:'select-workout', component:SelectWorkoutComponent},
    {path:'select-workout/:user_id', component:SelectWorkoutComponent},
    {path:'workout-coach', component:WorkoutCoachComponent},
    {path:'workout-coach/:user_id/:workout_id', component:WorkoutCoachComponent},
    {path:'my-members/add-running', component: AddRunningComponent},
    {path:'my-members/add-running/:user_id', component: AddRunningComponent},
    {path:'my-members/running-details', component:RunningDetailsComponent},
    {path:'my-members/running-details/:user_id/:workout_id', component:RunningDetailsComponent},
    {path:'challenge', component:ChallengeCoachComponent},
    {path:'challenge-details', component:DetailsChallengeComponent},
    {path:'create-challenge', component:CreateChallengeComponent},
    {path:'workouts', component:CoachWorkoutsComponent},
    {path:'runnings', component:CoachRunningsComponent},
    {path:'nutritions', component:CoachNutritionsComponent},
    {path:'chat-page', component:ChatPanelComponent},
    {path:'chat-page/:user_id', component:ChatPanelComponent},
    {path:'group-details/:group_id', component:MembersGroupDetailsComponent},
    {path:'member-group-chat/:group_id', component:MemberGroupChatComponent},
    {path:'nutritions/nutrition-details/:id', component:NutritionDetailsComponent},
    {path:'runnings/running-details', component:RunningDetailsComponent},
    {path:'recipes', component:RecipesMgtComponent},
    {path:'recipes/details', component:RecipesDetailsComponent},
    {path:'recipes/add', component:RecipesAddComponent},
    {path:'bmi-calculator', component:BmiCalculatorComponent},
    {path:'calories-calculator', component:CaloriesCalculatorComponent},
    {path:'calories-calculator/result', component:ResultComponent},
    {path:'vo2-calculator', component:Vo2CalculatorComponent},
    {path:'vo2-calculator-result', component:Vo2ResultComponent},
    {path:'faq', component:FaqComponent},
    {path:'terms-condition', component:TermsConditionComponent},
    {path:'privacy', component:PrivacyComponent},
  
    {path:'vo2-calculator/calculate', component:CalculateComponent},





    
  ]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachLayoutRoutingModule { }
