import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-add-running',
  templateUrl: './add-running.component.html',
  styleUrls: ['./add-running.component.css']
})
export class AddRunningComponent {

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ){
    let runningData:any = this.router.getCurrentNavigation()?.extras?.state;
    if(runningData){
      this.runningDataForEdit = runningData?.formatedData;
      this.runningDataForShow = runningData?.data;
      this.is_edit = (runningData?.is_edit) ? true : false;
    }
  }
  is_edit:boolean = false;
  runningDataForEdit:any;
  runningDataForShow:any;
  
  localStorageData:any;
  member_user_id: any;
  running_type:any = {
    1 : "Easy Running",
    2 : "Quality Session",
    3 : "Race"
  }
  ngOnInit(): void {

    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id
        if (!this.member_user_id) {
          // this.router.navigate(['/coach-layout/my-members']);
          history.back();
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach()
          if(this.localStorageData && 
            this.localStorageData.login_token && 
            this.localStorageData.coach_id
          ){
            this.runningForm.patchValue({
              token:this.localStorageData.login_token,
              coach_id:this.localStorageData.coach_id,
              user_id:this.member_user_id
            })
            if(this.runningDataForEdit){
              debugger
              console.log(this.runningDataForEdit);
              this._running_type_ = this.runningDataForEdit?.running_type;
              this.imageUrl = this.runningDataForEdit.image;
              if(this.runningDataForEdit?.running_type == 2) this.selectSets(this.runningDataForEdit?.sets);
              this.runningForm.patchValue({
                user_id:this.runningDataForEdit?.user_id,
                image:this.runningDataForEdit?.image || "",
                description:this.runningDataForEdit?.description || "",
                running_type:`${this.runningDataForEdit?.running_type}`,
                name:this.runningDataForEdit?.name || "",
                distance:`${this.runningDataForEdit?.distance || ''}`,
                distance_unit:this.runningDataForEdit?.distance_unit || "",
                duration:`${this.runningDataForEdit?.duration || ''}`,
                duration_unit:this.runningDataForEdit?.duration_unit || "",
                planned_pace:this.runningDataForEdit?.planned_pace || "",
                actual_pace:`${this.runningDataForEdit?.actual_pace || ''}`,
                actual_pace_unit:this.runningDataForEdit?.actual_pace_unit || "",
                warmup_distance:`${this.runningDataForEdit?.warmup_distance || ''}`,
                warmup_distance_unit:this.runningDataForEdit?.warmup_distance_unit || "",
                race_type:this.runningDataForEdit?.race_type || "",
                cooldown_distance:`${this.runningDataForEdit?.cooldown_distance || ""}`,
                cooldown_distance_unit:this.runningDataForEdit?.cooldown_distance_unit || '',
                time:this.runningDataForEdit?.time || "",
                vo2:this.runningDataForEdit?.vo2 || "",
                admin_running_id: !this.is_edit ? this.runningDataForEdit.running_id : ""
              })
            }
          }
        }
      }
    })
  }

  selectSets(setData:any[]){
    this.sets = setData.map((data)=>{
      let dataMake = {
        reps:data?.reps,distance:data?.distance,distance_unit:data?.distance_unit||"km",effort:data?.effort,
        avg_pace:data?.avg_pace,avg_pace_unit:data?.avg_pace_unit||"min/km",recovery_between_reps:data?.recovery_between_reps,
        recovery_between_reps_unit:data?.recovery_between_reps_unit||"min",reps_data:[],
        isSet:true
      }
      for (let index = 0; index < data?.pace.length; index++) {
        dataMake.reps_data.push({
          rep:data?.pace[index].rep || `Rep ${index}`,
          pace:data?.pace[index].pace,
          pace_unit:data?.pace[index].pace_unit || "min/km",
          differenceReps:"",
        })        
      }
      return dataMake;
    })
  }

  imageUrl:any;
  file_for_send:any;
  onFileSelected(event:any): void {
    const file = event.target.files[0];
    this.file_for_send = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = reader.result as string;
      };
    }
  }

  runningForm:FormGroup = new FormGroup({
    token:new FormControl("",[Validators.required]),
    coach_id:new FormControl("",[Validators.required]),
    user_id:new FormControl("",[Validators.required]),
    image:new FormControl(""),
    description:new FormControl(""),
    running_type:new FormControl("1",[Validators.required]),
    name:new FormControl("",[Validators.required]),

    // for Easy Running
    distance:new FormControl(""),
    distance_unit:new FormControl("km"),
    duration:new FormControl(""),
    duration_unit:new FormControl("min"),
    planned_pace:new FormControl("4:06 /km - 4:31 /km"),
    actual_pace:new FormControl(""),
    actual_pace_unit:new FormControl("min/km"),

    //  for Race
    warmup_distance:new FormControl(""),
    warmup_distance_unit:new FormControl("km"),
    race_type:new FormControl(""),
    cooldown_distance:new FormControl(""),
    cooldown_distance_unit:new FormControl("km"),
    time:new FormControl(""), 
    vo2: new FormControl(""),
    admin_running_id:new FormControl("")

    // sets:new FormControl("",[Validators.required]),
  })

  _running_type_ = 1;
  /**
  _running_type_ = 1   == Easy Running 
  _running_type_ = 2   == Quality Session
  _running_type_ = 3   == Race
   */
  selectionChange(event){
    // console.log(event);
    this._running_type_ = event.value;
    if(this._running_type_ == 1){
      this.runningForm.patchValue({
        planned_pace:"4:06 /km - 4:31 /km"
      })
    }else{
      this.runningForm.patchValue({
        planned_pace:""
      })
    }
    
  }  

  // race_type:string = "";
  selectionRaceType(event){
    // this.race_type = event.value;
    let findValue = this.running_vo2_max.find((rv2m:any)=> rv2m.show_value_for_race_type == event.value)
    if(!findValue) return;
    this.runningForm.patchValue({
      vo2:findValue.value
    })
  }

  selectActualPaceForHourAndMin(event,key_select){
    // if(event.key == "ArrowLeft" || event.key == "ArrowRight")  return event.preventDefault();
    if(![0,1,2,3,4,5,6,7,8,9,'0','1','2','3','4','5','6','7','8','9','Backspace','Control'].includes(event.key)) event.preventDefault();
    if(event.key == "Backspace") return;
    if(event.target.value && `${event.target.value}`.length == 5){
      event.preventDefault();
    }
    if(event.target.value && `${event.target.value}`.length == 2){
      this.runningForm.patchValue({
        [key_select]:`${event.target.value}:`
      })
      
    }
    if(this.runningForm.value[key_select].length == 3 && [6,7,8,9,'6','7','8','9'].includes(event.key)){
      this.runningForm.patchValue({
        [key_select]:`${this.runningForm.value[key_select]}0`
      })
    }
  }

  RunningQualitySectionValues: any[] = [
    {showValue:"Easy",value:"easy",avgPaceValue:"4:45/km"},
    {showValue:"Marathon",value:"marathon",avgPaceValue:"4:10/km"},
    {showValue:"Threshlod",value:"threshlod",avgPaceValue:"3:56/km"},
    {showValue:"Interval",value:"interval",avgPaceValue:"3:37/km"},
    {showValue:"Repetition",value:"repetition",avgPaceValue:"3:22/km"},
    // {showValue:"Hills",value:"hills",avgPaceValue:"Disappeared"},
    {showValue:"FastReps",value:"fastReps",avgPaceValue:"3:02/km" }
  ] 
  addRepsData(event,i){
    // console.log(event.target.value);
    console.log(event.key);
    if(![0,1,2,3,4,5,6,7,8,9,'0','1','2','3','4','5','6','7','8','9','ArrowRight','ArrowLeft','Backspace','Control'].includes(event.key)) event.preventDefault();

    let value = this.sets[i].reps + event.key;
    this.sets[i].reps_data = [];
    for (let index = 0; index < Number(value); index++) {
      // const element = array[index];
      this.sets[i].reps_data.push({
        rep:`Rep. ${index+1}`,
        pace:"",
        pace_unit:"min/km",
        differenceReps:"",
      })
    }
  }
  selectPaceForRepsSetForHourAndMin(event,i,j){
    if(![0,1,2,3,4,5,6,7,8,9,'0','1','2','3','4','5','6','7','8','9','Backspace','Control'].includes(event.key)) event.preventDefault();
    if(event.key == "Backspace") return;
    if(event.target.value && `${event.target.value}`.length == 5){
      event.preventDefault();
    }
    if(event.target.value && `${event.target.value}`.length == 2){
      this.sets[i].reps_data[j].pace = `${event.target.value}:`
    }
    if(this.sets[i]?.reps_data[j]?.pace.length == 3 && [6,7,8,9,'6','7','8','9'].includes(event.key)){
      this.sets[i].reps_data[j].pace = `${this.sets[i].reps_data[j].pace}0`;
    }
  }
  selectPaceForRepsKeyUp(event,i,j){
    // console.log(event.target.value);
    // console.log(event.key);
    debugger
    let value:string = event.target.value;
    let valueArr = `${value}`.split(":");
    let totalMinut:number = Number(valueArr[0]) * 60;
    if(valueArr[1]){
      totalMinut = totalMinut + Number(valueArr[1]);
    }
    console.log(totalMinut);
    if(this.sets[i].avg_pace){
      let findData = this.RunningQualitySectionValues.find((rqsv:any)=>{
        return rqsv.value == this.sets[i].effort;
      })
      if(findData && findData.avgPaceValue){
        let findDataHourMinArr = `${findData.avgPaceValue}`.split("/")[0].split(":");
        let minOfFindData = Number(findDataHourMinArr[0]) * 60;
        if(findDataHourMinArr[1]){
          minOfFindData = minOfFindData + Number(findDataHourMinArr[1]);
        }
        // if()
        this.sets[i].reps_data[j].differenceReps = `${minOfFindData - totalMinut}`;
      }
    }
  }
  sets:any [] = [];
  addSet(){
    this.sets.push(
      {
        reps:"",distance:"",distance_unit:"km",effort:"",
        avg_pace:"",avg_pace_unit:"min/km",recovery_between_reps:"",
        recovery_between_reps_unit:"min",reps_data:[],
        isSet:true
      }
    )
  }
  addResetSet(){
    this.sets.push(
      {
        distance:"",distance_unit:"km",
        isSet:false
      }
    )
  }
  removeSet(i:number){
    this.sets.splice(i,1)
  }

  selectionChangeOfEffort(event,i){
    // console.log(event);
    debugger;
    let value = event.value;
    let findData = this.RunningQualitySectionValues.find((rqsv:any)=>{
      return rqsv.value == value;
    })
    if(findData){
      this.sets[i].avg_pace = findData.avgPaceValue;
      if(this.sets[i].reps){
        for (let index = 0; index < Number(this.sets[i].reps); index++) {
          let event = {
            target:{
              value:this.sets[i].reps_data[index].pace
            }
          }
          this.selectPaceForRepsKeyUp(event,i,index);
        }
      }
    }
  }

  running_vo2_max:any[] = [
    {show_value:"Marathon",value:`2:55:58 @ 4:10/km`,show_value_for_race_type:"marathon"},
    {show_value:"Half Marathon",value: `1:24:17 @ 4:00/km`,show_value_for_race_type:"half_marathon"},
    {show_value:"15K",value: `58:33 @ 3:54/km`,show_value_for_race_type:"15K"},
    {show_value:"10K",value: `38:05 @ 3:49/km`,show_value_for_race_type:"10K"},
    {show_value:"5K",value: `18:22 @ 3:40/km`,show_value_for_race_type:"5K"},
    {show_value:"2Mi",value: `11:28 @ 3:34/km`,show_value_for_race_type:"2Mi"},
    {show_value:"3200m",value: `11:23 @ 3:34/km`,show_value_for_race_type:"3200m"},
    {show_value:"3K",value: `10:37 @ 3:32/km`,show_value_for_race_type:"3K"},
    {show_value:"1Mi",value: `5:22 @ 3:20/km`,show_value_for_race_type:"1Mi"},
    {show_value:"1600m",value: `5:19 @ 3:20/km`,show_value_for_race_type:"1600m"},
    {show_value:"1500m",value: `4:54 @ 3:16/km`,show_value_for_race_type:"1500m"},
    {show_value:"800m",value: `2:23 @ 2:59/km`,show_value_for_race_type:"800m"},
  ]

  showErrorForEasyRunning = {
    distance:false,
    distance_unit:false,
    duration:false,
    duration_unit:false,
    actual_pace:false,
    actual_pace_unit:false,
    planned_pace:false
  }
  showErrorForRace = {
    warmup_distance:false,
    warmup_distance_unit:false,
    race_type:false,
    cooldown_distance:false,
    cooldown_distance_unit:false,
    time:false,
    vo2:false

  }
  showErrorForQuality = {
    warmup_distance:false,
    warmup_distance_unit:false,
    cooldown_distance:false,
    cooldown_distance_unit:false
  }
  submitted:boolean = false;
  invalid_for_validation:boolean = false;
  allValidationReset(){
    for (const key in this.showErrorForEasyRunning) {
      if (Object.prototype.hasOwnProperty.call(this.showErrorForEasyRunning, key)) {
        this.showErrorForEasyRunning[key] = false;
      }
    }
  }
  submit(){
    debugger
    this.submitted = true;
    this.invalid_for_validation = false;
    this.allValidationReset();
    if(this.runningForm.invalid){
      console.log("Invalid");
      return;
    }

    let saveData = new FormData();
    saveData.append("","");

    saveData.append("token",this.runningForm.value.token);
    saveData.append("coach_id",this.runningForm.value.coach_id);
    saveData.append("user_id",this.runningForm.value.user_id);
    if(this.file_for_send){
      saveData.append("image",this.file_for_send);
    }
    // else if(this.imageUrl){
    //   // saveData.append("image","");
    //   saveData.append("image",this.imageUrl);
    // }

    if(this.runningForm.value.admin_running_id){
      saveData.append("admin_running_id",this.runningForm.value.admin_running_id);
    }

    saveData.append("description",this.runningForm.value.description || "");
    saveData.append("running_type",this.runningForm.value.running_type);
    saveData.append("name",this.runningForm.value.name);
    if(this._running_type_ == 1){
      for (const key in this.showErrorForEasyRunning) {
        if (Object.prototype.hasOwnProperty.call(this.showErrorForEasyRunning, key)) {
          if(!this.runningForm.value[key]){
            this.showErrorForEasyRunning[key] = false;
            this.invalid_for_validation = true;
          }else{
            saveData.append(key,this.runningForm.value[key]);
          }
        }
      }
    }else if(this._running_type_ == 2){
      for (const key in this.showErrorForQuality) {
        if (Object.prototype.hasOwnProperty.call(this.showErrorForQuality, key)) {
          if(!this.runningForm.value[key]){
            this.showErrorForQuality[key] = false;
            this.invalid_for_validation = true;
          }else{
            saveData.append(key,this.runningForm.value[key]);
          }
        }
      }
      let sets_data = this.sets.filter((sd)=>{
        if(
          sd.isSet
        ){
          if(
            sd.reps && sd.distance && sd.distance_unit && sd.effort &&
            sd.avg_pace && sd.avg_pace_unit && sd.recovery_between_reps &&
            sd.recovery_between_reps_unit && sd.reps_data
          ){
            return sd;
          }
        }else{
          if(
            sd.distance && sd.distance_unit
          ){
            return sd;
          }
        }
      }).map((sdData)=>{
        if( sdData.isSet){
          return {
            reps: sdData.reps, distance: sdData.distance, distance_unit: sdData.distance_unit, effort: sdData.effort,
            avg_pace: sdData.avg_pace, avg_pace_unit: sdData.avg_pace_unit, recovery_between_reps: sdData.recovery_between_reps,
            recovery_between_reps_unit: sdData.recovery_between_reps_unit, reps_data: sdData.reps_data
          }
        }else{
          return {
            distance: sdData.distance, 
            distance_unit: sdData.distance_unit
          }
        }
      })
      if(sets_data.length){
        saveData.append("sets",JSON.stringify(sets_data));
      }
    }else{
      for (const key in this.showErrorForRace) {
        if (Object.prototype.hasOwnProperty.call(this.showErrorForRace, key)) {
          if(!this.runningForm.value[key]){
            this.showErrorForRace[key] = false;
            this.invalid_for_validation = true;
          }else{
            saveData.append(key,this.runningForm.value[key]);
          }
        }
      }
    }
    if(this.invalid_for_validation){
      console.log("Invalid");
      return;      
    }
    let apiString = "user/running/add";
    if(this.is_edit){
      apiString = "user/running/update";
      if(!this.runningDataForEdit.running_id){
        return;
      }
      saveData.append("running_id",this.runningDataForEdit.running_id);
    }

    this.api.coach_post(apiString,saveData).subscribe({
      next:(result:any)=>{
          if(result && !result.error){
            this.router.navigate([`/coach-layout/member-details/${this.member_user_id}`])
          }  
      },
      error:(err)=>{
        console.log(err);
      }
    })

  }


  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }

  
}
