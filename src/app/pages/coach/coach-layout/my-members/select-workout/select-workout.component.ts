import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-select-workout',
  templateUrl: './select-workout.component.html',
  styleUrls: ['./select-workout.component.css']
})
export class SelectWorkoutComponent implements OnInit{
    
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ){
    this.workOutData = this.router.getCurrentNavigation()?.extras?.state;
    console.log(this.workOutData);
    
  }

  workOutData:any;
  member_user_id:any;
  ngOnInit(): void {

    this.activeRoute.params.subscribe({
      next: (res: any) => {
        debugger
        this.member_user_id = res?.user_id
        if (!this.member_user_id) {
          // history.back();
          this.router.navigate(['/coach-layout/my-members']);
        } else {
          this.levelList();
          this.equipmentslList(); 
          this.adminWorkout();
          this.frontMusclesList();
          this.bankMusclesList();
          if(this.workOutData){
            this.workOutForm.patchValue(this.workOutData);
            if(this.workOutData.exercises){
              this.exercise_value = this.workOutData.exercises || [];
            }
          }else{
            this.router.navigate(['/coach-layout/member-details/'+this.member_user_id]);
          }
        }
      }
    })
  }
  workOutForm:FormGroup = new FormGroup({
    token:new FormControl("",[Validators.required]),
    coach_id:new FormControl("",[Validators.required]),
    user_id:new FormControl("",[Validators.required]),
    name:new FormControl("",[Validators.required]),
    description:new FormControl("",[Validators.required]),
    level_id:new FormControl("",[Validators.required]),
    types:new FormControl("",[Validators.required]),
    time_required:new FormControl("",[Validators.required]),
    equipments:new FormControl(""),
    calories:new FormControl(""),
    days:new FormControl("",[Validators.required]),
    weeks:new FormControl("",[Validators.required]),
    exercises:new FormControl(""),
    image:new FormControl("",[Validators.required]),
    muscles:new FormControl("",[Validators.required]),
    admin_workout_id:new FormControl("")
  })

  levels:any[] = [];
  levelList(){
    this.api.common_api_post("levels",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.levels = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  equipments:any[] = [];
  equipmentslList(){
    this.api.common_api_post("equipments",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.equipments = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  bankMuscles:any[] = [];
  bankMusclesList(){
    this.api.common_api_post("back",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.bankMuscles = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  frontMuscles:any[] = [];
  frontMusclesList(){
    this.api.common_api_post("front",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.frontMuscles = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  search_text:string = "";
  level_selection:string="";
  equipment_selection:any[] = [];
  back_selection:any[] = [];
  frount_selection:any[] = [];
  debounce:any;
  filterForSearch(){
    if(this.debounce){
      clearTimeout(this.debounce);
    }
    this.debounce = setTimeout(() => {
      this.applyFilter();
    }, 2000);
    console.log(this.search_text);
  }

  applyFilter(){
    // console.log(this.equipment_selection);
    // return
    let mus:string = [...this.back_selection,...this.frount_selection].toString();
    // console.log(
    //   "",
    //   mus,
    //   this.level_selection,
    //   this.equipment_selection
    // );
    
    this.adminWorkout(
      this.search_text,
      mus,
      this.level_selection,
      this.equipment_selection.toString()
    );
  }

  clearFilter(){
    this.search_text = "";
    this.level_selection="";
    this.equipment_selection = [];
    this.back_selection = [];
    this.frount_selection = [];
    this.adminWorkout()
  }

  exercise_list:any[] = [];
  adminWorkout(
      name:string="", 
      muscles:string="",
      level_id:string="", 
      equipments:string=""
  ){
    this.api.common_api_post("exercises",{
      name,muscles,level_id,equipments
    }).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.exercise_list = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  dataURLtoFile(dataurl: String, filename: string) {
    var arr: any[] = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[arr.length - 1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }


  exercise_value:any[] = [];
  exercise_value_for_send:any[] = [];
  select_exercise(id:any,data:any){
    let ind = this.exercise_value.findIndex(ev=> ev == id);
    if(ind != -1){
      this.exercise_value.splice(ind,1)
      this.exercise_value_for_send.splice(ind,1)
    }else{
      this.exercise_value.push(id);
      let valueForSave = `${data?.exercise_id}:${data?.counting_method}:${data?.steps}:${data?.relaxation_time}`;
      this.exercise_value_for_send.push(valueForSave);
    }
    console.log(this.exercise_value_for_send);
    
  }
  check_exercise_selection(id:string){
    // return this.exercise_value.includes(id);
    return this.exercise_value.some((d)=>{return `${d}`.toLocaleLowerCase() == `${id}`.toLocaleLowerCase()});
  }
  submit(){
    console.log(this.workOutForm.value);
    if(
      this.exercise_value_for_send && 
      this.exercise_value && 
      this.exercise_value_for_send.length != this.exercise_value.length
    ){
      let el = this.exercise_list.filter((el)=>{return this.exercise_value.some(ev=> ev == el.exercise_id)});
      el.forEach((alValue:any)=>{
        let valueForSave = `${alValue?.exercise_id}:${alValue?.counting_method}:${alValue?.steps}:${alValue?.relaxation_time}`;
        this.exercise_value_for_send.push(valueForSave);
      })
    }
    this.workOutForm.patchValue({
      exercises:this.exercise_value_for_send.toString()
    })

    if(this.workOutForm.invalid){
      console.log("invalid");
      return      
    }
    
    let formData = new FormData();
    formData.append("token",this.workOutForm.value.token);
    formData.append("coach_id",this.workOutForm.value.coach_id);
    formData.append("user_id",this.workOutForm.value.user_id);
    formData.append("name",this.workOutForm.value.name);
    formData.append("description",this.workOutForm.value.description);
    formData.append("level_id",this.workOutForm.value.level_id);
    formData.append("types",this.workOutForm.value.types);
    formData.append("time_required",this.workOutForm.value.time_required);
    formData.append("equipments",this.workOutForm.value.equipments);
    formData.append("calories",this.workOutForm.value.calories);
    formData.append("days",this.workOutForm.value.days);
    formData.append("weeks",this.workOutForm.value.weeks);
    formData.append("exercises",this.workOutForm.value.exercises);
    formData.append("exercises",this.workOutForm.value.exercises);
    if(this.workOutForm.value.admin_workout_id){
      formData.append("admin_workout_id",this.workOutForm.value.admin_workout_id);
    }
    if(!`${this.workOutForm.value.image}`.startsWith('http')){
      let image = this.dataURLtoFile(this.workOutForm.value.image, "workout_image.jpeg");
      formData.append("image",image);
    }
    // else{
    //   formData.append("image",this.workOutForm.value.image);
    // }
    formData.append("muscles",this.workOutForm.value.muscles);

    let apiString = "user/workouts/add";
    if(this.workOutData?.is_edit){
      apiString = "user/workouts/update";
      formData.append('workout_id',this.workOutData?.workout_id);
    }
    this.api.coach_post(apiString,formData).subscribe({
      next:(result:any)=>{
          if(result && !result.error){
            this.router.navigate([`/coach-layout/member-details/${this.member_user_id}`])
          }  
      },
      error:(err)=>{
        console.log(err);
      }
    })
  }

  goBack(){
    let route = `/coach-layout/member-details/${this.member_user_id}`
    this.router.navigate([route]);
  }

 

}
