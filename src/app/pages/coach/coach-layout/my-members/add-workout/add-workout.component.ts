import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-add-workout',
  templateUrl: './add-workout.component.html',
  styleUrls: ['./add-workout.component.css']
})
export class AddWorkoutComponent implements OnInit{

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ){
    let workOutData:any = this.router.getCurrentNavigation()?.extras?.state;
    if(workOutData){
      this.workOutDataForEdit = workOutData?.formatedData;
      this.workOutDataForShow = workOutData?.data;
      this.is_edit = (workOutData?.is_edit) ? true : false;
    }
  }
  is_edit:boolean = false;
  workOutDataForEdit:any;
  workOutDataForShow:any;
  
  localStorageData:any;
  member_user_id: any;
  ngOnInit(): void {

    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id
        if (!this.member_user_id) {
          // this.router.navigate(['/coach-layout/my-members']);
          history.back();
        } else {
          this.typeList();
          this.levelList();
          this.bankMusclesList();
          this.equipmentslList();
          this.frontMusclesList();
          this.localStorageData = this.localstorage.getWebLocalForCoach()
          if(this.localStorageData && 
            this.localStorageData.login_token && 
            this.localStorageData.coach_id
          ){
            this.workOutForm.patchValue({
              token:this.localStorageData.login_token,
              coach_id:this.localStorageData.coach_id,
              user_id:this.member_user_id
            })
            if(this.workOutDataForEdit){
              debugger
                console.log(this.workOutDataForEdit);
                this.imageUrl = this.workOutDataForEdit.image;
                this.equipment_value = this.workOutDataForEdit.equipments || [];
                this.back = this.workOutDataForEdit?.muscles || [];
                this.front = this.workOutDataForEdit?.muscles || [];
                if(this.equipment_value && this.equipment_value.length !=0 ){
                  this.isEquipmentRequired = true;
                }
                this.days = this.workOutDataForEdit?.days || [];
                this.workOutForm.patchValue({
                  admin_workout_id:this.workOutDataForEdit.admin_workout_id,
                  calories:this.workOutDataForEdit.calories,
                  description:this.workOutDataForEdit.description,
                  equipments:this.workOutDataForEdit.equipments,
                  image:this.workOutDataForEdit.image,
                  level_id:this.workOutDataForEdit.level_id,
                  muscles:this.workOutDataForEdit.muscles,
                  name:this.workOutDataForEdit.name,
                  time_required:this.workOutDataForEdit.time_required,
                  types:this.workOutDataForEdit.types,
                  user_id:this.workOutDataForEdit.user_id,
                  days:this.workOutDataForEdit.days,
                  weeks:`${this.workOutDataForEdit.weeks}`,
                  exercises:this.workOutDataForEdit.exercises
                })
            }
          }
        }
      }
    })
  }

  imageUrl:any;
  onFileSelected(event:any): void {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = reader.result as string;
      };
    }
  }

  workOutForm:FormGroup = new FormGroup({
    token:new FormControl("",[Validators.required]),
    coach_id:new FormControl("",[Validators.required]),
    user_id:new FormControl("",[Validators.required]),
    name:new FormControl("",[Validators.required]),
    description:new FormControl("",[Validators.required]),
    level_id:new FormControl("",[Validators.required]),
    types:new FormControl("",[Validators.required]),
    time_required:new FormControl(""),
    equipments:new FormControl(""),
    calories:new FormControl("500"),
    days:new FormControl("",[Validators.required]),
    weeks:new FormControl("",[Validators.required]),
    exercises:new FormControl(""),
    image:new FormControl("",[Validators.required]),
    muscles:new FormControl("",[Validators.required]),
    admin_workout_id:new FormControl("")
  })

  back:any[] = [];
  front:any[] = [];
  submitted:boolean=false;
  submit(){
    this.submitted = true;
    this.workOutForm.patchValue({
      days:this.days.toString(),
      equipments:this.equipment_value.toString(),
      image:this.imageUrl,
      // muscles: [...this.back, ...this.front].filter((bf,index,arr)=> arr.findIndex(arf=> arf==bf) == index ).toString()
      muscles: [... new Set([...this.back,...this.front])].toString()
    })
    console.log(this.workOutForm.value);
    // this.workOutForm.value.days = this.days.toString();
    // this.workOutForm.value.equipments = this.equipment_value.toString();
    if(this.workOutForm.invalid){
      console.log("Invalid");
      return;
    };
    let data = this.workOutForm.value;
    if(this.is_edit){
      data.is_edit = this.is_edit;
      data.workout_id = this.workOutDataForEdit?.workout_id 
    }
    data.types = data.types.toString();
    // this.router.getCurrentNavigation().extras.state
    let routerLink="/coach-layout/select-workout/"+this.member_user_id;
    this.router.navigate([routerLink],{
      state:data
    })
  }
  
  types:any[] = [];
  typeList(){
    this.api.common_api_post("types",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.types = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  levels:any[] = [];
  levelList(){
    this.api.common_api_post("levels",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.levels = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  equipments:any[] = [];
  equipmentslList(){
    this.api.common_api_post("equipments",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.equipments = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  bankMuscles:any[] = [];
  bankMusclesList(){
    this.api.common_api_post("back",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.bankMuscles = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }
  frontMuscles:any[] = [];
  frontMusclesList(){
    this.api.common_api_post("front",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.frontMuscles = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  days:any[] = [];
  selectDay(event:any){
    if(event.target.checked){
      if(!this.days.includes(event.target.value)){
        this.days.push(event.target.value);
      }
    }else{
      let index = this.days.findIndex(i=>{return i == event.target.value});
      if(index != -1) this.days.splice(index,1);
      // if(!this.days.includes(event.target.value)){
      //   this.days.push(event.target.value);
      // }
    }
  }

  checkDay(value:string){
    // return this.days.includes(value);
    return this.days.some((d)=>{return `${d}`.toLocaleLowerCase() == `${value}`.toLocaleLowerCase()});
  }

  equipment_value:any[] = [];
  selectEquipment(event){
    let index = this.equipment_value.findIndex(i=>{return i == event.value});
    if(index != -1) this.equipment_value.splice(index,1);
    else this.equipment_value.push(event.value);
    console.log(this.equipment_value);    
  }

  checkEquipment(level_id:string){
    // return this.equipment_value.includes(level_id);
    return this.equipment_value.some((d)=>{return `${d}`.toLocaleLowerCase() == `${level_id}`.toLocaleLowerCase()});

  }
  isEquipmentRequired:boolean=false;
  EquipmentRequired(event){
    this.isEquipmentRequired = event.checked;
    this.equipment_value = [];
  }

  goBack(){
    let route = `/coach-layout/member-details/${this.member_user_id}`
    this.router.navigate([route]);
  }
  
  
}
