import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, pipe } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { calculateDaysDifference, calculateYearsDifference } from 'src/app/utils/common-function';

@Component({
  selector: 'app-my-members',
  templateUrl: './my-members.component.html',
  styleUrls: ['./my-members.component.css']
})
export class MyMembersComponent implements OnInit{

  constructor(
    // private dataService : DataSharedService, 
    private localStorage : SessionStorageService, 
    private api : ApiService, 
    private router : Router
  ) { 
    
  }

  staticImg = 'assets/images/Coach/upload_profile_pic.png'
  localStorageData:any;
  searchControl!: FormControl;
  tableValue: number = 0;
  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.localStorageData && 
      this.localStorageData.login_token && 
      this.localStorageData.coach_id
    ){
      // this.searchControl.patchValue("");
      this.tableValue = 0;
      this.searchControl = new FormControl();
      this.changeTab(this.tableValue);
      this.activeInactiveMember(this.tableValue);
      this.searchControl.valueChanges.pipe(debounceTime(2000)).subscribe(()=>{
          this.activeInactiveMember(this.tableValue,this.searchControl.value)
      })
    }
  }

  membersList(
    type:number,
    token:String,
    CoachId:String,
    Search:String = ""
  ){
    return new Promise((resolve,reject)=>{
      let data:any = {
        "token":token,
        "coach_id":CoachId,
      }
      let apiEndPoint = "";
      if(type == 0){
        apiEndPoint = "customers/active";
      }else{
        apiEndPoint = "customers/inactive";
      }
      if(Search){
        data.search = Search;
        apiEndPoint = `${apiEndPoint}/search`;
      }
      this.api.coach_post(apiEndPoint,data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
              resolve({
                success:true,
                data:res.data
              })
          }else{
            reject({
              success:false,
              message:res.message
            })
          }
        },
        error: (err: any) => {
          console.log(err);
          // this.error = err.error.message;
          reject({
            success:false,
            message:err.error.message
          })
        }
      });
  
    })
  }

  memberList:any[]=[];
  changeTab(type:number){
    this.searchControl.patchValue("");
    this.tableValue = type;
    // this.activeInactiveMember(type);
  }
  activeInactiveMember(type:number,Search:string=""){
    this.tableValue = type;
    this.membersList(
      type,
      this.localStorageData.login_token,
      this.localStorageData.coach_id,
      Search
    ).then((response:any)=>{
      this.memberList = response["data"];
    })
  }

  getAge(dob:string){
    if(dob){
      return calculateYearsDifference(dob);
    }
    return 0;
  }
  getPackageDayDiff(subscription_date:string,subscription_expiry_date:string){
    if(subscription_date && subscription_expiry_date){
      return calculateDaysDifference(subscription_date,subscription_expiry_date)
    }
    return 0;
  }

  goToDetail(item:any){
    this.router.navigateByUrl(`/coach-layout/member-details/${item?.user_id}`,{state:item});
  }


}
