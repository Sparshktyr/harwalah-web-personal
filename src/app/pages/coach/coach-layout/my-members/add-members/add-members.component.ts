import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerifyEmailComponent } from '../verify-email/verify-email.component';
import { ApiService } from 'src/app/service/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionStorageService } from 'src/app/service/session-storage.service';

interface Country {
  country_id:Number,
  name:String,
  mobile_code:any,
  country_code:String
}
@Component({
  selector: 'app-add-members',
  templateUrl: './add-members.component.html',
  styleUrls: ['./add-members.component.css']
})
export class AddMembersComponent implements OnInit{

  hide = true;
  hide1 = true;
  selectedCountryCode: any = '+91';

  constructor(
    public dialog: MatDialog,
    private api:ApiService,
    private localStorage : SessionStorageService,
  ){

  }
  localStorageData:any;
  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.localStorageData && 
      this.localStorageData.login_token && 
      this.localStorageData.coach_id
    ){
      this.addMemberForm.patchValue({
        token:this.localStorageData.login_token,
        coach_id:this.localStorageData.coach_id
      })
    }
    this.getCountryList()
  }

  addMemberForm:FormGroup = new FormGroup({
    first_name:new FormControl('',Validators.required),
    last_name:new FormControl('',Validators.required),
    email:new FormControl('',[Validators.required,Validators.email]),
    mobile:new FormControl('',[Validators.required,Validators.minLength(8),Validators.maxLength(13)]),
    country_id:new FormControl('',Validators.required),
    password:new FormControl('',Validators.required),
    confirm_password:new FormControl('',[Validators.required]),
    token:new FormControl('',Validators.required),
    coach_id:new FormControl('',Validators.required)
  })
  
  pwdMatchValidator() {
    return this.addMemberForm.controls['password'].value === this.addMemberForm.controls['confirm_password'].value
       ? null : {'mismatch': true};
 }

 selectCountryCode(code: string) {
  this.selectedCountryCode = code;
}
  getError(field:string){
    return this.addMemberForm.controls[field];
  }
//   {
//     "token":"d7239edc53a779ac153f226259c7e884",
//     "coach_id":"1",
//     "first_name":"Rahul",
//     "last_name":"Chauhan",
//     "email":"sumit.chauhan189@gmail.com",
//     "mobile":"8604106692",
//     "country_id":"1",
//     "password":"12345678"
// }
  country: Country[] = [];
  getCountryList(){
    this.api.common_api_post("countries",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.country = res.data;
        }
      },
      error: (err: any) => {
        console.log(err);
        // this.error = err.error.message;
      }
    });
  }

  AddCustomerSendOtpError:String="";
  submitted:Boolean=false;
  Verifyemail(){
    this.submitted = true;
    if(this.addMemberForm.invalid || this.pwdMatchValidator()){
      return;
    }
    this.AddCustomerSendOtp(
      this.addMemberForm.value.token,
      this.addMemberForm.value.coach_id,
      this.addMemberForm.value.email
    ).then((response:any)=>{
      this.dialog.open(VerifyEmailComponent,{
        disableClose:true,
        data:{
          formData:this.addMemberForm.value,
          response:response
        }
      })
    }).catch((err)=>{
      if(err && err.message){
        this.AddCustomerSendOtpError = err.message;
        setTimeout(() => {
          this.AddCustomerSendOtpError = "";
        }, 4000);
      }
    })
    
  }

  AddCustomerSendOtp(token:String,coach_id:String,email:String){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "coach_id":coach_id,
        "email":email
      }
      this.api.coach_post("customers/send/otp",data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            resolve({
              data:res.data
            })
          }else{
            reject({
              message:res.message
            })  
          }
        },
        error: (err: any) => {
          reject({
            message:err.error.message
          })
          // this.error = err.error.message;
        }
      });
  
    })
  }



}
