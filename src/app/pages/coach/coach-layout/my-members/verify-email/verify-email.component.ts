import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
  otp: string = '';
  resendDisabled: boolean = false;
  timer: number = 30;

  constructor(
    public dialogRef: MatDialogRef<VerifyEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api:ApiService,
    private router:Router

  ) { }

  ngOnInit(): void {
    this.startTimer();
    console.log(this.data);
    if(
      !this.data ||
      !this.data.formData
    ){
      document.getElementById("closeDialog")?.click();
    }
  }

  showError:String="";
  CustomerVerifyOtp(
    token:String,
    CoachId:String,
    email:String,
    otp:String
  ){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "coach_id":CoachId,
        "email":email,
        "otp":otp
      }
      this.api.coach_post("customers/verify/otp",data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
              resolve({
                success:true,
                message:res.message
              })
          }else{
            reject({
              success:false,
              message:res.message
            })
          }
        },
        error: (err: any) => {
          console.log(err);
          // this.error = err.error.message;
          reject({
            success:false,
            message:err.error.message
          })
        }
      });
  
    })
  }

  AddCustomer(AddCustomerFormData:any){
      this.api.coach_post("customers/add",AddCustomerFormData).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            this.router.navigateByUrl("/coach-layout/member-successfully").then(()=>{
              document.getElementById("closeDialog")?.click();
            });
            // routerLink="/coach-layout/member-successfully"
          }else{
            this.showError = res.message;
            setTimeout(() => {
              this.showError = ""
            }, 3000);
          }
        },
        error: (err: any) => {
          console.log(err);
          this.showError = err.error.message;
          setTimeout(() => {
            this.showError = ""
          }, 3000);

          // this.error = err.error.message;
        }
      });
    }

    submitVerifyOtp(){
      this.CustomerVerifyOtp(
        this.data.formData.token,
        this.data.formData.coach_id,
        this.data.formData.email,
        this.otp
      ).then(response=>{
        this.AddCustomer(this.data.formData)
      }).catch(err=>{
        this.showError = err.message;
        setTimeout(() => {
          this.showError = ""
        }, 3000);

      })
    }

    successMessage:String="";
    ResendOtp(){
      this.AddCustomerSendOtp(
        this.data.formData.token,
        this.data.formData.coach_id,
        this.data.formData.email
      ).then((response:any)=>{
        this.successMessage= "Otp Send Success.";
        this.startTimer();
        setTimeout(() => {
        this.successMessage= "";
        }, 3000);
      })
    }
    
  AddCustomerSendOtp(token:String,coach_id:String,email:String){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "coach_id":coach_id,
        "email":email
      }
      this.api.coach_post("customers/send/otp",data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            resolve({
              data:res.data,
              message:res.message
            })
          }else{
            reject({
              message:res.message
            })  
          }
        },
        error: (err: any) => {
          reject({
            message:err.error.message
          })
          // this.error = err.error.message;
        }
      });
  
    })
  }

  // ================
  startTimer() {
    this.resendDisabled = true; // Disable the resend button
    let interval = setInterval(() => {
      this.timer--; // Decrement the timer
      if (this.timer === 0) {
        clearInterval(interval); // Clear the interval when timer reaches 0
        this.resendDisabled = false; // Enable the resend button
        this.timer = 30; // Reset the timer to 30 seconds
      }
    }, 1000); // Update timer every second
  }

  formatTimer(time: number): string {
    let minutes: string = Math.floor(time / 60).toString().padStart(2, '0');
    let seconds: string = (time % 60).toString().padStart(2, '0');
    return `${minutes}:${seconds} sec`;
  }
}
