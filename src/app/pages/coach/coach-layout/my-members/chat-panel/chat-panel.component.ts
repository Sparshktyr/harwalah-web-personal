import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { initializeApp } from 'firebase/app';
import { getDatabase, onValue, push, ref, set } from "firebase/database";
import { environment } from "src/environments/environment";


@Component({
  selector: 'app-chat-panel',
  templateUrl: './chat-panel.component.html',
  styleUrls: ['./chat-panel.component.css']
})
export class ChatPanelComponent {

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) { }

  localStorageData: any;

  member_user_id: any;
  traniee_data:any;
  app:any; 
  database:any
  ngOnInit(): void {
    this.traniee_data = history.state;
    console.log(this.traniee_data);
    
    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id
        if (!this.member_user_id || !this.traniee_data) {
          this.router.navigate(['/coach-layout/my-members'])
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach();
          console.log(this.localStorageData);
          if(!this.localStorageData){
            return;
          }
          this.app = initializeApp(environment.firebaseConfig);
          this.database = getDatabase(this.app);
          this.getDataFromDataBase(this.database,this.member_user_id,this.localStorageData.coach_id);        

        }
      }
    })

  }

  message_data:any[] = [];
  message_list_url:any;
  getDataFromDataBase(db:any,user_id:string,coach_id:string){
    // 
    this.message_list_url = ref(db, `Messages/PrivateMessages/${user_id}_${coach_id}`);
    // if()
    onValue(this.message_list_url, (snapshot) => {
      let data:any[] = snapshot.val();
      // console.log(data);
      if(data){
        let message_data:any = []
        // need to logic here  
        for (const key in data) {
          if (Object.prototype.hasOwnProperty.call(data, key)) {
            // const element = object[key];
            message_data.push(data[key]);
          }
        }
        this.message_data = message_data;
        setTimeout(() => {
          this.scrollToBottom("scrollProperty");
         }, 50);
        // console.log(this.message);
      }else{
        this.getDataFromDataBaseSecondMethod(db,user_id,coach_id);
      }
      
      // updateStarCount(postElement, data);
    });
  }

  getDataFromDataBaseSecondMethod(db:any,user_id:string,coach_id:string){
    // 
    this.message_list_url = ref(db, `Messages/PrivateMessages/${coach_id}_${user_id}`);
    // if()
    onValue(this.message_list_url, (snapshot) => {
      let data:any[] = snapshot.val();
      // console.log(data);
      if(data){
        // need to logic here  
        let message_data:any = []
        for (const key in data) {
          if (Object.prototype.hasOwnProperty.call(data, key)) {
            // const element = object[key];
            message_data.push(data[key]);
          }
        }
        this.message_data = message_data;
        setTimeout(() => {
          this.scrollToBottom("scrollProperty");
         }, 50);
        // console.log(this.message);
      }      
      // updateStarCount(postElement, data);
    });
  }

  writeData(db,user_id:string,coach_id:string,data:any) {
    debugger
    // const db = getDatabase();
    if(!this.message_list_url){
      this.message_list_url = ref(db, `Messages/PrivateMessages/${user_id}_${coach_id}`);
    }
    const newPostRef = push(this.message_list_url);
    set(newPostRef,data).then(res=>{
      console.log(res);
      this.message = "";
      setTimeout(() => {
        this.scrollToBottom("scrollProperty");
       }, 50);
    }).catch((err)=>{console.log(err);});
  }

  message:string = "";
  submit(){
    if(!this.message){
      return;
    }
    let makeData:any = {
      message:this.message,
      readStatus:"",
      receiverId:this.traniee_data?.user?.user_id,
      receiverImage:this.traniee_data?.user?.profile_image || "",
      receiverName:`${this.traniee_data?.user?.first_name} ${this.traniee_data?.user?.last_name}`,
      senderId:this.localStorageData?.coach_id,
      senderImage:this.localStorageData?.profile_image,
      senderName:`${this.localStorageData?.first_name} ${this.localStorageData?.last_name}`,
      timestamp: new Date().getTime(),
      uid:"",
    }    
    this.writeData(this.database,this.member_user_id,this.localStorageData?.coach_id,makeData);
  }

  scrollToBottom(id){
    debugger
    const element = document.getElementById(id);
    element.scrollTop = element.scrollHeight;
}

}
