import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-running-details',
  templateUrl: './running-details.component.html',
  styleUrls: ['./running-details.component.css']
})
export class RunningDetailsComponent {

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) {
    // this.workoutData = this.router.getCurrentNavigation()?.extras?.state;
  }
  localStorageData: any;
  runningData: any;

  running_type:any = {
    1 : "Easy Running",
    2 : "Quality Session",
    3 : "Race"
  }
  member_user_id: any;
  workout_id: any;
  ngOnInit(): void {
    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id;
        this.workout_id = res?.workout_id;
        if (!this.member_user_id || !this.workout_id) {
          this.router.navigate(['/coach-layout/my-members'])
          return
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach();
          if (this.localStorageData &&
            this.localStorageData.login_token &&
            this.localStorageData.coach_id
          ) {
            // console.log(this.workoutData);
            this.RunningDetail(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.workout_id
            );
          }
        }
      }
    })
  }

  RunningDetail(
    token: string,
    coach_id: string,
    running_id: string
  ) {
    this.api.coach_post_app("user/running/detail", {
      token, coach_id,running_id
    }).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          if(res.data && res.data){
            this.runningData = res.data;
            console.log(this.runningData)
          }
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  goToEditRunning(data:any){
    if(!this.runningData){
      return;
    }
    console.log(this.runningData);
    let routerLink=`/coach-layout/my-members/add-running/${this.member_user_id}`;

    let formatedData :any = {
      token: "",
      coach_id: "",
      user_id: this.member_user_id,
      image: data?.image,
      description: data?.description,
      running_type: data?.running_type,
      name: data?.name,
      // distance: ,
      // distance_unit: ,
      // duration: ,
      // duration_unit: ,
      // planned_pace: ,
      // actual_pace: ,
      // actual_pace_unit: ,
      // warmup_distance: ,
      // warmup_distance_unit: ,
      // race_type: ,
      // cooldown_distance: ,
      // cooldown_distance_unit: ,
      // time: ,
      // vo2: ,
    }
    if(data.running_type == '1'){
      formatedData.distance = `${data?.distance}`;
      formatedData.distance_unit = data?.distance_unit;
      formatedData.duration = `${data?.duration}`;
      formatedData.duration_unit = data?.duration_unit;
      formatedData.actual_pace = data?.actual_pace;
      formatedData.actual_pace_unit = data?.actual_pace_unit;
      formatedData.planned_pace = "4:06 ~ 4:31/km";
    }else if(data.running_type == '2'){
      formatedData.warmup_distance = data?.warmup_distance;
      formatedData.warmup_distance_unit = data?.warmup_distance_unit;
      formatedData.cooldown_distance = data?.cooldown_distance;
      formatedData.cooldown_distance_unit = data?.cooldown_distance_unit;
      formatedData.sets = data?.sets;
    }else{
      formatedData.warmup_distance = `${data?.warmup_distance}`; 
      formatedData.warmup_distance_unit = data?.warmup_distance_unit; 
      formatedData.race_type = data?.race_type;
      formatedData.cooldown_distance = data?.cooldown_distance; 
      formatedData.cooldown_distance_unit = data?.cooldown_distance_unit; 
      formatedData.time = data?.time;
      formatedData.vo2 = data?.vo2;
    }
    formatedData.running_id = data.running_id;


    this.router.navigate([routerLink],{state:{
      formatedData,
      is_edit:true
    }})
  }

  deletePopup(){
    let isConfirm = window.confirm("Are you sure you want to delete.");
    if(isConfirm){
      this.deleteRunning(
        this.localStorageData.login_token,
        this.localStorageData.coach_id,
        this.workout_id
      );
    }
  }

  deleteRunning(
    token:string,
    coach_id: string,
    running_id: string
  ){
    // http://staging.harwalah.com/api/v1/web/coach/user/running/remove
    // let formData = new FormData()
    this.api.coach_post_app("user/running/remove", {
      coach_id,running_id,token
    }).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          // if(res.error && res.data){
            this.runningData = res.data;
            console.log(this.runningData)
            this.router.navigate(["/coach-layout/member-details/"+this.member_user_id])
          // }
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }


}
