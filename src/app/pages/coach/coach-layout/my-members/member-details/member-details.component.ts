import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";
import { convertSecondIntoMinuts } from "src/app/utils/common-function";

@Component({
  selector: "app-member-details",
  templateUrl: "./member-details.component.html",
  styleUrls: ["./member-details.component.css"],
})
export class MemberDetailsComponent implements OnInit {
  challengesListData: any;
  InviteToChallengesList: any;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) {
    // console.log(this.router.getCurrentNavigation()?.extras?.state);
  }
  localStorageData: any;

  member_user_id: any;
  traniee_data: any;
  ngOnInit(): void {
    this.traniee_data = history.state;
    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id;
        if (!this.member_user_id || !this.traniee_data) {
          this.router.navigate(["/coach-layout/my-members"]);
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach();
          if (
            this.localStorageData &&
            this.localStorageData.login_token &&
            this.localStorageData.coach_id
          ) {
            this.adminWorkout(
              this.localStorageData.login_token,
              this.localStorageData.coach_id
            );
            this.WorkoutList(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.member_user_id
            );
            this.adminRunning(
              this.localStorageData.login_token,
              this.localStorageData.coach_id
            );
            this.RunningList(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.member_user_id
            );
            this.challengesList(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.member_user_id
            );
            this.challengesListToInvite(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.member_user_id
            );
          }
        }
      },
    });
  }

  tab_select: number = 0;
  selectTab(event) {
    this.tab_select = event;
  }

  running_type: any = {
    1: "Easy Running",
    2: "Quality Session",
    3: "Race",
  };

  admin_workout_list: any[] = [];
  adminWorkout(token: string, coach_id: string) {
    this.api
      .coach_post_app("workouts/admin", {
        token,
        coach_id,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.admin_workout_list = res.data;
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }

  admin_easy_running_list: any[] = [];
  admin_quality_running_list: any[] = [];
  admin_race_running_list: any[] = [];
  adminRunning(token: string, coach_id: string, type: string = "4") {
    this.api
      .coach_post_app("running/admin", {
        token,
        coach_id,
        type,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            let admin_running_list: any[] = res.data;
            this.admin_easy_running_list = admin_running_list.filter(
              (arl) => arl.running_type == 1
            );
            this.admin_quality_running_list = admin_running_list.filter(
              (arl) => arl.running_type == 2
            );
            this.admin_race_running_list = admin_running_list.filter(
              (arl) => arl.running_type == 3
            );
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }

  coach_workout_list: any[] = [];
  WorkoutList(token: string, coach_id: string, user_id: string) {
    this.api
      .coach_post_app("user/workouts", {
        token,
        coach_id,
        user_id,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.coach_workout_list = res.data;
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }

  coach_running_list: any[] = [];
  RunningList(token: string, coach_id: string, user_id: string) {
    this.api
      .coach_post_app("user/running", {
        token,
        coach_id,
        user_id,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.coach_running_list = res.data;
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }

  convertSecondIntoMinut(sec: number) {
    return this.convertSecondIntoMinut(sec);
  }

  goToWorkOutDetail(element: any = undefined) {
    if (!element || (element && !element.workout_id)) {
      return;
    }
    this.router.navigateByUrl(
      "/coach-layout/workout-coach/" +
        this.member_user_id +
        "/" +
        element.workout_id,
      {
        state: element,
      }
    );
  }
  goToRunningDetail(element: any = undefined) {
    if (!element) {
      return;
    }
    this.router.navigateByUrl(
      "/coach-layout/my-members/running-details/" +
        this.member_user_id +
        "/" +
        element.running_id
    );
  }

  goTOAdminEditWorkOut(data: any) {
    if (!data || !data?.workout) {
      return;
    }
    let routerLink = `/coach-layout/add-workout/${this.member_user_id}`;
    let types = data?.workout?.types.map((ty) => {
      return ty?.type_id;
    });
    let equipments = data?.workout?.equipments.map((eq) => {
      return eq?.equipment_id;
    });
    let muscles = data?.workout?.muscles.map((eq) => {
      return eq?.muscles_id;
    });
    let formatedData = {
      token: "",
      coach_id: "",
      user_id: this.member_user_id,
      name: data?.workout?.name,
      description: data?.workout?.description,
      level_id: data?.workout?.level_id,
      types: types,
      time_required: data?.workout?.time_required,
      equipments: equipments,
      calories: data?.workout?.calories_burn,
      days: "",
      weeks: "",
      exercises: "",
      image: data?.workout?.image,
      muscles: muscles,
      admin_workout_id: data?.workout_id,
    };
    this.router.navigate([routerLink], {
      state: {
        formatedData,
        data,
      },
    });
  }

  goTOAdminEditRunning(data: any, running_type: string = "1") {
    if (!data) {
      return;
    }
    console.log(data);

    // return;
    let routerLink = `/coach-layout/my-members/add-running/${this.member_user_id}`;
    let types = data?.workout?.types.map((ty) => {
      return ty?.type_id;
    });
    let equipments = data?.workout?.equipments.map((eq) => {
      return eq?.equipment_id;
    });
    let muscles = data?.workout?.muscles.map((eq) => {
      return eq?.muscles_id;
    });
    let formatedData: any = {
      token: "",
      coach_id: "",
      user_id: this.member_user_id,
      image: data?.image,
      description: data?.description,
      running_type: data?.running_type,
      name: data?.name,
      // distance: ,
      // distance_unit: ,
      // duration: ,
      // duration_unit: ,
      // planned_pace: ,
      // actual_pace: ,
      // actual_pace_unit: ,
      // warmup_distance: ,
      // warmup_distance_unit: ,
      // race_type: ,
      // cooldown_distance: ,
      // cooldown_distance_unit: ,
      // time: ,
      // vo2: ,
    };
    if (running_type == "1") {
      formatedData.distance = `${data?.distance}`;
      formatedData.distance_unit = data?.distance_unit;
      formatedData.duration = `${data?.duration}`;
      formatedData.duration_unit = data?.duration_unit;
      formatedData.actual_pace = data?.actual_pace;
      formatedData.actual_pace_unit = data?.actual_pace_unit;
      formatedData.planned_pace = "4:06 ~ 4:31/km";
    } else if (running_type == "2") {
      formatedData.warmup_distance = data?.warmup_distance;
      formatedData.warmup_distance_unit = data?.warmup_distance_unit;
      formatedData.cooldown_distance = data?.cooldown_distance;
      formatedData.cooldown_distance_unit = data?.cooldown_distance_unit;
      formatedData.sets = data?.sets;
    } else {
      formatedData.warmup_distance = `${data?.warmup_distance}`;
      formatedData.warmup_distance_unit = data?.warmup_distance_unit;
      formatedData.race_type = data?.race_type;
      formatedData.cooldown_distance = data?.cooldown_distance;
      formatedData.cooldown_distance_unit = data?.cooldown_distance_unit;
      formatedData.time = data?.time;
      formatedData.vo2 = data?.vo2;
    }
    formatedData.running_id = data?.running_id;
    this.router.navigate([routerLink], {
      state: {
        formatedData,
        data,
      },
    });
  }

  goToChat() {
    let routerLink = `/coach-layout/chat-page/${this.member_user_id}`;
    this.router.navigateByUrl(routerLink, { state: this.traniee_data });
  }

  challengesList(token: string, coach_id: string, user_id: string) {
    this.api
      .coach_post_app("user/challenges", {
        token,
        coach_id,
        user_id,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.challengesListData = res.data;
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }
  challengesListToInvite(token: string, coach_id: string, user_id: string) {
    this.api
      .coach_post_app("challenges/user/invite", {
        token,
        coach_id,
        user_id,
      })
      .subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.InviteToChallengesList = res.data;
          }
        },
        error: (err: any) => {
          console.log(err.error.message);
          // this.error = err.error.message;
        },
      });
  }
}
