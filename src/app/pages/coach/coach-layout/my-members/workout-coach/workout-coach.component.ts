import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-workout-coach',
  templateUrl: './workout-coach.component.html',
  styleUrls: ['./workout-coach.component.css']
})
export class WorkoutCoachComponent implements OnInit{

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) {
    // this.workoutData = this.router.getCurrentNavigation()?.extras?.state;
  }
  localStorageData: any;
  workoutData: any;


  member_user_id: any;
  workout_id: any;
  ngOnInit(): void {
    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.user_id;
        this.workout_id = res?.workout_id;
        if (!this.member_user_id || !this.workout_id) {
          this.router.navigate(['/coach-layout/my-members'])
          return
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach()
          if (this.localStorageData &&
            this.localStorageData.login_token &&
            this.localStorageData.coach_id
          ) {
            // console.log(this.workoutData);
            this.WorkoutDetail(
              this.localStorageData.login_token,
              this.localStorageData.coach_id,
              this.workout_id
            );
          }
        }
      }
    })
  }

  WorkoutDetail(
    token: string,
    coach_id: string,
    workout_id: string
  ) {
    this.api.coach_post_app("user/workouts/detail", {
      token, coach_id,workout_id
    }).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          if(res.data && res.data[0]){
            this.workoutData = res.data[0];
          }
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        // this.error = err.error.message;
      }
    });
  }

  goToEditWorkout(){
    if(!this.workoutData){
      return;
    }
    console.log(this.workoutData);
    let routerLink=`/coach-layout/add-workout/${this.member_user_id}`;
    let types = this.workoutData?.types.map((ty)=>{return ty?.type_id});
    let equipments = this.workoutData?.equipments.map((eq)=>{return eq?.equipment_id});
    let muscles = this.workoutData?.muscles.map((eq)=>{return eq?.muscles_id});
    let days = this.workoutData?.days.map((eq)=>{return eq?.day});
    let exercises = this.workoutData?.exercises.map((eq)=>{return eq?.exercise_id});
    let formatedData = {
      token:"",
      coach_id:"",
      user_id:this.member_user_id,
      name:this.workoutData?.name,
      description:this.workoutData?.description,
      level_id:this.workoutData?.level_id,
      types:types,
      time_required:this.workoutData?.time_required,
      equipments:equipments,
      calories:this.workoutData?.calories_burn,
      days:days,
      weeks:this.workoutData?.for_weeks,
      exercises:exercises,
      image:this.workoutData?.image,
      muscles:muscles,
      workout_id:this.workoutData?.workout_id
    }
    this.router.navigate([routerLink],{state:{
      formatedData,
      is_edit:true
    }})
  }


}
