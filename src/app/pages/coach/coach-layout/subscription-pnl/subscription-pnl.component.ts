import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { calculateDaysDifference } from 'src/app/utils/common-function';

@Component({
  selector: 'app-subscription-pnl',
  templateUrl: './subscription-pnl.component.html',
  styleUrls: ['./subscription-pnl.component.css']
})
export class SubscriptionPnlComponent implements OnInit{

  constructor(
    private sesionStorage:SessionStorageService,
    private api : ApiService
  ){}

  coachData:any;
  planData:any;
  ngOnInit(): void {
    this.coachData = this.sesionStorage.getWebLocalForCoach();
    this.planData = this.coachData.plan;
  }
  package_type:any = {
    '1':"1 month",
    '2':"6 month",
    '3':"12 month"
  }
  subscription_auto_renew:any={
    '1':"Enabled",
    '0':"Disabled"
  }

  getDay(){
    return calculateDaysDifference(new Date().toDateString(),this.planData.expiry_date).toFixed(0)
  }
  
  changeStatus(event:any){
    console.log(event.checked);
    let status:number = 0;
    if(event.checked){
      status = 1;
    }else{
      // this.changeStatusActiveInactiveCoach(0)
      status = 0;
    }
    if(
      this.coachData.login_token,
      this.coachData.coach_id
    ){
      this.changeStatusAutoRenew(
        this.coachData.login_token,
        this.coachData.coach_id,
        status
      ).then((result:any)=>{
        // if(result && result.data && result.data.auto_renew){
          this.coachData.subscription_auto_renew = Number(status);
          this.sesionStorage.setWebLocalForCoach(this.coachData);
        // }
      });
    }
    
  }

  changeStatusAutoRenew(
    token:String,
    coach_id:String,
    status:number
  ){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "coach_id":coach_id,
        "auto_renew":`${status}`
    }
    this.api.coach_post("packages/auto-renew",data).subscribe({
      next: (res: any) => {
        if(res && !res.error){
            resolve({
              success:true,
              data:res.data
            })
        }else{
          reject({
            success:false,
            message:res.message
          })
        }
      },
      error: (err: any) => {
        console.log(err);
        // this.error = err.error.message;
        reject({
          success:false,
          message:err.error.message
        })
      }
    });

  })
  }


}
