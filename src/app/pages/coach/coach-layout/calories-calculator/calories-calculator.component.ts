import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calories-calculator',
  templateUrl: './calories-calculator.component.html',
  styleUrls: ['./calories-calculator.component.css']
})
export class CaloriesCalculatorComponent {
  calculatorForm: FormGroup;
  selectedGender: string | null = null;
  selectedFitnessLevel: string | null = null;

  constructor(private fb: FormBuilder, private router : Router) {
    this.calculatorForm = this.fb.group({
      dateOfBirth: ['', Validators.required],
      height: ['', [Validators.required, Validators.min(50), Validators.max(300)]],
      weight: ['', [Validators.required, Validators.min(10), Validators.max(500)]]
    });
  }

  selectGender(gender: string): void {
    this.selectedGender = gender;
  }

  selectFitnessLevel(level: string): void {
    this.selectedFitnessLevel = level;
  }

  calculateCalories(): void {
    if (this.calculatorForm.invalid || !this.selectedGender || !this.selectedFitnessLevel) {
      return;
    }

    const { dateOfBirth, height, weight } = this.calculatorForm.value;

    // Calculate Age
    const age = this.getAge(dateOfBirth);

    // Calculate BMR
    const bmr = this.calculateBMR(weight, height, age, this.selectedGender);

    // Activity level adjustment based on Fitness Level
    const activityMultiplier = this.getActivityMultiplier(this.selectedFitnessLevel);
    const adjustedCalories = bmr * activityMultiplier;

    console.log({
      BMR: bmr,
      AdjustedCalories: adjustedCalories,
      Age: age,
      Gender: this.selectedGender,
      FitnessLevel: this.selectedFitnessLevel
    });

    this.router.navigateByUrl('/coach-layout/calories-calculator/result', {state : {data : {
      BMR: bmr,
      AdjustedCalories: adjustedCalories,
      Age: age,
      Gender: this.selectedGender,
      FitnessLevel: this.selectedFitnessLevel,
      type : 'calories'
    }}})

  }

  private getAge(dateOfBirth: string): number {
    const birthDate = new Date(dateOfBirth);
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDiff = today.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  private calculateBMR(weight: number, height: number, age: number, gender: string): number {
    if (gender === 'male') {
      // BMR formula for males
      return 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age);
    } else {
      // BMR formula for females
      return 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age);
    }
  }

  private getActivityMultiplier(level: string): number {
    switch (level) {
      case 'Beginner':
        return 1.2; // Sedentary
      case 'Inexperienced':
        return 1.375; // Lightly active
      case 'Average level':
        return 1.55; // Moderately active
      case 'Advanced':
        return 1.725; // Very active
      case 'Expert':
        return 1.9; // Super active
      default:
        return 1.2; // Default to sedentary
    }
  }
}
