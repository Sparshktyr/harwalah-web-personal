import { Component } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {
stateData : any
  constructor(){

  }
  ngOnInit(){
    this.stateData = window.history.state['data']
  }
}
