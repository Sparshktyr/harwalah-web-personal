import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-coach-workouts',
  templateUrl: './coach-workouts.component.html',
  styleUrls: ['./coach-workouts.component.css']
})
export class CoachWorkoutsComponent {
  localStorageData: any;
  workoutsSave: any;
  constructor(
    private localStorage: SessionStorageService,
    private api: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach();
    if (
      this.localStorageData &&
      this.localStorageData.login_token &&
      this.localStorageData.coach_id
    ) {
      this.workouts(
        this.localStorageData.login_token,
        this.localStorageData.coach_id
      );
    }
  }

  workouts(token: any, coachId: any) {
    this.api
      .coach_post("workouts", { token: token, coach_id: coachId })
      .subscribe({
        next: (res: any) => {
          this.workoutsSave = res.data;
        },
      });
  }

  goToWorkoutDetail(element: any = undefined) {
    if (!element) {
      return;
    }
    this.router.navigateByUrl(
      "/coach-layout/workout-coach/" + element?.user_id +"/" +element.workout_id
    );
  }

  onSearch(event : any){
    this.api
    .coach_post("workouts", { token: this.localStorageData.login_token, coach_id: this.localStorageData.coach_id, name : event.target.value })
    .subscribe({
      next: (res: any) => {
        this.workoutsSave = res.data;
      },
    });
  }

}
