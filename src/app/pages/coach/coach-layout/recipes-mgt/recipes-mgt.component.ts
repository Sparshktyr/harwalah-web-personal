import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "src/app/service/api.service";
import { SessionStorageService } from "src/app/service/session-storage.service";

@Component({
  selector: "app-recipes-mgt",
  templateUrl: "./recipes-mgt.component.html",
  styleUrls: ["./recipes-mgt.component.css"],
})
export class RecipesMgtComponent {
  recepieData: any;
  localStorageData: any;
  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private localStorage: SessionStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach();
    if(this.localStorageData){
      this.recepieList();
    }
  }

  recepieList(): void {
    this.api.coach_post('recipes', {
      token : this.localStorageData.login_token, 
      coach_id : this.localStorageData.coach_id
    }).subscribe({
      next: (res:any) => {
        this.recepieData = res.data; 
      },
      error: (err) => {
        console.error('Error fetching happening challenges:', err);
      }
    });
  }

  goToDetails(element:any){
    this.router.navigateByUrl('/coach-layout/recipes/details', {state : {data : element}})
  }
}
