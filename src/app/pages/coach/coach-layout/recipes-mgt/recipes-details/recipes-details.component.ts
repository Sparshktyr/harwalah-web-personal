import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-recipes-details',
  templateUrl: './recipes-details.component.html',
  styleUrls: ['./recipes-details.component.css']
})
export class RecipesDetailsComponent {

  recepieData: any;
  localStorageData: any;
  constructor(
    private localStorage: SessionStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach();
    this.recepieData =  history.state['data']
    console.log("this.recepieData", this.recepieData)
  }
}
