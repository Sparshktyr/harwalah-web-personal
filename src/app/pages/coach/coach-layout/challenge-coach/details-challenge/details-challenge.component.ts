import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-details-challenge',
  templateUrl: './details-challenge.component.html',
  styleUrls: ['./details-challenge.component.css']
})
export class DetailsChallengeComponent {

  challengeData: any;

  constructor(private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras.state) {
      this.challengeData = navigation.extras.state['challengeData'];
      console.log(this.challengeData)
    }
  }
  ngOnInit(){
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras.state) {
      this.challengeData = navigation.extras.state['challengeData'];
      console.log(this.challengeData)
    }
    this.challengesBanner
   }
 
   challengesBanner: OwlOptions = {
     loop: true,
     mouseDrag: false,
     touchDrag: false,
     pullDrag: false,
     dots: false,
     navText: ['', ''],
     responsive: {
       0: {
         items: 1
       },
       400: {
         items: 1
       },
       740: {
         items: 1
       },
       940: {
         items: 1
       }
     },
     nav: false
   }
}
