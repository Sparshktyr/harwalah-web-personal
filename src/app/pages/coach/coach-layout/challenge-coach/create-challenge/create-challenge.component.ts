import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-create-challenge',
  templateUrl: './create-challenge.component.html',
  styleUrls: ['./create-challenge.component.css']
})
export class CreateChallengeComponent {

  imagePreview: string | ArrayBuffer | null = null;
 // imagePreviews: string[];

  imagePreviews: string[] = [];
  localStorageData: any;
  onFilesSelected(event: Event) {
    const files = (event.target as HTMLInputElement).files;

    if (files) {
      this.imagePreviews = [];  
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreviews.push(reader.result as string);
        };
        reader.readAsDataURL(file);
      }
    }
  }

  removeImage(index: number) {
    this.imagePreviews.splice(index, 1);
  }
  

  challengeForm: FormGroup;
  isPaid: boolean = false;

  constructor(private fb: FormBuilder, private api : ApiService, private localStorage : SessionStorageService, private router : Router) {
    this.challengeForm = this.fb.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      registration_start_date: ['', Validators.required],
      registration_end_date: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      duration: [''],
      ultimate_goal: [''],
      daily_steps_goal: [''],
      registrationLink: [''],
      location: [''],
      city: [''],
      is_paid: ['0'],
      registration_fee: [{ value: '', disabled: true }],
      description: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()

    this.onChanges();
    const today = new Date();
    this.challengeForm.get('registration_start_date').setValue(today);
  }
  onStartDateChange(startDate: Date): void {
    // Update the minimum date for registration_end_date
    this.challengeForm.get('registration_end_date').setValue(null); // Reset end date
    this.challengeForm.get('registration_end_date').updateValueAndValidity();
  }
  onChanges(): void {
    this.challengeForm.get('is_paid')?.valueChanges.subscribe(value => {
      this.isPaid = value === '1';
      if (this.isPaid) {
        this.challengeForm.get('registration_fee')?.enable();
      } else {
        this.challengeForm.get('registration_fee')?.disable();
        this.challengeForm.get('registration_fee')?.setValue('');
      }
    });

    this.challengeForm.get('registration_start_date')?.valueChanges.subscribe(() => {
      this.validateDates();
    });
    this.challengeForm.get('registration_end_date')?.valueChanges.subscribe(() => {
      this.validateDates();
    });
    this.challengeForm.get('start_date')?.valueChanges.subscribe(() => {
      this.validateDates();
    });
    this.challengeForm.get('end_date')?.valueChanges.subscribe(() => {
      this.validateDates();
    });
  }

  validateDates(): void {
    const registrationStartDate = this.challengeForm.get('registration_start_date')?.value;
    const lastDateToRegister = this.challengeForm.get('registration_end_date')?.value;
    const startDate = this.challengeForm.get('start_date')?.value;
    const endDate = this.challengeForm.get('end_date')?.value;

    if (registrationStartDate && lastDateToRegister && new Date(registrationStartDate) > new Date(lastDateToRegister)) {
      this.challengeForm.get('registration_end_date')?.setErrors({ invalid: true });
    }

    if (startDate && endDate && new Date(startDate) > new Date(endDate)) {
      this.challengeForm.get('end_date')?.setErrors({ invalid: true });
    }
  }
  private formatDate(date: Date): string {
    if (!date) return null; 

    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');

    return `${day}-${month}-${year}`; 
  }
  onSubmit(): void {
    if (this.challengeForm.valid) {
      console.log(this.challengeForm.value);
      let data = this.challengeForm.value

      console.log(this.challengeForm.value)
      const dataToSend = {
        ...data,
        registration_start_date: this.formatDate(this.challengeForm.get('registration_start_date').value),
        registration_end_date: this.formatDate(this.challengeForm.get('registration_end_date').value),
        start_date: this.formatDate(this.challengeForm.get('start_date').value),
        end_date: this.formatDate(this.challengeForm.get('end_date').value),


        image1 : "hbhbijk", token : this.localStorageData.login_token, coach_id : this.localStorageData.coach_id
      };
      this.api.coach_post('challenges/add', dataToSend).subscribe({
        next : (res : any)=>{
          console.log('Res : ', res)  
          this.router.navigateByUrl('coach-layout/challenge')
        }
      })
    } else {
      console.log('Form is invalid');
    }
  }
}
