import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-challenge-coach',
  templateUrl: './challenge-coach.component.html',
  styleUrls: ['./challenge-coach.component.css']
})
export class ChallengeCoachComponent {
  localStorageData: any;
  happeningChallenges: any;
  upcomingChallenges: any;
  pastChallenges: any;

  filteredHappeningChallenges: any[] = [];
  filteredUpcomingChallenges: any[] = [];
  filteredPastChallenges: any[] = [];
  
  searchHappening: string = '';
  searchUpcoming: string = '';
  searchPast: string = '';

  constructor(private fb: FormBuilder, private api : ApiService, private localStorage : SessionStorageService, private router : Router) {}

 ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()

    this.loadHappeningChallenges();
    this.loadUpcomingChallenges();
    this.loadPastChallenges();
  }

  loadHappeningChallenges(): void {
    this.api.coach_post('challenges', {
      token : this.localStorageData.login_token, 
      coach_id : this.localStorageData.coach_id,
      type: "happening"}).subscribe({
      next: (res:any) => {
        this.happeningChallenges = res.data; 
        this.filteredHappeningChallenges = res.data;
      },
      error: (err) => {
        console.error('Error fetching happening challenges:', err);
      }
    });
  }

  loadUpcomingChallenges(): void {
    this.api.coach_post('challenges', {
      token : this.localStorageData.login_token, 
      coach_id : this.localStorageData.coach_id,
      type: "upcoming"}).subscribe({
      next: (res: any) => {
        this.upcomingChallenges = res.data; 
        this.filteredUpcomingChallenges = res.data;

      },
      error: (err) => {
        console.error('Error fetching upcoming challenges:', err);
      }
    });
  }

  loadPastChallenges(): void {

    this.api.coach_post('challenges', {
      token : this.localStorageData.login_token, 
      coach_id : this.localStorageData.coach_id,
      type: "past"}).subscribe({
      next: (res: any) => {
        this.pastChallenges = res.data; 
        this.filteredPastChallenges = res.data; 

      },
      error: (err) => {
        console.error('Error fetching past challenges:', err);
      }
    });
  }

  filterHappeningChallenges(event:any) {
    let searchTerm = event.target.value
    searchTerm = searchTerm.toLowerCase();
    this.happeningChallenges = this.happeningChallenges.filter((challenge : any) =>{
      console.log(challenge)
      challenge.name.toLowerCase().includes(searchTerm) ||
      challenge.type.toLowerCase().includes(searchTerm)
    });
  }

  filterUpcomingChallenges() {
    const searchTerm1 = this.searchUpcoming.toLowerCase();
    this.upcomingChallenges = this.upcomingChallenges.filter((challenge : any) =>
      challenge.name.toLowerCase().includes(searchTerm1) ||
      challenge.type.toLowerCase().includes(searchTerm1)
    );
  }

  filterPastChallenges() {
    const searchTerm2 = this.searchPast.toLowerCase();
    this.pastChallenges = this.pastChallenges.filter((challenge : any) =>{
      challenge.name.toLowerCase().includes(searchTerm2) ||
      challenge.type.toLowerCase().includes(searchTerm2)
    }
    );
  }

  goToDetails(element : any){
    this.router.navigate(['/coach-layout/challenge-details'], { state: { challengeData: element } });
  }
}
