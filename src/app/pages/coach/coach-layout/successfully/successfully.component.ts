import { Component, OnInit } from '@angular/core';
import { DataSharedService } from 'src/app/service/data-shared.service';

@Component({
  selector: 'app-successfully',
  templateUrl: './successfully.component.html',
  styleUrls: ['./successfully.component.css']
})
export class SuccessfullyComponent implements OnInit{

  constructor(
    private dataService : DataSharedService, 
  ) { }


  subSrriptionData:any;
  ngOnInit(): void {
    this.dataService.getMessage.subscribe((data : any)  => {
      this.subSrriptionData = data;
      if(!this.subSrriptionData){
        window.history.back();
      }else{
        
      }
    });
  }
}
