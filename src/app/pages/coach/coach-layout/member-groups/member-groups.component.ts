import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-member-groups',
  templateUrl: './member-groups.component.html',
  styleUrls: ['./member-groups.component.css']
})
export class MemberGroupsComponent implements OnInit{
  tableValue: any;

  constructor(
    // private dataService : DataSharedService, 
    private localStorage : SessionStorageService,
    private api : ApiService,
    private router : Router
  ) { }

  localStorageData:any;
  searchControl!: FormControl;
  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.localStorageData && 
      this.localStorageData.login_token && 
      this.localStorageData.coach_id
    ){
      this.searchControl = new FormControl();
      this.fetchGroupList();
      this.searchControl.valueChanges.pipe(debounceTime(2000)).subscribe(()=>{
        // this.activeInactiveMember(this.tableValue,this.searchControl.value)
      this.fetchGroupList(this.searchControl.value);

    })
    }
  }

  groupListData:any[]=[];
  fetchGroupList(Search:string=""){
    this.groupList(
      this.localStorageData.login_token,
      this.localStorageData.coach_id,
      Search
    ).then((response:any)=>{
      this.groupListData = response["data"];
      // console.log(this.groupListData);
    })
  }


  groupList(
    token:String,
    CoachId:String,
    Search:String=""
  ){
    return new Promise((resolve,reject)=>{
      let data:any = {
        "token":token,
        "coach_id":CoachId,
      }
      let apiEndPoint = "groups";
      if(Search){
        apiEndPoint = apiEndPoint + "/search";
        data.search=Search;
      }
      this.api.coach_post(apiEndPoint,data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
              resolve({
                success:true,
                data:res.data
              })
          }else{
            reject({
              success:false,
              message:res.message
            })
          }
        },
        error: (err: any) => {
          console.log(err);
          // this.error = err.error.message;
          reject({
            success:false,
            message:err.error.message
          })
        }
      });
  
    })
  }

  
  goToDetail(item:any){
    this.router.navigateByUrl(`/coach-layout/group-details/${item?.group_id}`,{state:item});
  }

}
