import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MemberdataDialogComponent } from '../memberdata-dialog/memberdata-dialog.component';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit{

  constructor(
    public dialog: MatDialog,
    private api:ApiService,
    private localStorage : SessionStorageService,
    private router: Router

  ){}

  localStorageData:any;
  ngOnInit(): void {
    this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.localStorageData && 
      this.localStorageData.login_token && 
      this.localStorageData.coach_id
    ){
      this.groupForm.patchValue({
        token:this.localStorageData.login_token,
        coach_id:this.localStorageData.coach_id
      })
    }
  }


  urlgroup: any;
  groupUpload: any;
  onSelectBanner(event: any) {
    if (event.target.files && event.target.files[0]) {
      // debugger
      var reader = new FileReader();
      this.groupUpload = event.target.files[0]
      reader.readAsDataURL(event.target.files[0]); 
      reader.onload = (event: any) => {
        this.urlgroup = event.target.result;
      }
    }
    console.log(this.groupUpload);
  }

  memberlists(data:any){
    this.dialog.open(MemberdataDialogComponent,{
      disableClose:true,
      width: '800px',
      data: data
    }).afterClosed().subscribe({
      next:(returnData)=>{
        console.log(returnData);
        if(returnData && returnData.success){
          this.router.navigate(["coach-layout/member-groups"]);
        }
      },error:()=>{}
    })
  }

  
  groupForm:FormGroup = new FormGroup({
    token:new FormControl('',Validators.required),
    coach_id:new FormControl('',Validators.required),
    name:new FormControl('',Validators.required),
    type:new FormControl('',Validators.required),
    about:new FormControl(''),
    members:new FormControl(''),
    // image
  })

  f(controls:string){
    return this.groupForm.controls[controls].errors;
  }

  submitted:boolean=false;
  saveGroup(){
    this.submitted = true;

    if(this.groupForm.invalid || !this.urlgroup){
      console.log("Invalid");
      return;
    }
    // let formData = new FormData();
    // formData.append('token',this.localStorageData.login_token);
    // formData.append('coach_id',this.localStorageData.coach_id);
    // formData.append('name',this.groupForm.value.name);
    // formData.append('type',this.groupForm.value.type);
    // formData.append('about',this.groupForm.value.about);
    // formData.append('image',this.groupUploadFile);
    // formData.append('members',this.localStorageData.login_token);
    let data = this.groupForm.value;
    data.image = this.urlgroup;
    this.memberlists(data);
    return;
    
    // this.api.coach_post("",formData)

  }
  

}
