import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberGroupChatComponent } from './member-group-chat.component';

describe('MemberGroupChatComponent', () => {
  let component: MemberGroupChatComponent;
  let fixture: ComponentFixture<MemberGroupChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemberGroupChatComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemberGroupChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
