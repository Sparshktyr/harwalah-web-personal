import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { initializeApp } from 'firebase/app';
import { getDatabase, onValue, push, ref, set } from "firebase/database";
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-member-group-chat',
  templateUrl: './member-group-chat.component.html',
  styleUrls: ['./member-group-chat.component.css']
})
export class MemberGroupChatComponent {
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) { }

  localStorageData: any;

  member_user_id: any;
  traniee_data:any;
  app:any; 
  database:any
  ngOnInit(): void {
    this.traniee_data = history.state;
    console.log(this.traniee_data);
    
    this.activeRoute.params.subscribe({
      next: (res: any) => {
        this.member_user_id = res?.group_id
        if (!this.member_user_id || !this.traniee_data) {
          this.router.navigate(['/coach-layout/member-groups'])
        } else {
          this.localStorageData = this.localstorage.getWebLocalForCoach();
          console.log(this.localStorageData);
          if(!this.localStorageData){
            return;
          }
          this.app = initializeApp(environment.firebaseConfig);
          this.database = getDatabase(this.app);
          this.getGroup(this.database,this.member_user_id);
          this.getDataFromDataBase(this.database,this.member_user_id);        

        }
      }
    })

  }

  message_data:any[] = [];
  message_list_url:any;
  getDataFromDataBase(db:any,group_id:string){
    // 
    this.message_list_url = ref(db, `Messages/GroupMessages/${group_id}`);
    // if()
    onValue(this.message_list_url, (snapshot) => {
      let data:any[] = snapshot.val();
      // console.log(data);
      if(data){
        let message_data:any = []
        // need to logic here  
        for (const key in data) {
          if (Object.prototype.hasOwnProperty.call(data, key)) {
            // const element = object[key];
            message_data.push(data[key]);
          }
        }
        this.message_data = message_data;
        setTimeout(() => {
          this.scrollToBottom("scrollProperty");
         }, 50);
        console.log(this.message_data);
      }
      
      // updateStarCount(postElement, data);
    });
  }

  group_data; 
  group_data_url;
  getGroup(db:any,group_id:string){
    // 
    this.group_data_url = ref(db, `Groups/group_${group_id}/information`);
    // if()
    onValue(this.group_data_url, (snapshot) => {
      this.group_data = snapshot.val();
      console.log(this.group_data);
      if(!this.group_data){
        this.writeDataForGroup(this.database,group_id)
      }
    });
  }

  writeDataForGroup(db,group_id) {
    debugger
    // const db = getDatabase();
    if(!this.group_data_url){
      this.group_data_url = ref(db, `Groups/group_${group_id}/information`);
    }
    set(this.group_data_url,{
      groupId: group_id,
      groupImage: this.traniee_data?.image,
      groupName: this.traniee_data?.name,
      timestamp: new Date().getTime()

    }).then(res=>{
      console.log(res);
      this.message = "";
      setTimeout(() => {
        this.scrollToBottom("scrollProperty");
       }, 50);
    }).catch((err)=>{console.log(err);});
  }



  writeData(db,group_id,data:any) {
    debugger
    // const db = getDatabase();
    if(!this.message_list_url){
      this.message_list_url = ref(db, `Messages/GroupMessages/${group_id}`);
    }
    const newPostRef = push(this.message_list_url);
    set(newPostRef,data).then(res=>{
      console.log(res);
      this.message = "";
      setTimeout(() => {
        this.scrollToBottom("scrollProperty");
       }, 50);
    }).catch((err)=>{console.log(err);});
  }

  message:string = "";
  submit(){
    if(!this.message){
      return;
    }
    let makeData:any = {
      chat_id:this.member_user_id,
      mediaType:"text",
      message:this.message,
      readStatus:"",
      receiverId:"",
      receiverImage:"",
      receiverName:"",
      senderId:this.localStorageData?.coach_id,
      senderImage:this.localStorageData?.profile_image,
      senderName:`${this.localStorageData?.first_name} ${this.localStorageData?.last_name}`,
      timestamp: new Date().getTime()
    }    
    this.writeData(this.database,this.member_user_id,makeData);
  }

  scrollToBottom(id){
    debugger
    const element = document.getElementById(id);
    element.scrollTop = element.scrollHeight;
  }

}
