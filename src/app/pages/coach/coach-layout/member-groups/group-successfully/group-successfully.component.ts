import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-group-successfully',
  templateUrl: './group-successfully.component.html',
  styleUrls: ['./group-successfully.component.css']
})
export class GroupSuccessfullyComponent {


  constructor(
    public dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<GroupSuccessfullyComponent>,
  ){}

  close(){
    this.dialogRef.close();
  }

}
