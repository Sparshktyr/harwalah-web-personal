import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersGroupDetailsComponent } from './members-group-details.component';

describe('MembersGroupDetailsComponent', () => {
  let component: MembersGroupDetailsComponent;
  let fixture: ComponentFixture<MembersGroupDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MembersGroupDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MembersGroupDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
