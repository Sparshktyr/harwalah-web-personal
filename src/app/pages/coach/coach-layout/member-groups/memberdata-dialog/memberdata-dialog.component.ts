
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { GroupSuccessfullyComponent } from '../group-successfully/group-successfully.component';
import { FormControl } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { debounceTime } from 'rxjs';
import { calculateDaysDifference, calculateYearsDifference } from 'src/app/utils/common-function';
import { DialogRef } from '@angular/cdk/dialog';

@Component({
  selector: 'app-memberdata-dialog',
  templateUrl: './memberdata-dialog.component.html',
  styleUrls: ['./memberdata-dialog.component.css']
})
export class MemberdataDialogComponent implements OnInit{
  
  constructor(
    public dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MemberdataDialogComponent>,
    private api : ApiService
  ){}
  
  // ngOnInit(): void {
  //   console.log('data', this.data);
    
  // }

  searchControl!: FormControl;
  tableValue: number = 0;
  memberList:any[]=[];
  staticImg = 'assets/images/Coach/upload_profile_pic.png'

  ngOnInit(): void {
  //   console.log('data', this.data);

    // this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.data && 
      this.data.token && 
      this.data.coach_id
    ){
      debugger
      // this.searchControl.patchValue("");
      this.tableValue = 0;
      this.searchControl = new FormControl();
      this.changeTab(this.tableValue);
      this.activeInactiveMember(this.tableValue);
      this.searchControl.valueChanges.pipe(debounceTime(2000)).subscribe(()=>{
          this.activeInactiveMember(this.tableValue,this.searchControl.value)
      })
    }
  }

  changeTab(type:number){
    this.searchControl.patchValue("");
    this.tableValue = type;
    // this.activeInactiveMember(type);
  }

  activeInactiveMember(type:number,Search:string=""){
    this.tableValue = type;
    this.membersList(
      type,
      this.data.token,
      this.data.coach_id,
      Search
    ).then((response:any)=>{
      this.memberList = response["data"];
    })
  }

  formatFitnessLevel(level: string): string {
    if (!level) return level;
    switch (level.toLowerCase()) {
      case 'beginer':
        return 'Beginner';
      case 'intermediate':
        return 'Intermediate';
      case 'advanced':
        return 'Advanced';
      default:
        return level.charAt(0).toUpperCase() + level.slice(1);
    }
  }

  membersList(
    type:number,
    token:String,
    CoachId:String,
    Search:String = ""
  ){
    return new Promise((resolve,reject)=>{
      let data:any = {
        "token":token,
        "coach_id":CoachId,
      }
      let apiEndPoint = "";
      if(type == 0){
        apiEndPoint = "customers/active";
      }else{
        apiEndPoint = "customers/inactive";
      }
      if(Search){
        data.search = Search;
        apiEndPoint = `${apiEndPoint}/search`;
      }
      this.api.coach_post(apiEndPoint,data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
              resolve({
                success:true,
                data:res.data
              })
          }else{
            reject({
              success:false,
              message:res.message
            })
          }
        },
        error: (err: any) => {
          console.log(err);
          // this.error = err.error.message;
          reject({
            success:false,
            message:err.error.message
          })
        }
      });
  
    })
  }

  getAge(dob:string){
    if(dob){
      return calculateYearsDifference(dob);
    }
    return 0;
  }
  getPackageDayDiff(subscription_date:string,subscription_expiry_date:string){
    if(subscription_date && subscription_expiry_date){
      return calculateDaysDifference(subscription_date,subscription_expiry_date)
    }
    return 0;
  }

  members:any[]=[];
  checkCheckedMember(user_id:any){
    return this.members.includes(user_id);
  }
  selectMember(event:any,item:any){
    console.log(item.user_id);
    let findIndex = this.members.findIndex((i)=>{return i==item.user_id});
    if(event.checked){
      if(findIndex == -1){
        this.members.push(item.user_id);
      }
    }else{
      if(findIndex != -1){
        this.members.splice(findIndex,1);
      }
    }
    console.log(this.members);    
  }
  
  groupSuccessfully(){
    let formData = new FormData();
    formData.append("token",this.data.token);
    formData.append("coach_id",this.data.coach_id);
    formData.append("name",this.data.name);
    formData.append("type",this.data.type);
    formData.append("about",this.data.about);
    let image = this.dataURLtoFile(this.data.image, "group_image.jpeg")
    formData.append("image",image);
    formData.append("members",this.members.toString());

    this.api.coach_post("groups/add",formData).subscribe({
      next:(result:any)=>{
          if(result && !result.error){
            this.dialog.open(GroupSuccessfullyComponent,{}).afterClosed().subscribe({
              next:(res)=>{
                console.log(res);
                this.dialogRef.close({success:true});
              }
            })
          }  
      },
      error:(err)=>{
        console.log(err);
      }
    })

    
    // console.log(this.dialogRef);
    
  }

  success(){
    this.dialog.open(GroupSuccessfullyComponent,{}).afterClosed().subscribe({
      next:(res)=>{
        console.log(res);
        this.dialogRef.close({success:true});
      }
    })

  }

  dataURLtoFile(dataurl: String, filename: string) {
    var arr: any[] = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[arr.length - 1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }


  filterMembers(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.memberList.filter(item => {
      return item?.user?.first_name.toLowerCase().includes(filterValue) ||
             item?.user?.last_name.toLowerCase().includes(filterValue) ||
             item?.user?.gender.toLowerCase().includes(filterValue) ||
             this.formatFitnessLevel(item?.user?.fitness_level).toLowerCase().includes(filterValue) ||
             this.getAge(item?.user?.dob).toString().includes(filterValue) ||
             (this.tableValue === 0 && this.getPackageDayDiff(item?.subscription_date, item?.subscription_expiry_date).toString().includes(filterValue));
    });
  }
}
