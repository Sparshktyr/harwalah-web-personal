import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';
import { calculateDaysDifference, calculateYearsDifference } from 'src/app/utils/common-function';

@Component({
  selector: 'app-member-dialog',
  templateUrl: './member-dialog.component.html',
  styleUrls: ['./member-dialog.component.css']
})
export class MemberDialogComponent {
  
  localStorageData: any;
  member_user_id: any;
  traniee_data: any;
  
  constructor( private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localStorage: SessionStorageService,
    public dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<MemberDialogComponent>,
    ){
      console.log(data)
    }
    staticImg = 'assets/images/Coach/upload_profile_pic.png'
    searchControl!: FormControl;
    tableValue: number = 0;
  ngOnInit(){
    this.localStorageData = this.localStorage.getWebLocalForCoach()
    if(this.localStorageData && 
      this.localStorageData.login_token && 
      this.localStorageData.coach_id
    ){
      // this.searchControl.patchValue("");
      this.tableValue = 0;
      this.searchControl = new FormControl();
      this.changeTab(this.tableValue);
      this.activeInactiveMember(this.tableValue);
      this.searchControl.valueChanges.pipe(debounceTime(1000)).subscribe(()=>{
          this.activeInactiveMember(this.tableValue,this.searchControl.value)
      })
    }
  }

  membersList(
    type:number,
    token:String,
    CoachId:String,
    Search:String = ""
  ){
    return new Promise((resolve,reject)=>{
      let data:any = {
        "token":token,
        "coach_id":CoachId,
      }
      let apiEndPoint = "";
      if(type == 0){
        apiEndPoint = "customers/active";
      }else{
        apiEndPoint = "customers/inactive";
      }
      if(Search){
        data.search = Search;
        apiEndPoint = `${apiEndPoint}/search`;
      }
      this.api.coach_post(apiEndPoint,data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
              resolve({
                success:true,
                data:res.data
              })
          }else{
            reject({
              success:false,
              message:res.message
            })
          }
        },
        error: (err: any) => {
          console.log(err);
          // this.error = err.error.message;
          reject({
            success:false,
            message:err.error.message
          })
        }
      });
  
    })
  }

  memberList:any[]=[];
  changeTab(type:number){
    this.searchControl.patchValue("");
    this.tableValue = type;
    // this.activeInactiveMember(type);
  }
  activeInactiveMember(type:number,Search:string=""){
    this.tableValue = type;
    this.membersList(
      type,
      this.localStorageData.login_token,
      this.localStorageData.coach_id,
      Search
    ).then((response:any)=>{
      this.memberList = response["data"];
    })
  }
  getAge(dob:string){
    if(dob){
      return calculateYearsDifference(dob);
    }
    return 0;
  }

  getPackageDayDiff(subscription_date:string,subscription_expiry_date:string){
    if(subscription_date && subscription_expiry_date){
      return calculateDaysDifference(subscription_date,subscription_expiry_date)
    }
    return 0;
  }

  selectedMemberIds: number[] = [];
  isSelected(memberId: number): boolean {
    return this.selectedMemberIds.includes(memberId);
  }
  
  onCheckboxChange(member: any) {
    if (this.isSelected(member.user_id)) {
      this.selectedMemberIds = this.selectedMemberIds.filter(id => id !== member.user_id);
    } else {
      this.selectedMemberIds.push(member.user_id);
    }
    // this.updateMembersString();
  }

  updateMembersString() {
    const membersString = this.selectedMemberIds.join(',');
    console.log('Selected Members:', membersString); 

    let data = {
      "token":this.data.token,
      "coach_id":this.data.coach_id,
      "group_id":this.data.group_id,
      "members":membersString
  }
  console.log(data)

  this.api.coach_post("groups/member/add", data).subscribe({
    next: (res: any) => {
      this.dialogRef.close(true)
    },
  });
  }

}
