import { Component } from '@angular/core';

@Component({
  selector: 'app-vo2-result',
  templateUrl: './vo2-result.component.html',
  styleUrls: ['./vo2-result.component.css']
})
export class Vo2ResultComponent {
  tabs = ['Race Pace', 'Training', 'Equivalent'];
  activeTab = 0;

  tableData = {
    'Race Pace': [
      { distance: '5K', pace: '51:40' },
      { distance: '1Mi', pace: '16:37.8' },
      { distance: '1K', pace: '10:20.0' },
      { distance: '800M', pace: '8:16.0' },
      { distance: '400M', pace: '4:08.0' }
    ],
    'Training': [
      { distance: '10K', pace: '52:30' },
      { distance: '1Mi', pace: '17:00' }
    ],
    'Equivalent': [
      { distance: 'Marathon', pace: '3:15:00' },
      { distance: 'Half Marathon', pace: '1:35:00' }
    ]
  };

  get activeTableData() {
    return this.tableData[this.tabs[this.activeTab]];
  }

  setActiveTab(index: number) {
    this.activeTab = index;
  }
}
