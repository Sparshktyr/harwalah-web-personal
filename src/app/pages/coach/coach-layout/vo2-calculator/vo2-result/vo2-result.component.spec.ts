import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Vo2ResultComponent } from './vo2-result.component';

describe('Vo2ResultComponent', () => {
  let component: Vo2ResultComponent;
  let fixture: ComponentFixture<Vo2ResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Vo2ResultComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Vo2ResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
