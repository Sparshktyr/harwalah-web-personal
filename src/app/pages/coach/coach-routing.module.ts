import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoachComponent } from './coach.component';
import { StepElevenComponent } from './sign-up/step-eleven/step-eleven.component';


const routes: Routes = [
  {path:'',component:CoachComponent,children:[
    {path:'create-account',loadChildren:()=>import('./sign-up/sign-up.module').then(s=>s.SignUpModule)},
    {path:'step-11',component:StepElevenComponent},
    
    ]},

    {path:'coach-layout', loadChildren:()=>import('./coach-layout/coach-layout.module').then(l=>l.CoachLayoutModule)},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachRoutingModule { }
