import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-eight',
  templateUrl: './step-eight.component.html',
  styleUrls: ['./step-eight.component.css']
})
export class StepEightComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob ||
      // !this.localSignUpData.cover_image ||
      // !this.localSignUpData.profile_image ||
      !this.localSignUpData.about ||
      !this.localSignUpData.experience || 
      !this.localSignUpData.account_holder_name || 
      !this.localSignUpData.bank_name || 
      !this.localSignUpData.account_number || 
      !this.localSignUpData.iban_number || 
      !this.localSignUpData.swift_code ||
      !this.localSignUpData.specialities ||
      !this.localSignUpData.service_description
    ){
      this.router.navigateByUrl("/login");
    }
  }

  no_of_customers:String = "0 to 3";
  submit(){
    debugger
    if(!this.no_of_customers){
      return;
    }

    this.localSignUpData.no_of_customers = this.no_of_customers;
  
    this.router.navigateByUrl("/coach/create-account/step-9").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }
}
