import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-nine',
  templateUrl: './step-nine.component.html',
  styleUrls: ['./step-nine.component.css']
})
export class StepNineComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob ||
      !this.localSignUpData.cover_image ||
      !this.localSignUpData.profile_image ||
      !this.localSignUpData.about ||
      !this.localSignUpData.experience || 
      !this.localSignUpData.account_holder_name || 
      !this.localSignUpData.bank_name || 
      !this.localSignUpData.account_number || 
      !this.localSignUpData.iban_number || 
      !this.localSignUpData.swift_code ||
      !this.localSignUpData.specialities ||
      !this.localSignUpData.service_description || 
      !this.localSignUpData.no_of_customers
    ){
        // window.history.back();
    }
  }

  place_to_train:String = "Yes";
  isCheckStepFive(value:String){
    return this.goals.includes(value);
  }
  checkedStepFive(value:String){
    let index = this.goals.indexOf(value);
    if(index == -1){
      this.goals.push(value);
    }else{
      this.goals.splice(index,1);
    }
  }
  goals:String[] = [];

  submit(){
    debugger
    if(!this.place_to_train){
      return;
    }

    this.localSignUpData.goals = this.goals.toString();
    this.localSignUpData.place_to_train = this.place_to_train;

  
    this.router.navigateByUrl("/coach/create-account/step-10").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }
}
