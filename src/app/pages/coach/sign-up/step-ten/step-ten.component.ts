import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-ten',
  templateUrl: './step-ten.component.html',
  styleUrls: ['./step-ten.component.css']
})
export class StepTenComponent implements OnInit {


  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private api: ApiService,
    private localstorage: SessionStorageService
  ) { }


  localSignUpData: any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if (
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender ||
      !this.localSignUpData.dob ||
      !this.localSignUpData.cover_image ||
      !this.localSignUpData.profile_image ||
      !this.localSignUpData.about ||
      !this.localSignUpData.experience ||
      !this.localSignUpData.account_holder_name ||
      !this.localSignUpData.bank_name ||
      !this.localSignUpData.account_number ||
      !this.localSignUpData.iban_number ||
      !this.localSignUpData.swift_code ||
      !this.localSignUpData.specialities ||
      !this.localSignUpData.service_description ||
      !this.localSignUpData.no_of_customers ||
      !this.localSignUpData.goals ||
      !this.localSignUpData.place_to_train
    ) {
      // window.history.back();
      this.router.navigateByUrl("/login").then(() => {
      })
      return;

    }
  }

  price_per_month: String = "";

  error: String = "";
  submit() {
    debugger
    if (!this.price_per_month) {
      return;
    }

    this.localSignUpData.price_per_month = this.price_per_month;

    // this.router.navigateByUrl("/coach/step-11").then(()=>{
    //   this.localstorage.setSignupLocal(this.localSignUpData);
    // });
    // return;

    let data = new FormData();

    data.append("first_name", this.localSignUpData.first_name);
    data.append("last_name", this.localSignUpData.last_name);
    data.append("email", this.localSignUpData.email);
    data.append("mobile", this.localSignUpData.mobile);
    data.append("country_id", this.localSignUpData.country_id);
    data.append("dob", this.localSignUpData.dob);
    data.append("gender", this.localSignUpData.gender);

    let cover_image = this.dataURLtoFile(this.localSignUpData.cover_image, "coner_image.jpeg")
    let profile_image = this.dataURLtoFile(this.localSignUpData.profile_image, "profile_image.jpeg")
    console.log(cover_image);
    console.log(profile_image);
    data.append("cover_image", cover_image);
    data.append("profile_image", profile_image);

    data.append("password", this.localSignUpData.password);
    data.append("about", this.localSignUpData.about);
    data.append("experience", this.localSignUpData.experience);
    data.append("service_description", this.localSignUpData.service_description);
    // bank detail
    data.append("bank_details", JSON.stringify({
      account_holder_name: this.localSignUpData.account_holder_name,
      bank_name: this.localSignUpData.bank_name,
      account_number: this.localSignUpData.account_number,
      iban_number: this.localSignUpData.iban_number,
      swift_code: this.localSignUpData.swift_code,
    }));
    data.append("specialities", this.localSignUpData.specialities);
    data.append("no_of_customers", this.localSignUpData.no_of_customers);
    data.append("place_to_train", this.localSignUpData.place_to_train);
    data.append("goals", this.localSignUpData.goals);
    data.append("price_per_month", this.localSignUpData.price_per_month);
    data.append("device_type", "3");
    data.append("device_token", "abcd");

    // this.localSignUpData.otp

    // return

    this.api.coach_post('register', data).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          this.localstorage.removeSignupLocal();
          document.getElementById("success_model-after-complete")?.click();
          return
          this.router.navigateByUrl("/coach/step-11").then(() => {
            this.localstorage.setSignupLocal(this.localSignUpData);
          });
          // return;
        } else {
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });


  }

  dataURLtoFile(dataurl: String, filename: string) {
    var arr: any[] = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[arr.length - 1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  navigateToHome() {
    this.router.navigateByUrl("/home")
  }

}
