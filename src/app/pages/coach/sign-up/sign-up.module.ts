import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './sign-up.component';
import { StepOneComponent } from './step-one/step-one.component';
import { StepTwoComponent } from './step-two/step-two.component';
import { StepThreeComponent } from './step-three/step-three.component';
import { MaterialModule } from 'src/app/material/material.module';
import { StepFourComponent } from './step-four/step-four.component';
import { StepFiveComponent } from './step-five/step-five.component';
import { StepSixComponent } from './step-six/step-six.component';
import { StepSevenComponent } from './step-seven/step-seven.component';
import { StepEightComponent } from './step-eight/step-eight.component';
import { StepNineComponent } from './step-nine/step-nine.component';
import { StepTenComponent } from './step-ten/step-ten.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SignUpComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    StepFiveComponent,
    StepSixComponent,
    StepSevenComponent,
    StepEightComponent,
    StepNineComponent,
    StepTenComponent,
  ],
  imports: [
    CommonModule,
    SignUpRoutingModule,MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SignUpModule { }
