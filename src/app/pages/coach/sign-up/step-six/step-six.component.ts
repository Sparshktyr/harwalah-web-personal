import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-six',
  templateUrl: './step-six.component.html',
  styleUrls: ['./step-six.component.css']
})
export class StepSixComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();    
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob ||
      // !this.localSignUpData.cover_image ||
      // !this.localSignUpData.profile_image ||
      !this.localSignUpData.about ||
      !this.localSignUpData.experience || 
      !this.localSignUpData.account_holder_name || 
      !this.localSignUpData.bank_name || 
      !this.localSignUpData.account_number || 
      !this.localSignUpData.iban_number || 
      !this.localSignUpData.swift_code
    ){
      this.router.navigateByUrl("/login");
    }
  }


  isCheckStepFive(value:String){
    return this.specialities.includes(value);
  }
  checkedStepFive(value:String){
    let index = this.specialities.indexOf(value);
    if(index == -1){
      this.specialities.push(value);
    }else{
      this.specialities.splice(index,1);
    }
  }
  specialities:String[] = [];

  error:String="";
  submitted:Boolean = false;
  submit(){
    debugger
    this.submitted = true;
    if(this.specialities.length == 0){
      return;
    }

    // console.log(this.step_three_form.value);

    this.localSignUpData.specialities = this.specialities.join(',')
    // this.localSignUpData.specialities = "running"
    // console.log(this.localSignUpData);
  
    this.router.navigateByUrl("/coach/create-account/step-7").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }

}
