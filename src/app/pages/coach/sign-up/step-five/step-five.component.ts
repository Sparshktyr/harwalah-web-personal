import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-five',
  templateUrl: './step-five.component.html',
  styleUrls: ['./step-five.component.css']
})
export class StepFiveComponent implements OnInit{

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob ||
      // !this.localSignUpData.cover_image ||
      // !this.localSignUpData.profile_image ||
      !this.localSignUpData.about ||
      !this.localSignUpData.experience
    ){
      this.router.navigateByUrl("/login");
    }
    console.log(this.localSignUpData);
    
  }

  step_five_form:FormGroup = new FormGroup({
    account_holder_name:new FormControl('',Validators.required),
    bank_name:new FormControl('',Validators.required),
    account_number:new FormControl('',Validators.required),
    iban_number:new FormControl('',Validators.required),
    swift_code:new FormControl('',Validators.required),
  })

  getError(value:any){
    if(this.submitted &&  this.step_five_form.controls[value].errors){
      return true;
    }
    return false
  }

  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(this.step_five_form.invalid){
      return;
    }
    
    this.localSignUpData.account_holder_name = this.step_five_form.value.account_holder_name;
    this.localSignUpData.bank_name = this.step_five_form.value.bank_name;
    this.localSignUpData.account_number = this.step_five_form.value.account_number;
    this.localSignUpData.iban_number = this.step_five_form.value.iban_number;
    this.localSignUpData.swift_code = this.step_five_form.value.swift_code;

    this.router.navigateByUrl("/coach/create-account/step-6").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }

  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }


}
