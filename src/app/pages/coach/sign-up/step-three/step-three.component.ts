import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.css']
})
export class StepThreeComponent implements OnInit{

  
  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp
    ){
      this.router.navigateByUrl("/login");
    }
  }

  step_three_form:FormGroup = new FormGroup({
    gender:new FormControl('male',Validators.required),
    dob:new FormControl('',Validators.required),
  })

  maxDate = new Date();
  error:String="";
  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(this.step_three_form.invalid){
      // return;
    }

    // console.log(this.step_three_form.value);    

    let dob = new Date(this.step_three_form.value.dob).toLocaleDateString().split('/').map((item)=>{return (item.length == 1)?`0${item}`:item}).join("-");
    // dob = dob.map((item)=>{return (item.length == 1)?`0${item}`:item});
    this.localSignUpData.gender = this.step_three_form.value.gender;
    this.localSignUpData.dob = dob;

    // console.log(this.localSignUpData);
  
    this.router.navigateByUrl("/coach/create-account/step-4").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }
}
