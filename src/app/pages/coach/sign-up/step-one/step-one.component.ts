import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

interface Country {
  country_id:Number,
  name:String,
  mobile_code:String,
  country_code:String
}
@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.css']
})
export class StepOneComponent implements OnInit {
  
  country: Country[] = [];
  hide2= true
  hide = true;

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  ngOnInit(): void {
    this.getCountryList();
  }

  step_one_form:FormGroup = new FormGroup({
    first_name:new FormControl('',Validators.required),
    last_name:new FormControl('',Validators.required),
    email:new FormControl('',[Validators.required,Validators.email]),
    mobile:new FormControl('',Validators.required),
    country_id:new FormControl('',Validators.required),
    password:new FormControl('',Validators.required)
  })

  
  getError(value:any){
    if(this.submitted &&  this.step_one_form.controls[value].errors){
      return true;
    }
    return false
  }

  getCountryList(){
    this.api.common_api_post("countries",{}).subscribe({
      next: (res: any) => {
        if(res && !res.error){
          this.country = res.data;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });
  }

  error:String = "";
  password_error:String= "";
  submitted:Boolean = false;
  submit(){
    this.submitted = true;
    if(this.step_one_form.invalid){
      return;
    }
    if(`${this.step_one_form.value.password}`.length < 8 ){
      this.password_error = "Password Must be min 8 character";
      return;
    }

    
    // this.router.navigateByUrl("/coach/create-account/step-2").then(()=>{
    //   this.localstorage.setSignupLocal(this.step_one_form.value);
    // });
    // return;

    let data = {
      "email":this.step_one_form.value.email
    }
    this.api.coach_post('send/otp', data).subscribe({
      next: (res: any) => {
        // if(res && !res.error){
        //   this.router.navigateByUrl("/coach/create-account/step-2").then(()=>{
        //     this.localstorage.setSignupLocal(this.step_one_form.value);
        //   });
        // }
        if(res && !res.error){
          this.router.navigateByUrl("/coach/create-account/step-2").then(()=>{
            this.localstorage.setSignupLocal(this.step_one_form.value);
          });
        }else{
          this.error = res.message;
        }
      },
      error: (err: any) => {
        console.log(err.error.message);
        this.error = err.error.message;
      }
    });

  }

  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }
}
