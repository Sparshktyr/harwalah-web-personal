import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.css']
})
export class StepFourComponent implements OnInit{

  constructor(
    private router: Router,
    private activeRoute:ActivatedRoute,
    private api:ApiService,
    private localstorage:SessionStorageService
  ) { }


  localSignUpData:any;
  ngOnInit(): void {
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.first_name ||
      !this.localSignUpData.last_name ||
      !this.localSignUpData.email ||
      !this.localSignUpData.mobile ||
      !this.localSignUpData.country_id ||
      !this.localSignUpData.password ||
      !this.localSignUpData.otp ||
      !this.localSignUpData.gender || 
      !this.localSignUpData.dob
    ){
      this.router.navigateByUrl("/login");
    }
  }

  url1: any;
  // uploadProfileImage: any;
  uploadProfileImage1: any;
  onSelectBanner(event: any) {
    if (event.target.files && event.target.files[0]) {
      // debugger
      var reader = new FileReader();
      this.uploadProfileImage1 = event.target.files[0]

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url1 = event.target.result;
      }
    }
    console.log(this.uploadProfileImage1);
    
  }


  profileUrl: any;
  profileUpload: any;
  onSelectProfile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.profileUpload = event.target.files[0]
      // this.uploadImage1(this.uploadProfileImage1);

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.profileUrl = event.target.result;
      }
    }
    console.log(this.profileUpload);

  }

  step_four_form:FormGroup = new FormGroup({
    // cover_image:new FormControl('',Validators.required),
    // profile_image:new FormControl('',Validators.required),
    about:new FormControl('',Validators.required),
    experience:new FormControl('',Validators.required)
    // first_name:new FormControl('',Validators.required),
    // last_name:new FormControl('',Validators.required),
    // email:new FormControl('',[Validators.required,Validators.email]),
    // mobile:new FormControl('',Validators.required),
    // country_id:new FormControl('',Validators.required),
    // password:new FormControl('',Validators.required)
  })

  
  getError(value:any){
    if(this.submitted &&  this.step_four_form.controls[value].errors){
      return true;
    }
    return false
  }

  submitted:Boolean = false;
  // error:String = "";
  submit(){
    this.submitted = true;
    if(this.step_four_form.invalid){
      return;
    }

    console.log(this.uploadProfileImage1);
    
    // this.localSignUpData.cover_image = this.uploadProfileImage1;
    // this.localSignUpData.profile_image = this.profileUpload;
    this.localSignUpData.cover_image = this.url1;
    this.localSignUpData.profile_image = this.profileUrl

    this.localSignUpData.about = this.step_four_form.value.about;
    this.localSignUpData.experience = this.step_four_form.value.experience;

    // console.log(this.localSignUpData)

    // return
    this.router.navigateByUrl("/coach/create-account/step-5").then(()=>{
      this.localstorage.setSignupLocal(this.localSignUpData);
    });
    return;

  }

  mobileNumberValidation(event:any){
    // console.log(event);
    let values = ["1","2","3","4","5","6","7","8","9","0",'Tab','Backspace','Control','ArrowLeft','ArrowRight'];
    if(!values.includes(event.key)){
      event.preventDefault()
    }
    // let  []
  }

}
