import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoachRoutingModule } from './coach-routing.module';
import { CoachComponent } from './coach.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';
import { StepElevenComponent } from './sign-up/step-eleven/step-eleven.component';



@NgModule({
  declarations: [
  
    CoachComponent,
    StepElevenComponent,

  ],
  imports: [
    CommonModule,
    CoachRoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class CoachModule { }
