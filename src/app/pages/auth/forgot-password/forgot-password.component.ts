import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit{


  constructor(
    private api: ApiService, 
    private sessionStorage: SessionStorageService, 
    private router: Router,
    private activateRoute:ActivatedRoute
  ){}
  urlType:any={
    "USER":false,
    "COACH":false
  }
  ngOnInit(){
    this.activateRoute.params.subscribe((url_type:any)=>{
        // console.log(url_type);
        if(!url_type || (url_type.type && !["USER","COACH"].includes(url_type.type))){
          this.router.navigateByUrl("/login");
          return;
        }
        this.urlType[url_type.type] = true;
    })
  }
  
  forgetForm: FormGroup = new FormGroup({
    email: new FormControl("", Validators.required),
  });

  submitted: boolean = false; 
  error:String = "";
  forget() {
    this.submitted = true
    if (this.forgetForm.invalid) {
      return
    }

    let data = this.forgetForm.value
    if(this.urlType["USER"]){
      this.api.user_post('forget/send/otp', data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            this.router.navigateByUrl('/verify-email/USER').then(()=>{
              this.sessionStorage.setSignupLocal(this.forgetForm.value);
            })
          }else{
            this.error = res.message;
          }        
        }, error: (err: any) => {
          this.error = err.error.message;
          console.log(err);
        }
      })
    }else{
      this.api.coach_post('forget/send/otp', data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            this.router.navigateByUrl('/verify-email/COACH').then(()=>{
              this.sessionStorage.setSignupLocal(this.forgetForm.value);
            })
          }else{
            this.error = res.message;
          }        
        }, error: (err: any) => {
          this.error = err.error.message;
          console.log(err);
        }
      })
    }
    
  }
}
