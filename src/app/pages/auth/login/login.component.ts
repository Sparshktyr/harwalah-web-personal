import { Component, OnInit, ɵpublishDefaultGlobalUtils } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { DataSharedService } from 'src/app/service/data-shared.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit{

  constructor(
    private api : ApiService, 
    private sessionStorage : SessionStorageService,
    private router: Router,
    private dataSharedService : DataSharedService
  ){}

  ngOnInit(): void {
    // this.getCountryList();
    this.loginData = this.sessionStorage.removeWebLocal();
    this.loginData = this.sessionStorage.removeSignupLocal();
  }

  hide = true;
  submitted: boolean = false;

  loginForm: FormGroup = new FormGroup({
    email: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
    device_token: new FormControl("dummy_device_token"),
    device_type: new FormControl("3"),
  });

  get f() {
    return this.loginForm.controls;
  }

  loginData: any;
  error:any;

  login(loginMethod:String="user") {
    // debugger
    this.submitted = true 
    if (this.loginForm.invalid) {
      return
    }
    let data = this.loginForm.value;
    if(loginMethod == "user"){
      this.api.user_post('login', data).subscribe({
        next: (res: any) => {
          // this.loginData = res.data;

          let loginData:any = res.data;
          if(res.data && res.data.login_token && res.data.user_id){
            this.getUserProfileData(
              res.data.login_token,
              res.data.user_id
            ).then((profileDetail:any)=>{
              if(profileDetail && profileDetail[0] && profileDetail[0].plan){
                loginData.plan = profileDetail[0].plan;
              }
              this.router.navigateByUrl('/home').then(()=>{
                this.sessionStorage.setWebLocal(loginData);
                this.dataSharedService.setLoginData(JSON.stringify(loginData));
              });
            }).catch(()=>{
              this.router.navigateByUrl('/home').then(()=>{
                this.sessionStorage.setWebLocal(loginData);
                this.dataSharedService.setLoginData(JSON.stringify(loginData));
              });
            })
          }else{
            this.router.navigateByUrl('/home').then(()=>{
              this.sessionStorage.setWebLocal(loginData);
              this.dataSharedService.setLoginData(JSON.stringify(loginData));
            });
          }
          //
          if(res && !res.error){
            this.router.navigateByUrl('/home');
          }else{
            this.error = res.message; 
          }
        },
        error: (err: any) => {
          console.log(err.error.message);  
        }
      });
    }else{
      this.api.coach_post('login', data).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            // this.router.navigateByUrl('/home');
            let loginData:any = res.data;
            if(res.data && res.data.login_token && res.data.coach_id){
              this.getCoachProfileData(
                res.data.login_token,
                res.data.coach_id
              ).then((profileDetail:any)=>{
                if(profileDetail && profileDetail[0] && profileDetail[0].plan){
                  loginData.plan = profileDetail[0].plan;
                }
                this.router.navigateByUrl('/coach-layout/dashboard').then(()=>{
                  this.sessionStorage.setWebLocalForCoach(loginData);
                  this.dataSharedService.setLoginData(JSON.stringify(loginData));
                });
              }).catch(()=>{
                this.router.navigateByUrl('/coach-layout/dashboard').then(()=>{
                  this.sessionStorage.setWebLocalForCoach(loginData);
                  this.dataSharedService.setLoginData(JSON.stringify(loginData));
                });
              })
            }
          }else{
            this.error = res.message;
          }
        },
        error: (err: any) => {
          console.log(err);
        }
      });
    }
    
  }

  getCoachProfileData(
    token:String,
    coach_id:String
  ){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "coach_id":coach_id
      }
      this.api.coach_post('profile', data).subscribe({
          next: (res: any) => {
            if(res && !res.error){
              resolve(res.data)
            }else{
              reject(res)
            }
          },
          error: (err: any) => {
              reject(err)
          }
      });  
    })
  }

  
  getUserProfileData(
    token:String,
    user_id:String
  ){
    return new Promise((resolve,reject)=>{
      let data = {
        "token":token,
        "user_id":user_id
      }
      this.api.user_post('profile', data).subscribe({
          next: (res: any) => {
            if(res && !res.error){
              resolve(res.data)
            }else{
              reject(res)
            }
          },
          error: (err: any) => {
              reject(err)
          }
      });  
    })
  }


}
