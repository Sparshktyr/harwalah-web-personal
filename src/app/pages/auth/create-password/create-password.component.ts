import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.css']
})
export class CreatePasswordComponent {

  constructor(private api: ApiService, private localstorage: SessionStorageService, private router: Router,
    private activateRoute: ActivatedRoute
    ) {
  }

  urlType: any = {
    "USER": false,
    "COACH": false
  }
  localSignUpData:any;
  ngOnInit() {
    this.activateRoute.params.subscribe((url_type: any) => {
      // console.log(url_type);
      if (!url_type || (url_type.type && !["USER", "COACH"].includes(url_type.type))) {
        this.router.navigateByUrl("/login");
        return;
      }
      this.urlType[url_type.type] = true;
    })
    this.localSignUpData = this.localstorage.getSignupLocal();
    if(
      !this.localSignUpData ||
      !this.localSignUpData.email ||
      !this.localSignUpData.otp
    ){
      this.router.navigateByUrl('/forget-password').then(()=>{
        // this.sessionStorage.setSignupLocal(this.forgetForm.value);
      })
      return;
    }
  }

  hide: boolean = true;
  changePassForm: FormGroup = new FormGroup({
    password: new FormControl("", Validators.required),
    confirmPassword: new FormControl("", Validators.required),
  });
  otp: any;
  submitted: boolean = false;
  error:any;
  success:any;

  changePass() {
    this.submitted = true
    if (this.changePassForm.invalid) {
      return
    }
    if (this.changePassForm.value.password != this.changePassForm.value.confirmPassword) {
      this.error = "Password Must be Same.";
    }

    this.localSignUpData.password = this.changePassForm.value.password;

    if (this.urlType["USER"]) {
      this.api.user_post('reset/password', this.localSignUpData).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            this.changePassForm.reset();
            this.success = res.message;
            setTimeout(() => {
              this.router.navigateByUrl('/login').then(()=>{
              this.localstorage.removeSignupLocal();
              })            
            }, 3000);
          }else{
            this.error = res.message;
          }    
        }, error: (err: any) => {
        }
      })
    }else{
      this.api.coach_post('reset/password', this.localSignUpData).subscribe({
        next: (res: any) => {
          if(res && !res.error){
            this.changePassForm.reset();
            this.success = res.message;
            setTimeout(() => {
              this.router.navigateByUrl('/login').then(()=>{
              this.localstorage.removeSignupLocal();
              })            
            }, 3000);
          }else{
            this.error = res.message;
          }    
        }, error: (err: any) => {
        }
      })
    }
   
  }
}
