import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { SessionStorageService } from 'src/app/service/session-storage.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent {

  constructor(private api: ApiService, private localstorage: SessionStorageService, private router: Router,
    private activateRoute: ActivatedRoute
  ) {
  }

  localSignUpData: any;
  urlType: any = {
    "USER": false,
    "COACH": false
  }
  ngOnInit() {
    this.activateRoute.params.subscribe((url_type: any) => {
      // console.log(url_type);
      if (!url_type || (url_type.type && !["USER", "COACH"].includes(url_type.type))) {
        this.router.navigateByUrl("/login");
        return;
      }
      this.urlType[url_type.type] = true;
    })
    this.localSignUpData = this.localstorage.getSignupLocal();
    if (
      !this.localSignUpData ||
      !this.localSignUpData.email
    ) {
      this.router.navigateByUrl('/forget-password').then(() => {
        // this.sessionStorage.setSignupLocal(this.forgetForm.value);
      })
      return;
    }
  }

  otpForm: FormGroup = new FormGroup({
    otp: new FormControl("", Validators.required),
  });
  submitted: boolean = false;
  error: any;

  verifyOtp() {

    this.submitted = true
    if (this.otpForm.invalid) {
      return;
    }
    this.localSignUpData.otp = this.otpForm.value.otp;

    if (this.urlType["USER"]) {
      this.api.user_post('verify/otp', this.localSignUpData).subscribe({
        next: (res: any) => {
          if (res && !res.error) {
            this.router.navigateByUrl('/create-password/USER').then(() => {
              this.localstorage.setSignupLocal(this.localSignUpData);
            })
          } else {
            this.error = res.message;
          }
        }, error: (err: any) => {
          console.log(err)
        }
      })
    } else { }
    this.api.coach_post('verify/otp', this.localSignUpData).subscribe({
      next: (res: any) => {
        if (res && !res.error) {
          this.router.navigateByUrl('/create-password/COACH').then(() => {
            this.localstorage.setSignupLocal(this.localSignUpData);
          })
        } else {
          this.error = res.message;
        }
      }, error: (err: any) => {
        console.log(err)
      }
    })

  }
}
