import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { CreatePasswordComponent } from './pages/auth/create-password/create-password.component';
import { ForgotPasswordComponent } from './pages/auth/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './pages/auth/verify-email/verify-email.component';

const routes: Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home',component:LandingPageComponent},
  {path:'login',component:LoginComponent},
  {path:'forget-password/:type',component:ForgotPasswordComponent},
  {path:'verify-email/:type',component:VerifyEmailComponent},
  {path:'create-password/:type',component:CreatePasswordComponent},
  {path:'coach',loadChildren:()=>import('./pages/coach/coach.module').then(c=>c.CoachModule)},
  {path:'trainee',loadChildren:()=>import('./pages/trainee/trainee.module').then(t=>t.TraineeModule)},
  {path:'coach-layout', loadChildren:()=>import('./pages/coach/coach-layout/coach-layout.module').then(ca=>ca.CoachLayoutModule)},
  {path:'trainee-layout', loadChildren:()=>import('./pages/trainee/trainee-layout/trainee-layout.module').then(m=>m.TraineeLayoutModule)},

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
